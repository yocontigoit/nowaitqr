(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html":
    /*!***************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html ***!
      \***************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppHomeHomePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n\n  <div id=\"container\">\n    <img src=\"./assets/icon/logo2.png\" alt=\"\">\n\n    <br>\n\n    <ion-button expand=\"block\" (click)=\"loginGoogle()\" color=\"light\">\n      <ion-icon name=\"logo-google\"></ion-icon>\n      Regístrate con Google\n    </ion-button>\n\n    <br> <br> <br>\n\n    <ion-button id=\"boton\" (click)=\"signInWithFacebook()\" color=\"light\">\n      <ion-icon name=\"logo-facebook\" id=\"logo\"></ion-icon>\n      &nbsp;&nbsp;&nbsp;&nbsp;\n      Regístrate con Facebook\n    </ion-button>\n\n    <ion-button expand=\"block\" (click)=\"ciudad()\" color=\"dark\">\n      <ion-icon name=\"logo-google\"></ion-icon>\n      ciudad\n    </ion-button>\n\n    <!-- <br>\n\n    <ion-button expand=\"block\" color=\"dark\">\n      <ion-icon name=\"logo-google\"></ion-icon>\n      Regístrate con tu correo electrónico\n    </ion-button> -->\n\n    <!-- <strong>Ready to create an app?</strong>\n    <p>Start with Ionic <a target=\"_blank\" rel=\"noopener noreferrer\"\n        href=\"https://ionicframework.com/docs/components\">UI Components</a></p> -->\n  </div>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/home/home-routing.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/home/home-routing.module.ts ***!
      \*********************************************/

    /*! exports provided: HomePageRoutingModule */

    /***/
    function srcAppHomeHomeRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function () {
        return HomePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./home.page */
      "./src/app/home/home.page.ts");

      var routes = [{
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"]
      }];

      var HomePageRoutingModule = function HomePageRoutingModule() {
        _classCallCheck(this, HomePageRoutingModule);
      };

      HomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], HomePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/home/home.module.ts":
    /*!*************************************!*\
      !*** ./src/app/home/home.module.ts ***!
      \*************************************/

    /*! exports provided: HomePageModule */

    /***/
    function srcAppHomeHomeModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomePageModule", function () {
        return HomePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _home_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./home.page */
      "./src/app/home/home.page.ts");
      /* harmony import */


      var _home_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./home-routing.module */
      "./src/app/home/home-routing.module.ts");

      var HomePageModule = function HomePageModule() {
        _classCallCheck(this, HomePageModule);
      };

      HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"], _home_routing_module__WEBPACK_IMPORTED_MODULE_6__["HomePageRoutingModule"]],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_5__["HomePage"]]
      })], HomePageModule);
      /***/
    },

    /***/
    "./src/app/home/home.page.scss":
    /*!*************************************!*\
      !*** ./src/app/home/home.page.scss ***!
      \*************************************/

    /*! exports provided: default */

    /***/
    function srcAppHomeHomePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "#container {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 50%;\n  transform: translateY(-50%);\n}\n\n#container strong {\n  font-size: 20px;\n  line-height: 26px;\n}\n\n#container p {\n  font-size: 16px;\n  line-height: 22px;\n  color: #8c8c8c;\n  margin: 0;\n}\n\n#container a {\n  text-decoration: none;\n}\n\n#ion-button {\n  background: white;\n  background: -webkit-gradient(left top, right top, color-stop(0%, white), color-stop(47%, #f6f6f6), color-stop(100%, #ededed));\n  background: linear-gradient(to right, white 0%, #f6f6f6 47%, #ededed 100%);\n  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\"#ffffff\", endColorstr=\"#ededed\", GradientType=1 );\n}\n\n#logo {\n  color: #3b5998;\n}\n\n#boton {\n  width: 80%;\n  font-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0VBRUEsa0JBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLFFBQUE7RUFDQSwyQkFBQTtBQUFGOztBQUdBO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FBQUY7O0FBR0E7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7RUFFQSxjQUFBO0VBRUEsU0FBQTtBQUZGOztBQUtBO0VBQ0UscUJBQUE7QUFGRjs7QUFLQTtFQUNFLGlCQUFBO0VBRUEsNkhBQUE7RUFJQSwwRUFBQTtFQUNBLG9IQUFBO0FBRkY7O0FBS0E7RUFDRSxjQUFBO0FBRkY7O0FBS0E7RUFDRSxVQUFBO0VBRUEsaUJBQUE7QUFIRiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjY29udGFpbmVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIHRvcDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG59XG5cbiNjb250YWluZXIgc3Ryb25nIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBsaW5lLWhlaWdodDogMjZweDtcbn1cblxuI2NvbnRhaW5lciBwIHtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBsaW5lLWhlaWdodDogMjJweDtcblxuICBjb2xvcjogIzhjOGM4YztcblxuICBtYXJnaW46IDA7XG59XG5cbiNjb250YWluZXIgYSB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn1cblxuI2lvbi1idXR0b24ge1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwyNTUsMjU1LDEpO1xuICBiYWNrZ3JvdW5kOiAtbW96LWxpbmVhci1ncmFkaWVudChsZWZ0LCByZ2JhKDI1NSwyNTUsMjU1LDEpIDAlLCByZ2JhKDI0NiwyNDYsMjQ2LDEpIDQ3JSwgcmdiYSgyMzcsMjM3LDIzNywxKSAxMDAlKTtcbiAgYmFja2dyb3VuZDogLXdlYmtpdC1ncmFkaWVudChsZWZ0IHRvcCwgcmlnaHQgdG9wLCBjb2xvci1zdG9wKDAlLCByZ2JhKDI1NSwyNTUsMjU1LDEpKSwgY29sb3Itc3RvcCg0NyUsIHJnYmEoMjQ2LDI0NiwyNDYsMSkpLCBjb2xvci1zdG9wKDEwMCUsIHJnYmEoMjM3LDIzNywyMzcsMSkpKTtcbiAgYmFja2dyb3VuZDogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQobGVmdCwgcmdiYSgyNTUsMjU1LDI1NSwxKSAwJSwgcmdiYSgyNDYsMjQ2LDI0NiwxKSA0NyUsIHJnYmEoMjM3LDIzNywyMzcsMSkgMTAwJSk7XG4gIGJhY2tncm91bmQ6IC1vLWxpbmVhci1ncmFkaWVudChsZWZ0LCByZ2JhKDI1NSwyNTUsMjU1LDEpIDAlLCByZ2JhKDI0NiwyNDYsMjQ2LDEpIDQ3JSwgcmdiYSgyMzcsMjM3LDIzNywxKSAxMDAlKTtcbiAgYmFja2dyb3VuZDogLW1zLWxpbmVhci1ncmFkaWVudChsZWZ0LCByZ2JhKDI1NSwyNTUsMjU1LDEpIDAlLCByZ2JhKDI0NiwyNDYsMjQ2LDEpIDQ3JSwgcmdiYSgyMzcsMjM3LDIzNywxKSAxMDAlKTtcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCByZ2JhKDI1NSwyNTUsMjU1LDEpIDAlLCByZ2JhKDI0NiwyNDYsMjQ2LDEpIDQ3JSwgcmdiYSgyMzcsMjM3LDIzNywxKSAxMDAlKTtcbiAgZmlsdGVyOiBwcm9naWQ6RFhJbWFnZVRyYW5zZm9ybS5NaWNyb3NvZnQuZ3JhZGllbnQoIHN0YXJ0Q29sb3JzdHI9JyNmZmZmZmYnLCBlbmRDb2xvcnN0cj0nI2VkZWRlZCcsIEdyYWRpZW50VHlwZT0xICk7XG59XG5cbiNsb2dvIHtcbiAgY29sb3I6ICMzYjU5OThcbn1cblxuI2JvdG9uIHtcbiAgd2lkdGg6IDgwJTtcbiAgLy8gZm9udC1zaXplOiBsYXJnZTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/home/home.page.ts":
    /*!***********************************!*\
      !*** ./src/app/home/home.page.ts ***!
      \***********************************/

    /*! exports provided: HomePage */

    /***/
    function srcAppHomeHomePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomePage", function () {
        return HomePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic-native/facebook/ngx */
      "./node_modules/@ionic-native/facebook/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var firebase_database__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! firebase/database */
      "./node_modules/firebase/database/dist/index.esm.js");
      /* harmony import */


      var _servicio_usuario_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../servicio/usuario.service */
      "./src/app/servicio/usuario.service.ts");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/fire/auth */
      "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-auth.js"); // import { AngularFirestore } from 'angularfire2/firestore';


      var HomePage = /*#__PURE__*/function () {
        function HomePage(platform, fb, usuarioProv, alertController, // public afs: AngularFirestore,
        navCtrl, route, router, Afauth, loadingCtrl) {
          _classCallCheck(this, HomePage);

          this.platform = platform;
          this.fb = fb;
          this.usuarioProv = usuarioProv;
          this.alertController = alertController;
          this.navCtrl = navCtrl;
          this.route = route;
          this.router = router;
          this.Afauth = Afauth;
          this.loadingCtrl = loadingCtrl;
        } // signInWithFacebook() {
        //   console.log("Holi");
        //   localStorage.setItem("isLogin", 'true');
        //   if (this.platform.is('cordova')) {
        //     this.fb.login(['public_profile', 'email']).then((res: FacebookLoginResponse) => {
        //       const facebookCredential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
        //       firebase.auth().signInWithCredential(facebookCredential)
        //         .then(user => {
        //           //console.log('datos de la sesion del user',user);
        //           this.us = user;
        //           console.log('Usuario: ', JSON.stringify(this.us));
        //           localStorage.setItem("uid", this.us.uid);
        //           //console.log('userID',this.us.uid);
        //           this.url = "?height=500";
        //           this.usPhotoUrl = this.us.photoURL + this.url;
        //           console.log("Foto", this.usPhotoUrl);
        //           this.usuarioProv.cargarUsuario(
        //             this.us.displayName,
        //             this.us.email,
        //             this.us.photoURL,
        //             this.us.uid
        //           );
        //           //  sacar el codigo del usuario
        //           this.usuarioProv.getCodigo(this.us.uid).subscribe(co => {
        //             this.codigos = co;
        //             console.log('datos tabla user', this.codigos.length);
        //             if (this.codigos.length == 0) {
        //               console.log('agregar usuario nuevo');
        //               this.afs.collection('users').doc(this.usuarioProv.usuario.uid).set({
        //                 idUser: this.usuarioProv.usuario.uid,
        //                 displayName: this.us.displayName,
        //                 email: this.us.email,
        //                 photoURL: this.us.photoURL
        //               })
        //                 .then(() => this.mensaje(this.us.uid, this.us.email, this.us.photoURL))
        //                 .catch(function (error) {
        //                   console.error("Error adding document: ", error);
        //                 });
        //             }
        //             if (this.codigos.length == 1) {
        //               this.router.navigate(['ciudad', this.us.uid]);
        //             }
        //           });
        //         }).catch(e => alert('Error de autenticación' + JSON.stringify(e)));
        //     })
        //   }
        // }
        // signInWithFacebook() {
        //   localStorage.setItem("isLogin", 'true');
        //   if (this.platform.is('cordova')) {
        //     this.fb.login(['public_profile', 'email']).then((res: FacebookLoginResponse) => {
        //       const facebookCredential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
        //       firebase.auth().signInWithCredential(facebookCredential)
        //         .then(user => {
        //           //console.log('datos de la sesion del user',user);
        //           this.us = user;
        //           console.log('Usuario: ', JSON.stringify(this.us));
        //           localStorage.setItem("uid", this.us.uid);
        //           //console.log('userID',this.us.uid);
        //           this.url = "?height=500";
        //           this.usPhotoUrl = this.us.photoURL + this.url;
        //           this.usuarioProv.cargarUsuario(
        //             this.us.displayName,
        //             this.us.email,
        //             this.us.photoURL,
        //             this.us.uid
        //           );
        //           //sacar el codigo del usuario
        //           this.usuarioProv.getCodigo(this.us.uid).subscribe(co => {
        //             this.codigos = co;
        //             console.log('datos tabla user', this.codigos.length);
        //             if (this.codigos.length == 0) {
        //               console.log('agregar tel');
        //               this.afs.collection('users').doc(this.usuarioProv.usuario.uid).set({
        //                 key: this.usuarioProv.usuario.uid,
        //                 displayName: this.us.displayName,
        //                 email: this.us.email,
        //                 photoURL: this.us.photoURL,
        //                 user: 0,
        //                 status: 0,
        //                 seguidores: 0,
        //                 suscripcion: 0
        //               })
        //                 .then(() => this.mensaje(this.us.uid, this.us.email, this.us.photoURL))
        //                 .catch(function (error) {
        //                   console.error("Error adding document: ", error);
        //                 });
        //             }
        //             if (this.codigos.length == 1) {
        //               this.router.navigate(['ciudad', this.us.uid]);
        //             }
        //           });
        //         }).catch(e => alert('Error de autenticación' + JSON.stringify(e)));
        //     })
        //   }
        // }
        // loginGoogle(){
        //   this.usuarioProv.loginwithGoogle().then(() => {
        //     this.router.navigate(['/ciudad']);
        //   })
        //     .catch(e => alert('Algo salio mal en Google'));
        // }
        // signInWithFacebook() {
        //   this.usuarioProv.loginwithfacebook().then(res => {
        //     this.router.navigate(['/ciudad']);
        //   }).catch(err => {
        //     alert('hubo un error en facebook');
        //   })
        // }


        _createClass(HomePage, [{
          key: "ciudad",
          value: function ciudad() {
            this.router.navigate(['/ciudad']);
          }
        }, {
          key: "mensaje",
          value: function mensaje(uid, email, photoURL) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var loading, _yield$loading$onDidD, role, data;

              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.loadingCtrl.create({
                        cssClass: 'my-custom-class',
                        message: 'Iniciando Sesión',
                        duration: 4000
                      });

                    case 2:
                      loading = _context.sent;
                      _context.next = 5;
                      return loading.present();

                    case 5:
                      _context.next = 7;
                      return loading.onDidDismiss();

                    case 7:
                      _yield$loading$onDidD = _context.sent;
                      role = _yield$loading$onDidD.role;
                      data = _yield$loading$onDidD.data;
                      console.log('Loading dismissed!');

                    case 11:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }]);

        return HomePage;
      }();

      HomePage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"]
        }, {
          type: _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_3__["Facebook"]
        }, {
          type: _servicio_usuario_service__WEBPACK_IMPORTED_MODULE_5__["UsuarioService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]
        }, {
          type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_7__["AngularFireAuth"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
        }];
      };

      HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./home.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./home.page.scss */
        "./src/app/home/home.page.scss"))["default"]]
      })], HomePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=home-home-module-es5.js.map