(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["component-detalles-detalles-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/component/detalles/detalles.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/component/detalles/detalles.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content padding>\n  <br>\n\n  <div id=\"nombrePro\"><b id=\"frase\">No Wait</b> QR</div>\n\n  <br>\n\n  <ion-card id=\"cuadro\">\n    <ion-card-header>\n    </ion-card-header>\n\n    <ion-card-content>\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card id=\"cuadroTwo\">\n    <ion-card-header>\n    </ion-card-header>\n\n    <ion-card-content>\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card id=\"cuadroThree\">\n    <ion-card-header>\n    </ion-card-header>\n\n    <ion-card-content>\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card id=\"cuadroFour\">\n    <ion-card-header>\n      <div *ngFor=\"let tienda of detalleTienda\">\n        <p id=\"nombre\">{{tienda.nombre_tienda}}</p>\n      </div>\n      <div *ngFor=\"let suc of detalleSucursal\">\n        <p id=\"nombre_ti\">{{suc.nombre_suc}}</p>\n      </div>\n    </ion-card-header>\n\n    <ion-card-content>\n      <div *ngFor=\"let turno of detalleTurno\">\n        <div *ngIf=\"turno.estatus == 'pendiente'\">\n          <p id=\"turno\">Tu Lugar es el:</p>\n          <p id=\"numTurno\">000{{numTurno}}</p>\n          <br>\n          <p id=\"personas\">Personas antes de ti:</p>\n          <p id=\"numper\">{{total - 1}}</p>\n          <p id=\"personas\">Tiempo estimado:</p>\n          <p id=\"numpers\">{{(total - 1) * 5}} min</p>\n        </div>\n        <div *ngIf=\"turno.estatus == 'activado'\">\n          <br>\n          <p id=\"bienvenido\">Bienvenido</p>\n          <br>\n          <p id=\"disfruta\">Disfruta tu estancia</p>\n        </div>\n      </div>\n    </ion-card-content>\n  </ion-card>\n\n  <div *ngFor=\"let turno of detalleTurno\">\n    <div *ngIf=\"turno.estatus == 'pendiente'\">\n      <ion-button id=\"botonc\" (click)=\"refreshPage()\" color=\"dark\">\n        Actualizar Lista\n      </ion-button>\n    </div>\n  </div>\n\n  <ion-button id=\"botonb\" (click)=\"goToBack()\" color=\"dark\">\n    Atrás\n  </ion-button>\n\n</ion-content>");

/***/ }),

/***/ "./src/app/component/detalles/detalles-routing.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/component/detalles/detalles-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: DetallesPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesPageRoutingModule", function() { return DetallesPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _detalles_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./detalles.page */ "./src/app/component/detalles/detalles.page.ts");




const routes = [
    {
        path: '',
        component: _detalles_page__WEBPACK_IMPORTED_MODULE_3__["DetallesPage"]
    }
];
let DetallesPageRoutingModule = class DetallesPageRoutingModule {
};
DetallesPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DetallesPageRoutingModule);



/***/ }),

/***/ "./src/app/component/detalles/detalles.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/component/detalles/detalles.module.ts ***!
  \*******************************************************/
/*! exports provided: DetallesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesPageModule", function() { return DetallesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _detalles_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./detalles-routing.module */ "./src/app/component/detalles/detalles-routing.module.ts");
/* harmony import */ var _detalles_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detalles.page */ "./src/app/component/detalles/detalles.page.ts");







let DetallesPageModule = class DetallesPageModule {
};
DetallesPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _detalles_routing_module__WEBPACK_IMPORTED_MODULE_5__["DetallesPageRoutingModule"]
        ],
        declarations: [_detalles_page__WEBPACK_IMPORTED_MODULE_6__["DetallesPage"]]
    })
], DetallesPageModule);



/***/ }),

/***/ "./src/app/component/detalles/detalles.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/component/detalles/detalles.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#cuadro {\n  background-color: black;\n  height: 350px;\n  width: 300px;\n  border-radius: 25px;\n  margin-left: 5%;\n  position: fixed;\n}\n\n#cuadroTwo {\n  background-color: #797979;\n  height: 350px;\n  width: 300px;\n  border-radius: 25px;\n  margin-top: 17%;\n  margin-left: 10%;\n  position: fixed;\n}\n\n#cuadroThree {\n  background-color: #484646;\n  height: 350px;\n  width: 300px;\n  border-radius: 25px;\n  margin-top: 27%;\n  margin-left: 15%;\n  position: fixed;\n}\n\n#cuadroFour {\n  background-color: #b3b3b3;\n  height: 350px;\n  width: 300px;\n  border-radius: 25px;\n  margin-top: 37%;\n  margin-left: 10%;\n  position: fixed;\n}\n\n#nombre {\n  font-weight: bold;\n  font-size: x-large;\n  color: black;\n  text-align: center;\n  text-transform: uppercase;\n}\n\n#nombre_ti {\n  text-align: center;\n  margin-top: -15px;\n}\n\n#botonc {\n  width: 80%;\n  font-size: 71%;\n  font-weight: bold;\n  margin-left: 10%;\n}\n\n#turno {\n  font-size: x-large;\n  text-align: center;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n#numTurno {\n  font-size: xx-large;\n  text-align: center;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n#personas {\n  font-size: medium;\n  text-align: center;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n#numper {\n  text-align: center;\n  font-size: larger;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n#numpers {\n  text-align: center;\n  font-size: x-large;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n#imgfo {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  /* top: 50%; */\n  transform: translateY(550%);\n  position: fixed;\n}\n\n#texto {\n  text-align: center;\n  font-size: large;\n  color: #777777;\n  position: fixed;\n  margin-top: -35px;\n  margin-left: 20px;\n  margin-right: 20px;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n#botonc {\n  width: 80%;\n  font-size: 71%;\n  font-weight: bold;\n  margin-left: 10%;\n  position: fixed;\n  margin-top: 135%;\n}\n\n#botonb {\n  width: 80%;\n  font-size: 71%;\n  font-weight: bold;\n  margin-left: 10%;\n  position: fixed;\n  margin-top: 145%;\n}\n\n#bienvenido {\n  font-size: xx-large;\n  text-align: center;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n#disfruta {\n  font-size: x-large;\n  text-align: center;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n#nombrePro {\n  font-size: xx-large;\n  color: #676767;\n  font-family: Arial, Helvetica, sans-serif;\n  text-align: center;\n}\n\n#frase {\n  font-weight: bold;\n  font-family: Arial, Helvetica, sans-serif;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50L2RldGFsbGVzL2RldGFsbGVzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHVCQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBRUE7RUFDSSx5QkFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBRUE7RUFDSSx5QkFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBRUE7RUFDSSx5QkFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBRUE7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsaUJBQUE7QUFDSjs7QUFFQTtFQUNJLFVBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtBQUNKOztBQUVBO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtFQUNBLHlDQUFBO0FBQ0o7O0FBRUE7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EseUNBQUE7QUFDSjs7QUFFQTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSx5Q0FBQTtBQUNKOztBQUVBO0VBQ0ksa0JBQUE7RUFDQSxpQkFBQTtFQUNBLHlDQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EseUNBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLGNBQUE7RUFDQSwyQkFBQTtFQUNBLGVBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLHlDQUFBO0FBQ0o7O0FBRUE7RUFDSSxVQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUFDSjs7QUFFQTtFQUNJLFVBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQUNKOztBQUVBO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLHlDQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EseUNBQUE7QUFDSjs7QUFFQTtFQUNJLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLHlDQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFFQTtFQUNJLGlCQUFBO0VBQ0EseUNBQUE7QUFDSiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudC9kZXRhbGxlcy9kZXRhbGxlcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjY3VhZHJve1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbiAgICBoZWlnaHQ6IDM1MHB4O1xyXG4gICAgd2lkdGg6IDMwMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgIG1hcmdpbi1sZWZ0OiA1JTtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxufVxyXG5cclxuI2N1YWRyb1R3b3tcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM3OTc5Nzk7XHJcbiAgICBoZWlnaHQ6IDM1MHB4O1xyXG4gICAgd2lkdGg6IDMwMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgIG1hcmdpbi10b3A6IDE3JTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMCU7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbn1cclxuXHJcbiNjdWFkcm9UaHJlZSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNDg0NjQ2O1xyXG4gICAgaGVpZ2h0OiAzNTBweDtcclxuICAgIHdpZHRoOiAzMDBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgICBtYXJnaW4tdG9wOiAyNyU7XHJcbiAgICBtYXJnaW4tbGVmdDogMTUlO1xyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG59XHJcblxyXG4jY3VhZHJvRm91ciB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjNiM2IzO1xyXG4gICAgaGVpZ2h0OiAzNTBweDtcclxuICAgIHdpZHRoOiAzMDBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgICBtYXJnaW4tdG9wOiAzNyU7XHJcbiAgICBtYXJnaW4tbGVmdDogMTAlO1xyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG59XHJcblxyXG4jbm9tYnJlIHtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgZm9udC1zaXplOiB4LWxhcmdlO1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxufVxyXG5cclxuI25vbWJyZV90aSB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tdG9wOiAtMTVweDtcclxufVxyXG5cclxuI2JvdG9uYyB7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICAgZm9udC1zaXplOiA3MSU7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMCU7XHJcbn1cclxuXHJcbiN0dXJub3tcclxuICAgIGZvbnQtc2l6ZTogeC1sYXJnZTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtZmFtaWx5OiBBcmlhbCwgSGVsdmV0aWNhLCBzYW5zLXNlcmlmO1xyXG59XHJcblxyXG4jbnVtVHVybm97XHJcbiAgICBmb250LXNpemU6IHh4LWxhcmdlO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1mYW1pbHk6IEFyaWFsLCBIZWx2ZXRpY2EsIHNhbnMtc2VyaWY7XHJcbn1cclxuXHJcbiNwZXJzb25hc3tcclxuICAgIGZvbnQtc2l6ZTogbWVkaXVtO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1mYW1pbHk6IEFyaWFsLCBIZWx2ZXRpY2EsIHNhbnMtc2VyaWY7XHJcbn1cclxuXHJcbiNudW1wZXJ7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IGxhcmdlcjtcclxuICAgIGZvbnQtZmFtaWx5OiBBcmlhbCwgSGVsdmV0aWNhLCBzYW5zLXNlcmlmO1xyXG59XHJcblxyXG4jbnVtcGVyc3tcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogeC1sYXJnZTtcclxuICAgIGZvbnQtZmFtaWx5OiBBcmlhbCwgSGVsdmV0aWNhLCBzYW5zLXNlcmlmO1xyXG59XHJcblxyXG4jaW1nZm8ge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgbGVmdDogMDtcclxuICAgIHJpZ2h0OiAwO1xyXG4gICAgLyogdG9wOiA1MCU7ICovXHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoNTUwJSk7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgfVxyXG5cclxuI3RleHRve1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiBsYXJnZTtcclxuICAgIGNvbG9yOiAjNzc3Nzc3O1xyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgbWFyZ2luLXRvcDogLTM1cHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMjBweDtcclxuICAgIG1hcmdpbi1yaWdodDogMjBweDtcclxuICAgIGZvbnQtZmFtaWx5OiBBcmlhbCwgSGVsdmV0aWNhLCBzYW5zLXNlcmlmO1xyXG59XHJcblxyXG4jYm90b25je1xyXG4gICAgd2lkdGg6IDgwJTtcclxuICAgIGZvbnQtc2l6ZTogNzElO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBtYXJnaW4tbGVmdDogMTAlO1xyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgbWFyZ2luLXRvcDogMTM1JTtcclxufVxyXG5cclxuI2JvdG9uYntcclxuICAgIHdpZHRoOiA4MCU7XHJcbiAgICBmb250LXNpemU6IDcxJTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwJTtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIG1hcmdpbi10b3A6IDE0NSU7XHJcbn1cclxuXHJcbiNiaWVudmVuaWRve1xyXG4gICAgZm9udC1zaXplOiB4eC1sYXJnZTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtZmFtaWx5OiBBcmlhbCwgSGVsdmV0aWNhLCBzYW5zLXNlcmlmO1xyXG59XHJcblxyXG4jZGlzZnJ1dGF7XHJcbiAgICBmb250LXNpemU6IHgtbGFyZ2U7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LWZhbWlseTogQXJpYWwsIEhlbHZldGljYSwgc2Fucy1zZXJpZjtcclxufVxyXG5cclxuI25vbWJyZVBybyB7XHJcbiAgICBmb250LXNpemU6IHh4LWxhcmdlO1xyXG4gICAgY29sb3I6ICM2NzY3Njc7XHJcbiAgICBmb250LWZhbWlseTogQXJpYWwsIEhlbHZldGljYSwgc2Fucy1zZXJpZjtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4gIFxyXG4jZnJhc2Uge1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBmb250LWZhbWlseTogQXJpYWwsIEhlbHZldGljYSwgc2Fucy1zZXJpZjtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/component/detalles/detalles.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/component/detalles/detalles.page.ts ***!
  \*****************************************************/
/*! exports provided: DetallesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesPage", function() { return DetallesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var src_app_servicio_sucursal_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/servicio/sucursal.service */ "./src/app/servicio/sucursal.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);





let DetallesPage = class DetallesPage {
    constructor(route, sucProv, router) {
        this.route = route;
        this.sucProv = sucProv;
        this.router = router;
        this.detalleTienda = [];
        this.detalleSucursal = [];
        this.detalleTurno = [];
        this.allUser = [];
    }
    ngOnInit() {
        let id = this.route.snapshot.paramMap.get("id");
        console.log("Este es el idUser", id);
        this.idUser = id;
        let idTienda = this.route.snapshot.paramMap.get("idTienda");
        console.log("este es el idTienda", idTienda);
        this.idTienda = idTienda;
        let idFila = this.route.snapshot.paramMap.get("idFila");
        console.log("Este es el idFila", idFila);
        this.idFila = parseInt(idFila);
        var today = moment__WEBPACK_IMPORTED_MODULE_4__().format('YYYY-MM-DD');
        console.log("variable today", today);
        this.momentos = today;
        console.log("variable momentos", this.momentos);
        this.detalleTienda = [];
        this.detalleSucursal = [];
        this.detalleTurno = [];
        this.allUser = [];
        this.goToTienda();
        this.goToFila();
        this.Alluser();
    }
    goToTienda() {
        return new Promise(resolve => {
            let body = {
                idTienda: this.idTienda,
                accion: 'goToDetalleTienda'
            };
            this.sucProv.postData(body, 'lugares.php').subscribe(data => {
                for (let detalleTienda of data.result) {
                    this.detalleTienda.push(detalleTienda);
                    console.log("Datos de la tienda seleccionada", this.detalleTienda);
                    this.goToSucursal(detalleTienda.idSucursal);
                }
                resolve(true);
            });
        });
    }
    goToSucursal(idSucursal) {
        return new Promise(resolve => {
            let body = {
                idSucursal: idSucursal,
                accion: 'getDetalleSucursal'
            };
            this.sucProv.postData(body, 'lugares.php').subscribe(data => {
                for (let detalleSucursal of data.result) {
                    this.detalleSucursal.push(detalleSucursal);
                    console.log("Datos de las sucursales", this.detalleSucursal);
                }
                resolve(true);
            });
        });
    }
    goToFila() {
        return new Promise(resolve => {
            let body = {
                idUser: this.idUser,
                idTienda: this.idTienda,
                idFila: this.idFila,
                accion: 'getDetalleFila'
            };
            this.sucProv.postData(body, 'lugares.php').subscribe(data => {
                for (let turno of data.result) {
                    this.detalleTurno.push(turno);
                    console.log("Datos del turno", this.detalleTurno);
                    if (turno.estatus == 'pendiente') {
                        this.numTurno = turno.numTurno;
                        console.log("turnos en estado pendiente", this.numTurno);
                    }
                }
                resolve(true);
            });
        });
    }
    Alluser() {
        return new Promise(resolve => {
            let body = {
                idTienda: this.idTienda,
                fecha: this.momentos,
                estatus: 'pendiente',
                accion: 'getAllUser'
            };
            this.sucProv.postData(body, 'turno.php').subscribe(data => {
                for (let all of data.result) {
                    this.allUser.push(all);
                    console.log("Datos de los usuarios en tienda", this.allUser);
                    this.total = parseInt(this.allUser.length);
                    console.log("total de usuarios en espera", this.total);
                }
                resolve(true);
            });
        });
    }
    refreshPage() {
        this.ngOnInit();
    }
    goToBack() {
        this.router.navigate(['/lugares', this.idUser]);
    }
};
DetallesPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: src_app_servicio_sucursal_service__WEBPACK_IMPORTED_MODULE_3__["SucursalService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
DetallesPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-detalles',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./detalles.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/component/detalles/detalles.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./detalles.page.scss */ "./src/app/component/detalles/detalles.page.scss")).default]
    })
], DetallesPage);



/***/ })

}]);
//# sourceMappingURL=component-detalles-detalles-module-es2015.js.map