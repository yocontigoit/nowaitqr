(function () {
  function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["component-ciudad-ciudad-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/component/ciudad/ciudad.page.html":
    /*!*****************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/component/ciudad/ciudad.page.html ***!
      \*****************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentCiudadCiudadPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n\n  <br> <br> <br>\n\n  <ion-list>\n    <div id=\"select\">Selecciona</div>\n\n    <br> <br> <br> <br> <br>\n\n    <ion-item>\n      <ion-label> Estado </ion-label>\n      <ion-select placeholder=\"Selecciona\" (ionChange)=\"checkEstado($event)\">\n        <ion-select-option *ngFor=\"let munic of municipio\" [value]=\"munic.idMunic\">{{munic.nombre_mun}}\n        </ion-select-option>\n      </ion-select>\n    </ion-item>\n\n    <br> <br>\n\n    <div *ngIf=\"ocultar3\">\n      <ion-item>\n        <ion-label> Ciudad </ion-label>\n        <ion-select placeholder=\"Selecciona\" (ionChange)=\"checkValue($event)\">\n          <ion-select-option *ngFor=\"let ciudad of ciudades\" [value]=\"ciudad.idCiudad\">{{ciudad.nombre}}\n          </ion-select-option>\n        </ion-select>\n      </ion-item>\n    </div>\n\n    <br> <br>\n\n    <div *ngIf=\"ocultar1\">\n      <ion-item>\n        <ion-label> Sucursales </ion-label>\n        <ion-select placeholder=\"Selecciona\" (ionChange)=\"Sucursales($event)\">\n          <ion-select-option *ngFor=\"let sucursal of sucursales\" [value]=\"sucursal.idSucursal\">{{sucursal.nombre_suc}}\n          </ion-select-option>\n        </ion-select>\n      </ion-item>\n    </div>\n\n    <br> <br> <br> <br> <br>\n\n    <div *ngIf=\"ocultar2\">\n      <ion-button (click)=\"goToEstablecimiento()\" color=\"dark\" id=\"botonE\">Entrar</ion-button>\n    </div>\n\n  </ion-list>\n\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/component/ciudad/ciudad-routing.module.ts":
    /*!***********************************************************!*\
      !*** ./src/app/component/ciudad/ciudad-routing.module.ts ***!
      \***********************************************************/

    /*! exports provided: CiudadPageRoutingModule */

    /***/
    function srcAppComponentCiudadCiudadRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CiudadPageRoutingModule", function () {
        return CiudadPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ciudad_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./ciudad.page */
      "./src/app/component/ciudad/ciudad.page.ts");

      var routes = [{
        path: '',
        component: _ciudad_page__WEBPACK_IMPORTED_MODULE_3__["CiudadPage"]
      }];

      var CiudadPageRoutingModule = function CiudadPageRoutingModule() {
        _classCallCheck(this, CiudadPageRoutingModule);
      };

      CiudadPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], CiudadPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/component/ciudad/ciudad.module.ts":
    /*!***************************************************!*\
      !*** ./src/app/component/ciudad/ciudad.module.ts ***!
      \***************************************************/

    /*! exports provided: CiudadPageModule */

    /***/
    function srcAppComponentCiudadCiudadModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CiudadPageModule", function () {
        return CiudadPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _ciudad_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./ciudad-routing.module */
      "./src/app/component/ciudad/ciudad-routing.module.ts");
      /* harmony import */


      var _ciudad_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./ciudad.page */
      "./src/app/component/ciudad/ciudad.page.ts");

      var CiudadPageModule = function CiudadPageModule() {
        _classCallCheck(this, CiudadPageModule);
      };

      CiudadPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _ciudad_routing_module__WEBPACK_IMPORTED_MODULE_5__["CiudadPageRoutingModule"]],
        declarations: [_ciudad_page__WEBPACK_IMPORTED_MODULE_6__["CiudadPage"]]
      })], CiudadPageModule);
      /***/
    },

    /***/
    "./src/app/component/ciudad/ciudad.page.scss":
    /*!***************************************************!*\
      !*** ./src/app/component/ciudad/ciudad.page.scss ***!
      \***************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentCiudadCiudadPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "#select {\n  text-align: center;\n  font-size: x-large;\n  font-weight: bold;\n}\n\n#botonE {\n  width: 80%;\n  font-weight: bold;\n  left: 0;\n  right: 0;\n  margin-left: 10%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50L2NpdWRhZC9jaXVkYWQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FBQ0o7O0FBRUE7RUFDSSxVQUFBO0VBQ0EsaUJBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUVBLGdCQUFBO0FBQUoiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnQvY2l1ZGFkL2NpdWRhZC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjc2VsZWN0IHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogeC1sYXJnZTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcblxyXG4jYm90b25FIHtcclxuICAgIHdpZHRoOiA4MCU7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICByaWdodDogMDtcclxuICAgIC8vbWFyZ2luLXRvcDogMzAlO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwJTsgICAgXHJcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/component/ciudad/ciudad.page.ts":
    /*!*************************************************!*\
      !*** ./src/app/component/ciudad/ciudad.page.ts ***!
      \*************************************************/

    /*! exports provided: CiudadPage */

    /***/
    function srcAppComponentCiudadCiudadPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CiudadPage", function () {
        return CiudadPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var src_app_servicio_sucursal_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/servicio/sucursal.service */
      "./src/app/servicio/sucursal.service.ts");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var src_app_servicio_notification_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/servicio/notification.service */
      "./src/app/servicio/notification.service.ts");
      /* harmony import */


      var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/fire/firestore */
      "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js"); // import { async } from '@angular/core/testing';


      var CiudadPage = /*#__PURE__*/function () {
        function CiudadPage(sucProv, router, route, loadingCtrl, pushnot, db) {
          _classCallCheck(this, CiudadPage);

          this.sucProv = sucProv;
          this.router = router;
          this.route = route;
          this.loadingCtrl = loadingCtrl;
          this.pushnot = pushnot;
          this.db = db;
          this.ocultar1 = false;
          this.ocultar2 = false;
          this.ocultar3 = false;
          this.ciudades = [];
          this.sucursales = [];
          this.municipio = [];
        }

        _createClass(CiudadPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var id = this.route.snapshot.paramMap.get("id");
            console.log("este es el idUser:", id);
            this.idUser = id;
            this.AllMunic();
            this.ciudades = [];
            this.sucursales = [];
            this.municipio = [];
          }
        }, {
          key: "AllMunic",
          value: function AllMunic() {
            var _this = this;

            return new Promise(function (resolve) {
              var body = {
                accion: 'getDataMunicipio'
              };

              _this.sucProv.postData(body, 'add_user.php').subscribe(function (data) {
                var _iterator = _createForOfIteratorHelper(data.result),
                    _step;

                try {
                  for (_iterator.s(); !(_step = _iterator.n()).done;) {
                    var munic = _step.value;

                    _this.municipio.push(munic);

                    console.log("Total de municipio", _this.municipio);
                  }
                } catch (err) {
                  _iterator.e(err);
                } finally {
                  _iterator.f();
                }

                resolve(true);
              });
            });
          }
        }, {
          key: "checkEstado",
          value: function checkEstado(event) {
            var _this2 = this;

            console.log("Este es el idMunic", event.detail.value);
            var body = {
              idMunic: event.detail.value,
              idUser: this.idUser,
              accion: 'update_munic'
            };
            this.sucProv.postData(body, 'add_user.php').subscribe(function (data) {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                return regeneratorRuntime.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        if (data.success) {
                          console.log("Se ha agregado el municipio");
                        }

                      case 1:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee);
              }));
            });
            this.AllCiudad(event.detail.value);
          }
        }, {
          key: "AllCiudad",
          value: function AllCiudad(event) {
            var _this3 = this;

            this.ocultar3 = !this.ocultar3;
            return new Promise(function (resolve) {
              var body = {
                idMunic: event,
                accion: 'getdata'
              };

              _this3.sucProv.postData(body, 'add_user.php').subscribe(function (data) {
                var _iterator2 = _createForOfIteratorHelper(data.result),
                    _step2;

                try {
                  for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
                    var ciudad = _step2.value;

                    _this3.ciudades.push(ciudad);

                    console.log("Total de ciudades", _this3.ciudades);
                  }
                } catch (err) {
                  _iterator2.e(err);
                } finally {
                  _iterator2.f();
                }

                resolve(true);
              });
            });
          }
        }, {
          key: "checkValue",
          value: function checkValue(event) {
            var _this4 = this;

            console.log(event.detail.value);
            var body = {
              idCiudad: event.detail.value,
              idUser: this.idUser,
              accion: 'update_ciudad'
            };
            this.sucProv.postData(body, 'add_user.php').subscribe(function (data) {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this4, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                  while (1) {
                    switch (_context2.prev = _context2.next) {
                      case 0:
                        if (data.success) {
                          console.log("se ha agregado la ciudad");
                        }

                      case 1:
                      case "end":
                        return _context2.stop();
                    }
                  }
                }, _callee2);
              }));
            });
            this.sucursal(event.detail.value);
            this.idCiudad = event.detail.value;
          }
        }, {
          key: "sucursal",
          value: function sucursal(ciudad) {
            var _this5 = this;

            this.ocultar1 = !this.ocultar1;
            console.log("funcion de sucursal", ciudad);
            return new Promise(function (resolve) {
              var body = {
                idCiudad: ciudad,
                accion: 'getdata_sucursal'
              };

              _this5.sucProv.postData(body, 'add_user.php').subscribe(function (data) {
                var _iterator3 = _createForOfIteratorHelper(data.result),
                    _step3;

                try {
                  for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
                    var sucursal = _step3.value;

                    _this5.sucursales.push(sucursal);

                    console.log("Total de sucursales", _this5.sucursales);
                  }
                } catch (err) {
                  _iterator3.e(err);
                } finally {
                  _iterator3.f();
                }

                resolve(true);
              });
            });
          }
        }, {
          key: "Sucursales",
          value: function Sucursales(event) {
            var _this6 = this;

            this.ocultar2 = !this.ocultar2;
            console.log(event.detail.value);
            var body = {
              idSucursal: event.detail.value,
              idUser: this.idUser,
              accion: 'update_sucursal'
            };
            this.sucProv.postData(body, 'add_user.php').subscribe(function (data) {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this6, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
                return regeneratorRuntime.wrap(function _callee3$(_context3) {
                  while (1) {
                    switch (_context3.prev = _context3.next) {
                      case 0:
                        if (data.success) {
                          console.log("se ha guardado la sucursal");
                        }

                      case 1:
                      case "end":
                        return _context3.stop();
                    }
                  }
                }, _callee3);
              }));
            });
            this.idSucursal = event.detail.value; // this.goToEstablecimiento();

            this.db.collection("users").doc(this.idUser).update({
              estatusPush: 2
            }).then(function () {
              console.log("Se actualizo");
            });
          }
        }, {
          key: "goToEstablecimiento",
          value: function goToEstablecimiento() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var loading, _yield$loading$onDidD, role, data;

              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      _context4.next = 2;
                      return this.loadingCtrl.create({
                        cssClass: 'my-custom-class',
                        message: 'Iniciando Sesión',
                        duration: 2000
                      });

                    case 2:
                      loading = _context4.sent;
                      _context4.next = 5;
                      return loading.present();

                    case 5:
                      _context4.next = 7;
                      return loading.onDidDismiss();

                    case 7:
                      _yield$loading$onDidD = _context4.sent;
                      role = _yield$loading$onDidD.role;
                      data = _yield$loading$onDidD.data;
                      console.log('Loading dismissed!');
                      this.goToFinally();

                    case 12:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "goToFinally",
          value: function goToFinally() {
            this.router.navigate(['/tienda', this.idUser]); // this.pushnot.init_notification(this.idUser);
            // this.goToFinal();
            // setTimeout(this.goToFinal,2000);
          }
        }, {
          key: "goToFinal",
          value: function goToFinal() {
            this.router.navigate(['/tienda', this.idUser]);
          }
        }]);

        return CiudadPage;
      }();

      CiudadPage.ctorParameters = function () {
        return [{
          type: src_app_servicio_sucursal_service__WEBPACK_IMPORTED_MODULE_2__["SucursalService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]
        }, {
          type: src_app_servicio_notification_service__WEBPACK_IMPORTED_MODULE_5__["NotificationService"]
        }, {
          type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__["AngularFirestore"]
        }];
      };

      CiudadPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-ciudad',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./ciudad.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/component/ciudad/ciudad.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./ciudad.page.scss */
        "./src/app/component/ciudad/ciudad.page.scss"))["default"]]
      })], CiudadPage);
      /***/
    },

    /***/
    "./src/app/servicio/sucursal.service.ts":
    /*!**********************************************!*\
      !*** ./src/app/servicio/sucursal.service.ts ***!
      \**********************************************/

    /*! exports provided: SucursalService */

    /***/
    function srcAppServicioSucursalServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SucursalService", function () {
        return SucursalService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/http */
      "./node_modules/@angular/http/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js"); //import 'rxjs/add/operator/map';


      var SucursalService = /*#__PURE__*/function () {
        //server: string = "http://localhost/server_api_nowaitqr/";
        function SucursalService(http) {
          _classCallCheck(this, SucursalService);

          this.http = http;
          this.server = "https://proyectosinternos.com/NoWaitQR/server_api_nowaitqr/";
        } // listar() {
        //   return this.http.get('https://proyectosinternos.com/NoWaitQR/select_ciudad.php/');
        // }
        // getAll(){
        //   return this.http.get<[Usuario]>(this.url);
        // }


        _createClass(SucursalService, [{
          key: "postData",
          value: function postData(body, file) {
            var type = "application/json; charset=UTF-8";
            var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
              'Content-Type': type
            });
            var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({
              headers: headers
            });
            return this.http.post(this.server + file, JSON.stringify(body), options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
              return res.json();
            }));
          }
        }]);

        return SucursalService;
      }();

      SucursalService.ctorParameters = function () {
        return [{
          type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]
        }];
      };

      SucursalService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], SucursalService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=component-ciudad-ciudad-module-es5.js.map