(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["component-ciudad-ciudad-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/component/ciudad/ciudad.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/component/ciudad/ciudad.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n\n  <br> <br> <br>\n\n  <ion-list>\n    <div id=\"select\">Selecciona</div>\n\n    <br> <br> <br> <br> <br>\n\n    <ion-item>\n      <ion-label> Estado </ion-label>\n      <ion-select placeholder=\"Selecciona\" (ionChange)=\"checkEstado($event)\">\n        <ion-select-option *ngFor=\"let munic of municipio\" [value]=\"munic.idMunic\">{{munic.nombre_mun}}\n        </ion-select-option>\n      </ion-select>\n    </ion-item>\n\n    <br> <br>\n\n    <div *ngIf=\"ocultar3\">\n      <ion-item>\n        <ion-label> Ciudad </ion-label>\n        <ion-select placeholder=\"Selecciona\" (ionChange)=\"checkValue($event)\">\n          <ion-select-option *ngFor=\"let ciudad of ciudades\" [value]=\"ciudad.idCiudad\">{{ciudad.nombre}}\n          </ion-select-option>\n        </ion-select>\n      </ion-item>\n    </div>\n\n    <br> <br>\n\n    <div *ngIf=\"ocultar1\">\n      <ion-item>\n        <ion-label> Sucursales </ion-label>\n        <ion-select placeholder=\"Selecciona\" (ionChange)=\"Sucursales($event)\">\n          <ion-select-option *ngFor=\"let sucursal of sucursales\" [value]=\"sucursal.idSucursal\">{{sucursal.nombre_suc}}\n          </ion-select-option>\n        </ion-select>\n      </ion-item>\n    </div>\n\n    <br> <br> <br> <br> <br>\n\n    <div *ngIf=\"ocultar2\">\n      <ion-button (click)=\"goToEstablecimiento()\" color=\"dark\" id=\"botonE\">Entrar</ion-button>\n    </div>\n\n  </ion-list>\n\n</ion-content>");

/***/ }),

/***/ "./src/app/component/ciudad/ciudad-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/component/ciudad/ciudad-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: CiudadPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CiudadPageRoutingModule", function() { return CiudadPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ciudad_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ciudad.page */ "./src/app/component/ciudad/ciudad.page.ts");




const routes = [
    {
        path: '',
        component: _ciudad_page__WEBPACK_IMPORTED_MODULE_3__["CiudadPage"]
    }
];
let CiudadPageRoutingModule = class CiudadPageRoutingModule {
};
CiudadPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CiudadPageRoutingModule);



/***/ }),

/***/ "./src/app/component/ciudad/ciudad.module.ts":
/*!***************************************************!*\
  !*** ./src/app/component/ciudad/ciudad.module.ts ***!
  \***************************************************/
/*! exports provided: CiudadPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CiudadPageModule", function() { return CiudadPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _ciudad_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ciudad-routing.module */ "./src/app/component/ciudad/ciudad-routing.module.ts");
/* harmony import */ var _ciudad_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ciudad.page */ "./src/app/component/ciudad/ciudad.page.ts");







let CiudadPageModule = class CiudadPageModule {
};
CiudadPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _ciudad_routing_module__WEBPACK_IMPORTED_MODULE_5__["CiudadPageRoutingModule"]
        ],
        declarations: [_ciudad_page__WEBPACK_IMPORTED_MODULE_6__["CiudadPage"]]
    })
], CiudadPageModule);



/***/ }),

/***/ "./src/app/component/ciudad/ciudad.page.scss":
/*!***************************************************!*\
  !*** ./src/app/component/ciudad/ciudad.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#select {\n  text-align: center;\n  font-size: x-large;\n  font-weight: bold;\n}\n\n#botonE {\n  width: 80%;\n  font-weight: bold;\n  left: 0;\n  right: 0;\n  margin-left: 10%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50L2NpdWRhZC9jaXVkYWQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FBQ0o7O0FBRUE7RUFDSSxVQUFBO0VBQ0EsaUJBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUVBLGdCQUFBO0FBQUoiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnQvY2l1ZGFkL2NpdWRhZC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjc2VsZWN0IHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogeC1sYXJnZTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcblxyXG4jYm90b25FIHtcclxuICAgIHdpZHRoOiA4MCU7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICByaWdodDogMDtcclxuICAgIC8vbWFyZ2luLXRvcDogMzAlO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwJTsgICAgXHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/component/ciudad/ciudad.page.ts":
/*!*************************************************!*\
  !*** ./src/app/component/ciudad/ciudad.page.ts ***!
  \*************************************************/
/*! exports provided: CiudadPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CiudadPage", function() { return CiudadPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_servicio_sucursal_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/servicio/sucursal.service */ "./src/app/servicio/sucursal.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_servicio_notification_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/servicio/notification.service */ "./src/app/servicio/notification.service.ts");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");







// import { async } from '@angular/core/testing';
let CiudadPage = class CiudadPage {
    constructor(sucProv, router, route, loadingCtrl, pushnot, db) {
        this.sucProv = sucProv;
        this.router = router;
        this.route = route;
        this.loadingCtrl = loadingCtrl;
        this.pushnot = pushnot;
        this.db = db;
        this.ocultar1 = false;
        this.ocultar2 = false;
        this.ocultar3 = false;
        this.ciudades = [];
        this.sucursales = [];
        this.municipio = [];
    }
    ngOnInit() {
        let id = this.route.snapshot.paramMap.get("id");
        console.log("este es el idUser:", id);
        this.idUser = id;
        this.AllMunic();
        this.ciudades = [];
        this.sucursales = [];
        this.municipio = [];
    }
    AllMunic() {
        return new Promise(resolve => {
            let body = {
                accion: 'getDataMunicipio',
            };
            this.sucProv.postData(body, 'add_user.php').subscribe(data => {
                for (let munic of data.result) {
                    this.municipio.push(munic);
                    console.log("Total de municipio", this.municipio);
                }
                resolve(true);
            });
        });
    }
    checkEstado(event) {
        console.log("Este es el idMunic", event.detail.value);
        let body = {
            idMunic: event.detail.value,
            idUser: this.idUser,
            accion: 'update_munic'
        };
        this.sucProv.postData(body, 'add_user.php').subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (data.success) {
                console.log("Se ha agregado el municipio");
            }
        }));
        this.AllCiudad(event.detail.value);
    }
    AllCiudad(event) {
        this.ocultar3 = !this.ocultar3;
        return new Promise(resolve => {
            let body = {
                idMunic: event,
                accion: 'getdata',
            };
            this.sucProv.postData(body, 'add_user.php').subscribe(data => {
                for (let ciudad of data.result) {
                    this.ciudades.push(ciudad);
                    console.log("Total de ciudades", this.ciudades);
                }
                resolve(true);
            });
        });
    }
    checkValue(event) {
        console.log(event.detail.value);
        let body = {
            idCiudad: event.detail.value,
            idUser: this.idUser,
            accion: 'update_ciudad'
        };
        this.sucProv.postData(body, 'add_user.php').subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (data.success) {
                console.log("se ha agregado la ciudad");
            }
        }));
        this.sucursal(event.detail.value);
        this.idCiudad = event.detail.value;
    }
    sucursal(ciudad) {
        this.ocultar1 = !this.ocultar1;
        console.log("funcion de sucursal", ciudad);
        return new Promise(resolve => {
            let body = {
                idCiudad: ciudad,
                accion: 'getdata_sucursal',
            };
            this.sucProv.postData(body, 'add_user.php').subscribe(data => {
                for (let sucursal of data.result) {
                    this.sucursales.push(sucursal);
                    console.log("Total de sucursales", this.sucursales);
                }
                resolve(true);
            });
        });
    }
    Sucursales(event) {
        this.ocultar2 = !this.ocultar2;
        console.log(event.detail.value);
        let body = {
            idSucursal: event.detail.value,
            idUser: this.idUser,
            accion: 'update_sucursal'
        };
        this.sucProv.postData(body, 'add_user.php').subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (data.success) {
                console.log("se ha guardado la sucursal");
            }
        }));
        this.idSucursal = event.detail.value;
        // this.goToEstablecimiento();
        this.db.collection("users").doc(this.idUser).update({
            estatusPush: 2
        }).then(() => {
            console.log("Se actualizo");
        });
    }
    goToEstablecimiento() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                cssClass: 'my-custom-class',
                message: 'Iniciando Sesión',
                duration: 2000
            });
            yield loading.present();
            const { role, data } = yield loading.onDidDismiss();
            console.log('Loading dismissed!');
            this.goToFinally();
        });
    }
    goToFinally() {
        this.router.navigate(['/tienda', this.idUser]);
        // this.pushnot.init_notification(this.idUser);
        // this.goToFinal();
        // setTimeout(this.goToFinal,2000);
    }
    goToFinal() {
        this.router.navigate(['/tienda', this.idUser]);
    }
};
CiudadPage.ctorParameters = () => [
    { type: src_app_servicio_sucursal_service__WEBPACK_IMPORTED_MODULE_2__["SucursalService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
    { type: src_app_servicio_notification_service__WEBPACK_IMPORTED_MODULE_5__["NotificationService"] },
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__["AngularFirestore"] }
];
CiudadPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-ciudad',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./ciudad.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/component/ciudad/ciudad.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./ciudad.page.scss */ "./src/app/component/ciudad/ciudad.page.scss")).default]
    })
], CiudadPage);



/***/ }),

/***/ "./src/app/servicio/sucursal.service.ts":
/*!**********************************************!*\
  !*** ./src/app/servicio/sucursal.service.ts ***!
  \**********************************************/
/*! exports provided: SucursalService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SucursalService", function() { return SucursalService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");



//import 'rxjs/add/operator/map';

let SucursalService = class SucursalService {
    //server: string = "http://localhost/server_api_nowaitqr/";
    constructor(http) {
        this.http = http;
        this.server = "https://proyectosinternos.com/NoWaitQR/server_api_nowaitqr/";
    }
    // listar() {
    //   return this.http.get('https://proyectosinternos.com/NoWaitQR/select_ciudad.php/');
    // }
    // getAll(){
    //   return this.http.get<[Usuario]>(this.url);
    // }
    postData(body, file) {
        let type = "application/json; charset=UTF-8";
        let headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({ 'Content-Type': type });
        let options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        return this.http.post(this.server + file, JSON.stringify(body), options)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(res => res.json()));
    }
};
SucursalService.ctorParameters = () => [
    { type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"] }
];
SucursalService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], SucursalService);



/***/ })

}]);
//# sourceMappingURL=component-ciudad-ciudad-module-es2015.js.map