(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["component-login-correo-login-correo-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/component/login-correo/login-correo.page.html":
    /*!*****************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/component/login-correo/login-correo.page.html ***!
      \*****************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentLoginCorreoLoginCorreoPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- <ion-header>\n  <ion-toolbar>\n    <ion-buttons start>\n      <button ion-button icon-only (click)=\"atras()\" id=\"icono\">\n        <ion-icon name=\"arrow-back-circle-outline\"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-content class=\"cointer_inicio\">\n  \n  <ion-buttons start style=\"background-color: #192938;\">\n    <button ion-button icon-only (click)=\"atras()\" id=\"iconos\">\n      <ion-icon name=\"arrow-back-circle-outline\"></ion-icon>\n    </button>\n  </ion-buttons>\n  \n  <br> <br> <br> <br>\n\n  <p id=\"inicio\"> Inicio de Sesión con Correo</p>\n\n  <br> <br> \n\n  <ion-item>\n    <ion-label position=\"floating\" style=\"color: #fff;font-size:larger;\">Correo electrónico</ion-label>\n    <ion-input type=\"text\" [(ngModel)]=\"email\" name=\"email\" style=\"color: #fff;\"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label position=\"floating\" style=\"color: #fff;font-size:larger;\">Contraseña</ion-label>\n    <ion-input type=\"password\" [(ngModel)]=\"password\" name=\"password\" style=\"color: #fff;\"></ion-input>\n  </ion-item>\n\n  <br> <br> <br> <br> <br>\n\n    <ion-button expand=\"block\" padding color=\"dark\" (click)=\"OnSubmitLogin()\">Entrar</ion-button>\n\n    <!-- <ion-button expand=\"block\" padding color=\"dark\" (click)=\"view()\">QR</ion-button> -->\n\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/component/login-correo/login-correo-routing.module.ts":
    /*!***********************************************************************!*\
      !*** ./src/app/component/login-correo/login-correo-routing.module.ts ***!
      \***********************************************************************/

    /*! exports provided: LoginCorreoPageRoutingModule */

    /***/
    function srcAppComponentLoginCorreoLoginCorreoRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginCorreoPageRoutingModule", function () {
        return LoginCorreoPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _login_correo_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./login-correo.page */
      "./src/app/component/login-correo/login-correo.page.ts");

      var routes = [{
        path: '',
        component: _login_correo_page__WEBPACK_IMPORTED_MODULE_3__["LoginCorreoPage"]
      }];

      var LoginCorreoPageRoutingModule = function LoginCorreoPageRoutingModule() {
        _classCallCheck(this, LoginCorreoPageRoutingModule);
      };

      LoginCorreoPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], LoginCorreoPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/component/login-correo/login-correo.module.ts":
    /*!***************************************************************!*\
      !*** ./src/app/component/login-correo/login-correo.module.ts ***!
      \***************************************************************/

    /*! exports provided: LoginCorreoPageModule */

    /***/
    function srcAppComponentLoginCorreoLoginCorreoModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginCorreoPageModule", function () {
        return LoginCorreoPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _login_correo_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./login-correo-routing.module */
      "./src/app/component/login-correo/login-correo-routing.module.ts");
      /* harmony import */


      var _login_correo_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./login-correo.page */
      "./src/app/component/login-correo/login-correo.page.ts");

      var LoginCorreoPageModule = function LoginCorreoPageModule() {
        _classCallCheck(this, LoginCorreoPageModule);
      };

      LoginCorreoPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _login_correo_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginCorreoPageRoutingModule"]],
        declarations: [_login_correo_page__WEBPACK_IMPORTED_MODULE_6__["LoginCorreoPage"]]
      })], LoginCorreoPageModule);
      /***/
    },

    /***/
    "./src/app/component/login-correo/login-correo.page.scss":
    /*!***************************************************************!*\
      !*** ./src/app/component/login-correo/login-correo.page.scss ***!
      \***************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentLoginCorreoLoginCorreoPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "#entrar {\n  background: #b3b3b3;\n  color: white;\n  font-size: x-large;\n}\n\n#icono {\n  background: white;\n  font-size: 30px;\n}\n\n#boton {\n  width: 80%;\n  font-weight: bold;\n  left: 0;\n  right: 0;\n  margin-left: 10%;\n}\n\n#inicio {\n  font-size: x-large;\n  color: #fff;\n  text-align: center;\n  font-weight: bold;\n}\n\n#iconos {\n  background-color: #192938;\n  font-size: 40px;\n  color: #fff;\n}\n\n.cointer_inicio {\n  --ion-background-color:#192938;\n  background-color: #192938;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50L2xvZ2luLWNvcnJlby9sb2dpbi1jb3JyZW8ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFFQTtFQUNJLGlCQUFBO0VBQ0EsZUFBQTtBQUNKOztBQUVBO0VBQ0ksVUFBQTtFQUNBLGlCQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFFQSxnQkFBQTtBQUFKOztBQUdBO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQUFKOztBQUdBO0VBQ0kseUJBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQUFKOztBQUdBO0VBQ0ksOEJBQUE7RUFDQSx5QkFBQTtBQUFKIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50L2xvZ2luLWNvcnJlby9sb2dpbi1jb3JyZW8ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2VudHJhciB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjYjNiM2IzO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiB4LWxhcmdlO1xyXG59XHJcblxyXG4jaWNvbm8ge1xyXG4gICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbn1cclxuXHJcbiNib3RvbiB7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgcmlnaHQ6IDA7XHJcbiAgICAvL21hcmdpbi10b3A6IDMwJTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMCU7ICAgIFxyXG59XHJcblxyXG4jaW5pY2lvIHtcclxuICAgIGZvbnQtc2l6ZTogeC1sYXJnZTtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn1cclxuXHJcbiNpY29ub3Mge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzE5MjkzODtcclxuICAgIGZvbnQtc2l6ZTogNDBweDtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG59XHJcblxyXG4uY29pbnRlcl9pbmljaW97XHJcbiAgICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiMxOTI5Mzg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiMxOTI5Mzg7XHJcbn1cclxuIl19 */";
      /***/
    },

    /***/
    "./src/app/component/login-correo/login-correo.page.ts":
    /*!*************************************************************!*\
      !*** ./src/app/component/login-correo/login-correo.page.ts ***!
      \*************************************************************/

    /*! exports provided: LoginCorreoPage */

    /***/
    function srcAppComponentLoginCorreoLoginCorreoPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginCorreoPage", function () {
        return LoginCorreoPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var src_app_servicio_sucursal_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/servicio/sucursal.service */
      "./src/app/servicio/sucursal.service.ts");
      /* harmony import */


      var src_app_servicio_usuario_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/servicio/usuario.service */
      "./src/app/servicio/usuario.service.ts");
      /* harmony import */


      var _ionic_native_qr_scanner_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic-native/qr-scanner/ngx */
      "./node_modules/@ionic-native/qr-scanner/__ivy_ngcc__/ngx/index.js");

      var LoginCorreoPage = /*#__PURE__*/function () {
        function LoginCorreoPage(authServ, router, loadingCtrl, toastCtrl, sucProv, qrScanner, platform) {
          var _this = this;

          _classCallCheck(this, LoginCorreoPage);

          this.authServ = authServ;
          this.router = router;
          this.loadingCtrl = loadingCtrl;
          this.toastCtrl = toastCtrl;
          this.sucProv = sucProv;
          this.qrScanner = qrScanner;
          this.platform = platform;
          this.usuarios = [];
          this.platform.backButton.subscribeWithPriority(0, function () {
            document.getElementsByTagName('body')[0].style.opacity = '1';

            _this.qrScan.unsubscribe();
          });
        }

        _createClass(LoginCorreoPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.usuarios = [];
          }
        }, {
          key: "OnSubmitLogin",
          value: function OnSubmitLogin() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _this2 = this;

              var toast;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      if (!(this.email != "" && this.password != "")) {
                        _context.next = 5;
                        break;
                      }

                      console.log('Estas en la funcion');
                      this.authServ.login(this.email, this.password).then(function (res) {
                        _this2.authServ.ingresoEmail(_this2.email).subscribe(function (ingreso) {
                          _this2.ingresos = ingreso;
                          console.log("estos son los datos del usuario logueado", _this2.ingresos);

                          _this2.ingresos.forEach(function (element) {
                            var id = element.idUser;
                            var idSucursal = element.sucursal;

                            _this2.router.navigate(['/tienda', id]);

                            _this2.mensaje();

                            localStorage.setItem("uid", id);
                            localStorage.setItem("isLogin", 'true');
                          });
                        });
                      })["catch"](function (err) {
                        alert('hubo un error en correo');

                        _this2.mensaje_error();
                      });
                      _context.next = 10;
                      break;

                    case 5:
                      _context.next = 7;
                      return this.toastCtrl.create({
                        message: 'Los datos estan incompletos',
                        duration: 2000
                      });

                    case 7:
                      toast = _context.sent;
                      _context.next = 10;
                      return toast.present();

                    case 10:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "mensaje",
          value: function mensaje() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var loading, _yield$loading$onDidD, role, data;

              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.loadingCtrl.create({
                        cssClass: 'my-custom-class',
                        message: 'Iniciando Sesión',
                        duration: 2000
                      });

                    case 2:
                      loading = _context2.sent;
                      _context2.next = 5;
                      return loading.present();

                    case 5:
                      _context2.next = 7;
                      return loading.onDidDismiss();

                    case 7:
                      _yield$loading$onDidD = _context2.sent;
                      role = _yield$loading$onDidD.role;
                      data = _yield$loading$onDidD.data;
                      console.log('Loading dismissed!');

                    case 11:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "mensaje_error",
          value: function mensaje_error() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var toast;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      _context3.next = 2;
                      return this.toastCtrl.create({
                        message: 'Correo o Contraseña Invalidos, vuelva a intentarlo',
                        duration: 4000
                      });

                    case 2:
                      toast = _context3.sent;
                      _context3.next = 5;
                      return toast.present();

                    case 5:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "atras",
          value: function atras() {
            this.router.navigate(['/login']);
          }
        }, {
          key: "view",
          value: function view() {
            var _this3 = this;

            // Optionally request the permission early
            this.qrScanner.prepare().then(function (status) {
              if (status.authorized) {
                // camera permission was granted
                // start scanning
                var scanSub = _this3.qrScanner.scan().subscribe(function (text) {
                  console.log('Scanned something', text);
                  alert("precion");

                  _this3.qrScanner.hide(); // hide camera preview


                  scanSub.unsubscribe(); // stop scanning
                });

                _this3.qrScanner.show();

                document.getElementsByTagName('body')[0].style.opacity = '0';
                _this3.qrScan = _this3.qrScanner.scan().subscribe(function (textFound) {
                  document.getElementsByTagName("body")[0].style.opacity = "1";

                  _this3.qrScanner.hide();

                  _this3.qrScan.unsubscribe();

                  _this3.qrText = textFound;
                  alert("precion configuracion");
                }), function (err) {
                  alert(JSON.stringify(err));
                };
              } else if (status.denied) {// camera permission was permanently denied
                // you must use QRScanner.openSettings() method to guide the user to the settings page
                // then they can grant the permission from there
              } else {// permission was denied, but not permanently. You can ask for permission again at a later time.
                }
            })["catch"](function (e) {
              return console.log('Error is', e);
            });
          }
        }]);

        return LoginCorreoPage;
      }();

      LoginCorreoPage.ctorParameters = function () {
        return [{
          type: src_app_servicio_usuario_service__WEBPACK_IMPORTED_MODULE_5__["UsuarioService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]
        }, {
          type: src_app_servicio_sucursal_service__WEBPACK_IMPORTED_MODULE_4__["SucursalService"]
        }, {
          type: _ionic_native_qr_scanner_ngx__WEBPACK_IMPORTED_MODULE_6__["QRScanner"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"]
        }];
      };

      LoginCorreoPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login-correo',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./login-correo.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/component/login-correo/login-correo.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./login-correo.page.scss */
        "./src/app/component/login-correo/login-correo.page.scss"))["default"]]
      })], LoginCorreoPage);
      /***/
    },

    /***/
    "./src/app/servicio/sucursal.service.ts":
    /*!**********************************************!*\
      !*** ./src/app/servicio/sucursal.service.ts ***!
      \**********************************************/

    /*! exports provided: SucursalService */

    /***/
    function srcAppServicioSucursalServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SucursalService", function () {
        return SucursalService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/http */
      "./node_modules/@angular/http/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js"); //import 'rxjs/add/operator/map';


      var SucursalService = /*#__PURE__*/function () {
        //server: string = "http://localhost/server_api_nowaitqr/";
        function SucursalService(http) {
          _classCallCheck(this, SucursalService);

          this.http = http;
          this.server = "https://proyectosinternos.com/NoWaitQR/server_api_nowaitqr/";
        } // listar() {
        //   return this.http.get('https://proyectosinternos.com/NoWaitQR/select_ciudad.php/');
        // }
        // getAll(){
        //   return this.http.get<[Usuario]>(this.url);
        // }


        _createClass(SucursalService, [{
          key: "postData",
          value: function postData(body, file) {
            var type = "application/json; charset=UTF-8";
            var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
              'Content-Type': type
            });
            var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({
              headers: headers
            });
            return this.http.post(this.server + file, JSON.stringify(body), options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
              return res.json();
            }));
          }
        }]);

        return SucursalService;
      }();

      SucursalService.ctorParameters = function () {
        return [{
          type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]
        }];
      };

      SucursalService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], SucursalService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=component-login-correo-login-correo-module-es5.js.map