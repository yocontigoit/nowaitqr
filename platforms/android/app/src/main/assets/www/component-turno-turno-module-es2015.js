(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["component-turno-turno-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/component/turno/turno.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/component/turno/turno.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content padding>\n  \n  <div id=\"container\">\n    <img src=\"./assets/icon/new_disigner_small.jpg\" width=\"50%\" style=\"margin-left: 25%;\" alt=\"\">\n  <br>\n  </div>\n\n  <!-- <div id=\"nombrePro\"><b id=\"frase\">No Wait</b> QR</div> -->\n\n  <ion-card id=\"cuadro\">\n    <ion-card-header>\n    </ion-card-header>\n\n    <ion-card-content>\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card id=\"cuadroTwo\">\n    <ion-card-header>\n    </ion-card-header>\n\n    <ion-card-content>\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card id=\"cuadroThree\">\n    <ion-card-header>\n    </ion-card-header>\n\n    <ion-card-content>\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card id=\"cuadroFour\">\n    <ion-card-header>\n      <div *ngFor=\"let tienda of tiendaSec\">\n        <p id=\"nombre\">{{tienda.nombre_tienda}}</p>\n      </div>\n      <div *ngFor=\"let suc of sucursalSec\">\n        <p id=\"nombre_ti\">{{suc.nombre_suc}}</p>\n      </div>\n    </ion-card-header>\n\n    <ion-card-content>\n      <div *ngFor=\"let turno of turnoSel\">\n        <div *ngIf=\"turno.estatus == 'pendiente'\">\n          <p id=\"turno\">Tu Lugar es el:</p>\n          <p id=\"numTurno\">000{{numTurno}}</p>\n          <br>\n          <p id=\"personas\">Personas antes de ti:</p>\n          <p id=\"numper\">{{total - 1}}</p>\n          <p id=\"personas\">Tiempo estimado:</p>\n          <p id=\"numpers\">{{(total - 1) * 5}} min</p>\n        </div>\n        <div *ngIf=\"turno.estatus == 'activado'\">\n          <br>\n          <p id=\"bienvenido\">Bienvenido</p>\n          <br>\n          <p id=\"disfruta\">Disfruta tu estancia</p>\n        </div>\n      </div>\n    </ion-card-content>\n  </ion-card>\n\n  <!-- <div *ngFor=\"let turno of turnoSel\">\n    <div *ngIf=\"turno.estatus == 'pendiente'\">\n      <ion-button id=\"botonc\" (click)=\"refreshPage()\" color=\"dark\">\n        Actualizar Lista\n      </ion-button>\n    </div>\n  </div> -->\n\n  <div id=\"imgfo\">\n    <p id=\"texto\">Escanea otra tienda, banco o servicios para iniciar tu fila</p>\n    <img src=\"./assets/icon/ejemplo.png\" (click)=\"goToEstablecimiento()\" alt=\"\">\n  </div>\n\n</ion-content>");

/***/ }),

/***/ "./src/app/component/turno/turno-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/component/turno/turno-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: TurnoPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TurnoPageRoutingModule", function() { return TurnoPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _turno_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./turno.page */ "./src/app/component/turno/turno.page.ts");




const routes = [
    {
        path: '',
        component: _turno_page__WEBPACK_IMPORTED_MODULE_3__["TurnoPage"]
    }
];
let TurnoPageRoutingModule = class TurnoPageRoutingModule {
};
TurnoPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], TurnoPageRoutingModule);



/***/ }),

/***/ "./src/app/component/turno/turno.module.ts":
/*!*************************************************!*\
  !*** ./src/app/component/turno/turno.module.ts ***!
  \*************************************************/
/*! exports provided: TurnoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TurnoPageModule", function() { return TurnoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _turno_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./turno-routing.module */ "./src/app/component/turno/turno-routing.module.ts");
/* harmony import */ var _turno_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./turno.page */ "./src/app/component/turno/turno.page.ts");







let TurnoPageModule = class TurnoPageModule {
};
TurnoPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _turno_routing_module__WEBPACK_IMPORTED_MODULE_5__["TurnoPageRoutingModule"]
        ],
        declarations: [_turno_page__WEBPACK_IMPORTED_MODULE_6__["TurnoPage"]]
    })
], TurnoPageModule);



/***/ }),

/***/ "./src/app/component/turno/turno.page.scss":
/*!*************************************************!*\
  !*** ./src/app/component/turno/turno.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#cuadro {\n  background-color: black;\n  height: 350px;\n  width: 300px;\n  border-radius: 25px;\n  margin-left: 5%;\n  position: fixed;\n}\n\n#cuadroTwo {\n  background-color: #797979;\n  height: 350px;\n  width: 300px;\n  border-radius: 25px;\n  margin-top: 17%;\n  margin-left: 10%;\n  position: fixed;\n}\n\n#cuadroThree {\n  background-color: #484646;\n  height: 350px;\n  width: 300px;\n  border-radius: 25px;\n  margin-top: 27%;\n  margin-left: 15%;\n  position: fixed;\n}\n\n#cuadroFour {\n  background-color: #b3b3b3;\n  height: 350px;\n  width: 300px;\n  border-radius: 25px;\n  margin-top: 37%;\n  margin-left: 10%;\n  position: fixed;\n}\n\n#nombre {\n  font-weight: bold;\n  font-size: x-large;\n  color: black;\n  text-align: center;\n  text-transform: uppercase;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n#nombre_ti {\n  text-align: center;\n  margin-top: -15px;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n#botonc {\n  width: 80%;\n  font-size: 71%;\n  font-weight: bold;\n  margin-left: 10%;\n}\n\n#turno {\n  font-size: x-large;\n  text-align: center;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n#numTurno {\n  font-size: xx-large;\n  text-align: center;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n#personas {\n  font-size: medium;\n  text-align: center;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n#numper {\n  text-align: center;\n  font-size: larger;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n#numpers {\n  text-align: center;\n  font-size: x-large;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n#imgfo {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  /* top: 50%; */\n  transform: translateY(500%);\n  position: fixed;\n}\n\n#texto {\n  text-align: center;\n  font-size: large;\n  color: #777777;\n  position: fixed;\n  margin-top: -35px;\n  margin-left: 20px;\n  margin-right: 20px;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n#nombrePro {\n  font-size: xx-large;\n  color: #676767;\n  font-family: Arial, Helvetica, sans-serif;\n  text-align: center;\n}\n\n#bienvenido {\n  font-size: xx-large;\n  text-align: center;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n#disfruta {\n  font-size: x-large;\n  text-align: center;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n#frase {\n  font-weight: bold;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n#botonc {\n  width: 80%;\n  font-size: 71%;\n  font-weight: bold;\n  margin-left: 10%;\n  position: fixed;\n  margin-top: 135%;\n}\n\n.footer-ios ion-toolbar:first-of-type {\n  --border-width: 0px 0 0;\n}\n\n#container {\n  left: 0;\n  right: 0;\n  --ion-background-color:#192938;\n  background-color: #192938;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50L3R1cm5vL3R1cm5vLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHVCQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBRUE7RUFDSSx5QkFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBRUE7RUFDSSx5QkFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBRUE7RUFDSSx5QkFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBRUE7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSx5Q0FBQTtBQUNKOztBQUVBO0VBQ0ksa0JBQUE7RUFDQSxpQkFBQTtFQUNBLHlDQUFBO0FBQ0o7O0FBRUE7RUFDSSxVQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSx5Q0FBQTtBQUNKOztBQUVBO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLHlDQUFBO0FBQ0o7O0FBRUE7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EseUNBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSx5Q0FBQTtBQUNKOztBQUVBO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtFQUNBLHlDQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxjQUFBO0VBQ0EsMkJBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSx5Q0FBQTtBQUNKOztBQUVBO0VBQ0ksbUJBQUE7RUFDQSxjQUFBO0VBQ0EseUNBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLHlDQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EseUNBQUE7QUFDSjs7QUFFQTtFQUNJLGlCQUFBO0VBQ0EseUNBQUE7QUFDSjs7QUFFQTtFQUNJLFVBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQUNKOztBQUVBO0VBQ0ksdUJBQUE7QUFDSjs7QUFFQTtFQUNJLE9BQUE7RUFDQSxRQUFBO0VBQ0EsOEJBQUE7RUFDQSx5QkFBQTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50L3R1cm5vL3R1cm5vLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNjdWFkcm97XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxuICAgIGhlaWdodDogMzUwcHg7XHJcbiAgICB3aWR0aDogMzAwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDUlO1xyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG59XHJcblxyXG4jY3VhZHJvVHdve1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzc5Nzk3OTtcclxuICAgIGhlaWdodDogMzUwcHg7XHJcbiAgICB3aWR0aDogMzAwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgbWFyZ2luLXRvcDogMTclO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwJTtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxufVxyXG5cclxuI2N1YWRyb1RocmVlIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM0ODQ2NDY7XHJcbiAgICBoZWlnaHQ6IDM1MHB4O1xyXG4gICAgd2lkdGg6IDMwMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgIG1hcmdpbi10b3A6IDI3JTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxNSU7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbn1cclxuXHJcbiNjdWFkcm9Gb3VyIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNiM2IzYjM7XHJcbiAgICBoZWlnaHQ6IDM1MHB4O1xyXG4gICAgd2lkdGg6IDMwMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgIG1hcmdpbi10b3A6IDM3JTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMCU7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbn1cclxuXHJcbiNub21icmUge1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBmb250LXNpemU6IHgtbGFyZ2U7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgZm9udC1mYW1pbHk6IEFyaWFsLCBIZWx2ZXRpY2EsIHNhbnMtc2VyaWY7XHJcbn1cclxuXHJcbiNub21icmVfdGkge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbWFyZ2luLXRvcDogLTE1cHg7XHJcbiAgICBmb250LWZhbWlseTogQXJpYWwsIEhlbHZldGljYSwgc2Fucy1zZXJpZjtcclxufVxyXG5cclxuI2JvdG9uYyB7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICAgZm9udC1zaXplOiA3MSU7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMCU7XHJcbn1cclxuXHJcbiN0dXJub3tcclxuICAgIGZvbnQtc2l6ZTogeC1sYXJnZTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtZmFtaWx5OiBBcmlhbCwgSGVsdmV0aWNhLCBzYW5zLXNlcmlmO1xyXG59XHJcblxyXG4jbnVtVHVybm97XHJcbiAgICBmb250LXNpemU6IHh4LWxhcmdlO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1mYW1pbHk6IEFyaWFsLCBIZWx2ZXRpY2EsIHNhbnMtc2VyaWY7XHJcbn1cclxuXHJcbiNwZXJzb25hc3tcclxuICAgIGZvbnQtc2l6ZTogbWVkaXVtO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1mYW1pbHk6IEFyaWFsLCBIZWx2ZXRpY2EsIHNhbnMtc2VyaWY7XHJcbn1cclxuXHJcbiNudW1wZXJ7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IGxhcmdlcjtcclxuICAgIGZvbnQtZmFtaWx5OiBBcmlhbCwgSGVsdmV0aWNhLCBzYW5zLXNlcmlmO1xyXG59XHJcblxyXG4jbnVtcGVyc3tcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogeC1sYXJnZTtcclxuICAgIGZvbnQtZmFtaWx5OiBBcmlhbCwgSGVsdmV0aWNhLCBzYW5zLXNlcmlmO1xyXG59XHJcblxyXG4jaW1nZm8ge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgbGVmdDogMDtcclxuICAgIHJpZ2h0OiAwO1xyXG4gICAgLyogdG9wOiA1MCU7ICovXHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoNTAwJSk7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgfVxyXG5cclxuI3RleHRve1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiBsYXJnZTtcclxuICAgIGNvbG9yOiAjNzc3Nzc3O1xyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgbWFyZ2luLXRvcDogLTM1cHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMjBweDtcclxuICAgIG1hcmdpbi1yaWdodDogMjBweDtcclxuICAgIGZvbnQtZmFtaWx5OiBBcmlhbCwgSGVsdmV0aWNhLCBzYW5zLXNlcmlmO1xyXG59XHJcblxyXG4jbm9tYnJlUHJvIHtcclxuICAgIGZvbnQtc2l6ZTogeHgtbGFyZ2U7XHJcbiAgICBjb2xvcjogIzY3Njc2NztcclxuICAgIGZvbnQtZmFtaWx5OiBBcmlhbCwgSGVsdmV0aWNhLCBzYW5zLXNlcmlmO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4jYmllbnZlbmlkb3tcclxuICAgIGZvbnQtc2l6ZTogeHgtbGFyZ2U7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LWZhbWlseTogQXJpYWwsIEhlbHZldGljYSwgc2Fucy1zZXJpZjtcclxufVxyXG5cclxuI2Rpc2ZydXRhe1xyXG4gICAgZm9udC1zaXplOiB4LWxhcmdlO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1mYW1pbHk6IEFyaWFsLCBIZWx2ZXRpY2EsIHNhbnMtc2VyaWY7XHJcbn1cclxuICBcclxuI2ZyYXNlIHtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgZm9udC1mYW1pbHk6IEFyaWFsLCBIZWx2ZXRpY2EsIHNhbnMtc2VyaWY7XHJcbn1cclxuXHJcbiNib3RvbmN7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICAgZm9udC1zaXplOiA3MSU7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMCU7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICBtYXJnaW4tdG9wOiAxMzUlO1xyXG59XHJcblxyXG4uZm9vdGVyLWlvcyBpb24tdG9vbGJhcjpmaXJzdC1vZi10eXBlIHtcclxuICAgIC0tYm9yZGVyLXdpZHRoOiAwcHggMCAwO1xyXG59XHJcblxyXG4jY29udGFpbmVye1xyXG4gICAgbGVmdDogMDtcclxuICAgIHJpZ2h0OiAwO1xyXG4gICAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjojMTkyOTM4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzE5MjkzODtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/component/turno/turno.page.ts":
/*!***********************************************!*\
  !*** ./src/app/component/turno/turno.page.ts ***!
  \***********************************************/
/*! exports provided: TurnoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TurnoPage", function() { return TurnoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var src_app_servicio_sucursal_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/servicio/sucursal.service */ "./src/app/servicio/sucursal.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);





let TurnoPage = class TurnoPage {
    constructor(router, route, sucProv) {
        this.router = router;
        this.route = route;
        this.sucProv = sucProv;
        this.tiendaSec = [];
        this.sucursalSec = [];
        this.turnoSel = [];
        this.allUser = [];
        this.myUsers = [];
    }
    ngOnInit() {
        let id = this.route.snapshot.paramMap.get("id");
        console.log("este es el idUser:", id);
        this.idUser = id;
        let idTienda = this.route.snapshot.paramMap.get("idTienda");
        console.log("este es el idTienda:", idTienda);
        this.idTienda = parseInt(idTienda);
        let sumTurno = this.route.snapshot.paramMap.get("sumTurno");
        console.log("Este es el numero de turno", sumTurno);
        this.sumTurn = parseInt(sumTurno);
        var today = moment__WEBPACK_IMPORTED_MODULE_4__().format('YYYY-MM-DD');
        console.log("variable today", today);
        this.momentos = today;
        console.log("variable momentos", this.momentos);
        this.tiendaSec = [];
        this.sucursalSec = [];
        this.turnoSel = [];
        this.allUser = [];
        this.myUsers = [];
        this.goToInfoTienda();
        this.goToTurno();
        this.Alluser();
        // this.myUser();
        this.ConsComTienda();
    }
    goToInfoTienda() {
        return new Promise(resolve => {
            let body = {
                idTienda: this.idTienda,
                accion: 'getInfoTienda'
            };
            this.sucProv.postData(body, 'establecimiento.php').subscribe(data => {
                for (let tienda of data.result) {
                    this.tiendaSec.push(tienda);
                    this.idSucursal = parseInt(tienda.idSucursal);
                    this.goToInformation();
                    console.log("Datos de la tienda seleccionada", this.tiendaSec);
                }
                resolve(true);
            });
        });
    }
    goToInformation() {
        return new Promise(resolve => {
            let body = {
                idSucursal: this.idSucursal,
                accion: 'getInfoSuc'
            };
            this.sucProv.postData(body, 'establecimiento.php').subscribe(data => {
                for (let sucursal of data.result) {
                    this.sucursalSec.push(sucursal);
                    console.log("Datos de la sucursal seleccionada", this.sucursalSec);
                }
                resolve(true);
            });
        });
    }
    goToTurno() {
        return new Promise(resolve => {
            let body = {
                idTienda: this.idTienda,
                idUser: this.idUser,
                sumTurno: this.sumTurn,
                accion: 'getInfoTurno'
            };
            this.sucProv.postData(body, 'turno.php').subscribe(data => {
                for (let turno of data.result) {
                    this.turnoSel.push(turno);
                    console.log("Datos del turno", this.turnoSel);
                    console.log("Numero de personas en la tienda", this.turnoSel.length);
                    this.numTurno = turno.numTurno;
                    if (turno.estatus == 'activado') {
                        this.router.navigate(['/tienda', this.idUser]);
                    }
                }
                resolve(true);
            });
        });
    }
    Alluser() {
        return new Promise(resolve => {
            let body = {
                idTienda: this.idTienda,
                fecha: this.momentos,
                estatus: 'pendiente',
                accion: 'getAllUser'
            };
            this.sucProv.postData(body, 'turno.php').subscribe(data => {
                for (let all of data.result) {
                    this.allUser.push(all);
                    console.log("Datos de los usuarios en tienda", this.allUser);
                    this.total = parseInt(this.allUser.length);
                    console.log("total de usuarios en espera", this.total);
                }
                resolve(true);
            });
        });
    }
    goToEstablecimiento() {
        this.router.navigate(['/tienda', this.idUser]);
    }
    refreshPage() {
        this.ngOnInit();
    }
    ConsComTienda() {
        return new Promise(resolve => {
            let body = {
                idUser: this.idUser,
                idTienda: this.idTienda,
                accion: 'getComTienda'
            };
            this.sucProv.postData(body, 'add_formulario.php').subscribe(data => {
                if (data.result == 0) {
                    console.log("si cuenta y es 0");
                    this.myUser();
                }
                resolve(true);
            });
        });
    }
    myUser() {
        return new Promise(resolve => {
            let body = {
                idUser: this.idUser,
                accion: 'getDataMyuser'
            };
            this.sucProv.postData(body, 'add_user.php').subscribe(data => {
                for (let myUser of data.result) {
                    this.myUsers.push(myUser);
                    console.log("Estos son los datos del usuario", this.myUsers);
                    console.log("Datos del formulario", myUser.formulario);
                    if (myUser.formulario == 0) {
                        let body = {
                            formulario: 1,
                            idUser: this.idUser,
                            accion: 'getUpdateFormUser'
                        };
                        this.sucProv.postData(body, 'add_user.php').subscribe(data => {
                            if (data.success) {
                                console.log("Se ha modificado el estatus formulario");
                            }
                        });
                    }
                }
                resolve(true);
            });
        });
    }
};
TurnoPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: src_app_servicio_sucursal_service__WEBPACK_IMPORTED_MODULE_3__["SucursalService"] }
];
TurnoPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-turno',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./turno.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/component/turno/turno.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./turno.page.scss */ "./src/app/component/turno/turno.page.scss")).default]
    })
], TurnoPage);



/***/ })

}]);
//# sourceMappingURL=component-turno-turno-module-es2015.js.map