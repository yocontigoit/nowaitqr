// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
};

export const firebaseConfig = {
  apiKey: "AIzaSyCRmj3FQtOAxQWkAK8eyn9llGutqDTjvsc",
  authDomain: "nowaitqr.firebaseapp.com",
  databaseURL: "https://nowaitqr.firebaseio.com",
  projectId: "nowaitqr",
  storageBucket: "nowaitqr.appspot.com",
  messagingSenderId: "307577600417",
  //appId: "1:307577600417:web:ac2d48f8df089c0609f3e0",
  //measurementId: "G-ENT3G2M3PG"
}
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
