import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Http, RequestOptions, Headers } from '@angular/http';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  userId: any = '';

  constructor(
    private oneSignal: OneSignal,
    public platform: Platform,
    public db: AngularFirestore,
    private http: Http
  ) { }

  init_notification(idUser) {

    if (this.platform.is('cordova')) {
      this.oneSignal.startInit('2ce12b47-8282-4956-99af-d75d626562f9', '307577600417');

      this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

      this.oneSignal.handleNotificationReceived().subscribe(() => {
        // do something when notification is received
      });

      this.oneSignal.handleNotificationOpened().subscribe(() => {
        // do something when a notification is opened
      });

      this.oneSignal.endInit();

      this.oneSignal.getIds().then(data => {
        // alert('Data :' + JSON.stringify(data));
        const uidUser = localStorage.getItem("uid");
        const playerID = data.userId;

        this.db.collection("users").doc(idUser).update({
          playerID: playerID,
          estatus: 1,
          estatusPush: 1
        }).then(() => {
          console.log("Se actualizo");
          this.continue(idUser, 1, playerID);
        });

      });

    }
    else{
      console.log("OneSignal solo funciona en el dispositivo");  
    }

  }

  continue(idUser, estatus, playerID){
    console.log("Si llega a la funcion continue");

    let data = JSON.stringify({
      playerID: playerID,
      idUser: idUser,
      estatus: estatus,
      accion: "updateplayId"
    });

    let url = 'https://proyectosinternos.com/NoWaitQR/server_api_nowaitqr/add_user.php/';
    //let url = "http://localhost/server_api_nowaitqrEstablecimiento/acceso.php";
    let type = "application/json; charset=UTF-8";
    let headers = new Headers({ "Content-Type": type });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, data, options).subscribe(res => {
    });

  }

}
