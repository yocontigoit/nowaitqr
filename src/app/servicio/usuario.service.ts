import { Injectable } from '@angular/core';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import * as firebase from 'firebase/app';
import "firebase/database";
//import { auth } from 'firebase';
import 'firebase/auth';
import 'firebase/firestore';
import { FirebaseApp } from '@angular/fire';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  usuario: Credenciales = {};
  codigo: AngularFirestoreCollection<any[]>;
  _codigo: Observable<any>;

  us: any;
  url: any;
  usPhotoUrl: any;

  logueoE: AngularFirestoreCollection<any[]>;
  _logueoE: Observable<any>;

  logueoG: AngularFirestoreCollection<any[]>;
  _logueoG: Observable<any>;


  constructor(
    // public afs: AngularFirestore,
    private google: GooglePlus,
    private Afauth: AngularFireAuth,
    public db: AngularFirestore,
    public http: Http
  ) { }

  createEmail(nombre: string, email: string, password: string) {

    return this.Afauth.createUserWithEmailAndPassword(email, password).then((user: any) => {
      localStorage.setItem("isLogin", 'true');
      this.us = user.user;
      console.log('Usuario: ', JSON.stringify(this.us));
      console.log("contraseña", this.us.password);
      localStorage.setItem("uid", this.us.uid);

      this.db.collection("users").doc(this.us.uid).set({
        idUser: this.us.uid,
        correo: email,
        displayName: nombre,
        estatus: 0
      })
        .then(() => console.log("El usuario se creo correctamente"))
        .catch(function (error) {
          console.error("Error adding document: ", error);
        });

      let data = JSON.stringify({
        displayName: nombre,
        email: email,
        idUser: this.us.uid,
        accion: "add_user"
      });

      let url = 'https://proyectosinternos.com/NoWaitQR/server_api_nowaitqr/add_user.php/';
      //let url = "http://localhost/server_api_nowaitqr/add_user.php";
      let type = "application/json; charset=UTF-8";
      let headers = new Headers({ "Content-Type": type });
      let options = new RequestOptions({ headers: headers });
      return this.http.post(url, data, options).subscribe(res => {
      });
    })
  }


  login(email: string, password: string) {
    return this.Afauth.signInWithEmailAndPassword(email, password).then(res => {
      console.log(res);
    }).catch(err => console.log('error amiga', err))
  }

  ingresoEmail(email: string) {
    this.logueoE = this.db.collection<any>('users', ref =>
      ref.where('correo', '==', email));
    this._logueoE = this.logueoE.valueChanges();

    return (this._logueoE = this.logueoE.snapshotChanges().pipe(
      map(changes => {
        return changes.map(action => {
          const data = action.payload.doc.data() as any;
          data.$key = action.payload.doc.id;
          return data;
        });
      })
    ));
  }


  cargarUsuario(nombre: string,
    email: string,
    imagen: string,
    uid: string) {
    this.usuario.nombre = nombre;
    this.usuario.email = email;
    this.usuario.imagen = imagen;
    this.usuario.uid = uid;
  }

  cargarUsuarioGmail(nombre: string,
    email: string,
    imagen: string,
    uid: string) {
    this.usuario.nombre = nombre;
    this.usuario.email = email;
    this.usuario.imagen = imagen;
    this.usuario.uid = uid;
  }


  ingresoGmail(idUser: string) {
    this.logueoG = this.db.collection<any>('users', ref =>
      ref.where('idUser', '==', idUser));
    this._logueoG = this.logueoG.valueChanges();

    return (this._logueoG = this.logueoG.snapshotChanges().pipe(
      map(changes => {
        return changes.map(action => {
          const data = action.payload.doc.data() as any;
          data.$key = action.payload.doc.id;
          return data;
        });
      })
    ));
  }


  add_UserGmail(nombre: string, email: string, uid: string) {
    let data = JSON.stringify({
      displayName: nombre,
      email: email,
      idUser: uid,
      accion: "add_user"
    });

    let url = 'https://proyectosinternos.com/NoWaitQR/server_api_nowaitqr/add_user.php/';
    //let url = "http://localhost/server_api_nowaitqr/add_user.php";
    let type = "application/json; charset=UTF-8";
    let headers = new Headers({ "Content-Type": type });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, data, options).subscribe(res => {
    });
  }


  public getCodigo(idx) {
    this.codigo = this.db.collection<any>("users", ref =>
      ref.where("idUser", "==", idx)
    );
    this._codigo = this.codigo.valueChanges();
    return (this._codigo = this.codigo.snapshotChanges().pipe(
      map(changes => {
        return changes.map(action => {
          const data = action.payload.doc.data() as any;
          data.$key = action.payload.doc.id;
          return data;
        });
      })
    ));
  }



  // loginwithfacebook(){
  //   return this.fb.login(['email', 'public_profile']).then((response: FacebookLoginResponse) => 
  //     {
  //       console.log('Logged into Facebook!', response)
  //       const credencial_fb = auth.FacebookAuthProvider.credential(response.authResponse.accessToken);
  //       return this.Afauth.auth.signInWithCredential(credencial_fb);
  //     })
  // }

  // loginwithGoogle(){
  //   return this.google.login({}).then(result => {
  //     const user_data_google = result; //Datos que devuelve google

  //     return this.Afauth.auth.signInWithCredential(auth.GoogleAuthProvider.credential(null, user_data_google.accessToken))
  //   })
  // }



}

export interface Credenciales {
  nombre?: string;
  email?: string;
  imagen?: string;
  uid?: string;
}

export interface Credenciales {
  nombre?: string;
  email?: string;
  imagen?: string;
  uid?: string;
}