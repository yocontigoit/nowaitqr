import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Http, RequestOptions, Headers } from '@angular/http';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class PushnotificationService {

  userId: any = '';

  constructor(
    private oneSignal: OneSignal,
    public platform: Platform,
    public db: AngularFirestore,
    private http: Http
  ) { }

  init_notification() {

    if (this.platform.is('cordova')) {
      this.oneSignal.startInit('2ce12b47-8282-4956-99af-d75d626562f9', '307577600417');

      this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

      this.oneSignal.handleNotificationReceived().subscribe(() => {
        // do something when notification is received
      });

      this.oneSignal.handleNotificationOpened().subscribe(() => {
        // do something when a notification is opened
      });

      this.oneSignal.endInit();
    }
    else{
      console.log("OneSignal solo funciona en el dispositivo");  
    }

  }

  init_notifications(idUser) {

    if (this.platform.is('cordova')) {
      this.oneSignal.startInit('2ce12b47-8282-4956-99af-d75d626562f9', '307577600417');

      this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

      this.oneSignal.handleNotificationReceived().subscribe(() => {
        // do something when notification is received
      });

      this.oneSignal.handleNotificationOpened().subscribe(() => {
        // do something when a notification is opened
      });

      this.oneSignal.endInit();

      this.oneSignal.getIds().then(data => {
        // alert('Data :' + JSON.stringify(data));
        const uidUser = localStorage.getItem("uid");
        const playerID = data.userId;

        this.db.collection("users").doc(idUser).update({
          playerID: playerID
        }).then(() => {
          console.log("Se actualizo");
          this.continue(playerID, idUser);
        });

      });

    }
    else{
      console.log("OneSignal solo funciona en el dispositivo");  
    }

  }


  continue(playerID, idUser){
    console.log("Si llega a la funcion continue");

    let data = JSON.stringify({
      playerID: playerID,
      idUser: idUser,
      accion: "updateplayId"
    });

    let url = 'https://proyectosinternos.com/NoWaitQR/server_api_nowaitqr/add_user.php/';
    //let url = "http://localhost/server_api_nowaitqr/acceso.php";
    let type = "application/json; charset=UTF-8";
    let headers = new Headers({ "Content-Type": type });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, data, options).subscribe(res => {
    });

  }

}
