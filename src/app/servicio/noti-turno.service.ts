import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class NotiTurnoService {

  constructor(
    public http: Http
  ) { }

  insertaNotificacion(playerUser) {
  
    let data = JSON.stringify({
      titulo: 'Nuevo Cliente',
      descripcion: 'Se encunetra un nuevo cliente esperando en la fila',
      idUsers: playerUser
    });

    // let url = 'https://proyectosinternos.com/server_ez/notiAdmin.php/';
    let url = 'https://proyectosinternos.com/NoWaitQR_Notificacion/notificacion.php/';
    let type = "application/json; charset=UTF-8";
    let headers = new Headers({ "Content-Type": type });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, data, options).subscribe(res => {
      // console.log('Que pasa ', res._body)

    });

  }
}
