import { TestBed } from '@angular/core/testing';

import { NotiTurnoService } from './noti-turno.service';

describe('NotiTurnoService', () => {
  let service: NotiTurnoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NotiTurnoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
