import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
//import 'rxjs/add/operator/map';
import { map } from "rxjs/operators";

export interface Usuario {
  idUser: string;
  displayName: string;
  email: string;
}

@Injectable({
  providedIn: 'root'
})

export class SucursalService {

  server: string = "https://proyectosinternos.com/NoWaitQR/server_api_nowaitqr/";
  //server: string = "http://localhost/server_api_nowaitqr/";


  constructor(
    public http: Http,
  ) { }

  // listar() {
  //   return this.http.get('https://proyectosinternos.com/NoWaitQR/select_ciudad.php/');
  // }

  // getAll(){
  //   return this.http.get<[Usuario]>(this.url);
  // }

  postData(body, file){
		let type = "application/json; charset=UTF-8";
		let headers = new Headers({ 'Content-Type': type });
		let options = new RequestOptions({ headers: headers });

		return this.http.post(this.server + file, JSON.stringify(body), options)
		.pipe(map(res => res.json()));
	}


}
