import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { UsuarioService } from 'src/app/servicio/usuario.service';
import * as firebase from 'firebase/app';
import "firebase/database";
//import { auth } from 'firebase';
import 'firebase/auth';
import 'firebase/firestore';
import { FirebaseApp } from '@angular/fire';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { AngularFirestore } from '@angular/fire/firestore';
import { NotificationService } from 'src/app/servicio/notification.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  nombre: string;
  email: string;
  password: string;

  us: any;
  url: any;
  usPhotoUrl: any;

  codigos: any;
  ingresos: any;

  constructor(
    private authServ: UsuarioService,
    private router: Router,
    private fb: Facebook,
    public loadingCtrl: LoadingController,
    public db: AngularFirestore,
    public toastCtrl: ToastController,
    private pushnot: NotificationService
  ) { }

  ngOnInit() {
  }

  segmentChanged($event) { console.log($event) }

  async OnCreateLogin() {
    if (this.nombre != "" && this.email != "" && this.password != "") {

      this.authServ.createEmail(this.nombre, this.email, this.password).then(res => {
        console.log("datos del then", res);

        this.authServ.ingresoEmail(this.email).subscribe(ingreso => {
          this.ingresos = ingreso;
          console.log("estos son los datos del usuario logueado", this.ingresos);

          this.ingresos.forEach(element => {
            const id = element.idUser
            const estatus = element.estatus

            if (estatus == 1) {
              this.router.navigate(['/ciudad', id]);
              this.mensaje();
              localStorage.setItem("uid", id);
              localStorage.setItem("isLogin", 'true');
            }
            if (estatus == 0) {
              this.pushnot.init_notification(id);
              localStorage.setItem("uid", id);
              localStorage.setItem("isLogin", 'true');
              // this.pushnot.continue('0dGzNzHl4NRS4z6L3tNUhB7XVgE3' , 1 , '62280ad0-e157-4625-a93f-65f9afb1b305');
            }

            // this.router.navigate(['/ciudad', id]);
            // this.mensaje();
          })
        })
      }).catch(err => {
        alert('hubo un error en email+');
      })

    } else {

      const toast = await this.toastCtrl.create({
        message: 'Falta algun campo por llenar',
        duration: 3000
      });
      toast.present();
    }
  }


  signInWithFacebook() {
    this.fb.login(['public_profile', 'email']).then((res: FacebookLoginResponse) => {
      const facebookCredential = firebase.default.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
      firebase.default.auth().signInWithCredential(facebookCredential)
        .then(user => {
          //console.log('datos de la sesion del user',user);
          this.us = user.user;
          console.log('Usuario: ', JSON.stringify(this.us));
          localStorage.setItem("uid", this.us.uid);

          //console.log('userID',this.us.uid);
          this.url = "?height=500";
          this.usPhotoUrl = this.us.photoURL + this.url;
          this.authServ.cargarUsuario(
            this.us.displayName,
            this.us.email,
            this.us.photoURL,
            this.us.uid
          );
          //sacar el codigo del usuario
          this.authServ.getCodigo(this.us.uid).subscribe(co => {
            this.codigos = co;
            console.log('datos tabla user', this.codigos.length);
            if (this.codigos.length == 0) {
              console.log('agregar tel');
              this.db.collection('users').doc(this.authServ.usuario.uid).set({
                idUser: this.authServ.usuario.uid,
                displayName: this.us.displayName,
                email: this.us.email,
                photoURL: this.us.photoURL,
              })
                .then(() => this.mensaje())
                .catch(function (error) {
                  console.error("Error adding document: ", error);
                });

            }
            if (this.codigos.length == 1) {
              this.router.navigate(['/ciudad', this.us.uid]);
            }
          });
          // this.navCtrl.setRoot(HomePage);

        }).catch(e => alert('Error de autenticación' + JSON.stringify(e)));
    })
  }

  async mensaje() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'my-custom-class',
      message: 'Iniciando Sesión',
      duration: 2000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

  atras() {
    this.router.navigate(['/login']);
  }
}
