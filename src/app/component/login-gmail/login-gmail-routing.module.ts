import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginGmailPage } from './login-gmail.page';

const routes: Routes = [
  {
    path: '',
    component: LoginGmailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginGmailPageRoutingModule {}
