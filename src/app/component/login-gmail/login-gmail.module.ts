import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginGmailPageRoutingModule } from './login-gmail-routing.module';

import { LoginGmailPage } from './login-gmail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoginGmailPageRoutingModule
  ],
  declarations: [LoginGmailPage]
})
export class LoginGmailPageModule {}
