import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SucursalService } from 'src/app/servicio/sucursal.service';
import * as moment from 'moment';

@Component({
  selector: 'app-detalles',
  templateUrl: './detalles.page.html',
  styleUrls: ['./detalles.page.scss'],
})
export class DetallesPage implements OnInit {

  idUser: string;
  idTienda: string;
  detalleTienda: any = [];
  detalleSucursal: any = [];
  detalleTurno: any = [];
  numTurno: number;
  allUser: any = [];
  total: number;
  idFila: number;
  momentos: any;

  constructor(
    private route: ActivatedRoute,
    private sucProv: SucursalService,
    private router: Router
  ) { }

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get("id");
    console.log("Este es el idUser", id);
    this.idUser = id;

    let idTienda = this.route.snapshot.paramMap.get("idTienda");
    console.log("este es el idTienda", idTienda);
    this.idTienda = idTienda;

    let idFila = this.route.snapshot.paramMap.get("idFila");
    console.log("Este es el idFila", idFila);
    this.idFila = parseInt(idFila);

    var today = moment().format('YYYY-MM-DD');
    console.log("variable today", today);

    this.momentos = today;
    console.log("variable momentos", this.momentos);
    
    
    this.detalleTienda = [];
    this.detalleSucursal = [];
    this.detalleTurno = [];
    this.allUser = [];

    this.goToTienda();
    this.goToFila();
    this.Alluser();
  }


  goToTienda(){
    return new Promise(resolve => {
      let body = {
        idTienda: this.idTienda,
        accion: 'goToDetalleTienda'
      }

      this.sucProv.postData(body, 'lugares.php').subscribe(data => {
        for(let detalleTienda of data.result){
          this.detalleTienda.push(detalleTienda)
          console.log("Datos de la tienda seleccionada", this.detalleTienda);
          this.goToSucursal(detalleTienda.idSucursal);
        }
        resolve(true);
      });
    });
  }


  goToSucursal(idSucursal){
    return new Promise(resolve => {
      let body = {
        idSucursal: idSucursal,
        accion: 'getDetalleSucursal'
      }

      this.sucProv.postData(body, 'lugares.php').subscribe(data => {
        for(let detalleSucursal of data.result){
          this.detalleSucursal.push(detalleSucursal)
          console.log("Datos de las sucursales", this.detalleSucursal);
        }
        resolve(true);
      });
    });
  }


  goToFila(){
    return new Promise(resolve => {
      let body = {
        idUser: this.idUser,
        idTienda: this.idTienda,
        idFila: this.idFila,
        accion: 'getDetalleFila'
      }
      
      this.sucProv.postData(body, 'lugares.php').subscribe(data => {
        for(let turno of data.result){
          this.detalleTurno.push(turno)
          console.log("Datos del turno", this.detalleTurno);
          if (turno.estatus == 'pendiente') {
            this.numTurno = turno.numTurno;
            console.log("turnos en estado pendiente", this.numTurno);
          }
        }
        resolve(true);
      });
    });
  }


  Alluser(){
    return new Promise(resolve => {
      let body = {
        idTienda: this.idTienda,
        fecha: this.momentos,
        estatus: 'pendiente',
        accion: 'getAllUser'
      };

      this.sucProv.postData(body, 'turno.php').subscribe(data => {
        for(let all of data.result){
          this.allUser.push(all)
          console.log("Datos de los usuarios en tienda", this.allUser);
          this.total = parseInt(this.allUser.length);
          console.log("total de usuarios en espera", this.total);
        }
        resolve(true);
      });
    });
  }

  refreshPage() {
    this.ngOnInit();
  }

  goToBack(){
    this.router.navigate(['/lugares', this.idUser]);
  }

}
