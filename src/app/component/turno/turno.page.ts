import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SucursalService } from 'src/app/servicio/sucursal.service';
import * as moment from 'moment';

@Component({
  selector: 'app-turno',
  templateUrl: './turno.page.html',
  styleUrls: ['./turno.page.scss'],
})
export class TurnoPage implements OnInit {

  idUser: string;
  idTienda: number;
  tiendaSec: any = [];
  sucursalSec: any = [];
  idSucursal: number;
  turnoSel: any = [];
  allUser: any = [];
  total: number;
  numTurno: number;
  sumTurn: number;
  momentos: any;
  myUsers: any = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private sucProv: SucursalService
  ) { }

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get("id");
    console.log("este es el idUser:", id);
    this.idUser = id;

    let idTienda = this.route.snapshot.paramMap.get("idTienda");
    console.log("este es el idTienda:", idTienda);
    this.idTienda = parseInt(idTienda);

    let sumTurno = this.route.snapshot.paramMap.get("sumTurno");
    console.log("Este es el numero de turno", sumTurno);
    this.sumTurn = parseInt(sumTurno);

    var today = moment().format('YYYY-MM-DD');
    console.log("variable today", today);

    this.momentos = today;
    console.log("variable momentos", this.momentos);

    this.tiendaSec = [];
    this.sucursalSec = [];
    this.turnoSel = [];
    this.allUser = [];
    this.myUsers = [];

    this.goToInfoTienda();
    this.goToTurno();
    this.Alluser();
    // this.myUser();
    this.ConsComTienda();
  }

  goToInfoTienda() {
    return new Promise(resolve => {
      let body = {
        idTienda: this.idTienda,
        accion: 'getInfoTienda'
      };

      this.sucProv.postData(body, 'establecimiento.php').subscribe(data => {
        for (let tienda of data.result) {
          this.tiendaSec.push(tienda)
          this.idSucursal = parseInt(tienda.idSucursal);
          this.goToInformation();
          console.log("Datos de la tienda seleccionada", this.tiendaSec);
        }
        resolve(true);
      });
    });
  }

  goToInformation() {
    return new Promise(resolve => {
      let body = {
        idSucursal: this.idSucursal,
        accion: 'getInfoSuc'
      };

      this.sucProv.postData(body, 'establecimiento.php').subscribe(data => {
        for (let sucursal of data.result) {
          this.sucursalSec.push(sucursal)
          console.log("Datos de la sucursal seleccionada", this.sucursalSec);
        }
        resolve(true);
      });
    });
  }

  goToTurno() {
    return new Promise(resolve => {
      let body = {
        idTienda: this.idTienda,
        idUser: this.idUser,
        sumTurno: this.sumTurn,
        accion: 'getInfoTurno'
      };

      this.sucProv.postData(body, 'turno.php').subscribe(data => {
        for (let turno of data.result) {
          this.turnoSel.push(turno)
          console.log("Datos del turno", this.turnoSel);
          console.log("Numero de personas en la tienda", this.turnoSel.length);
          this.numTurno = turno.numTurno;
          if (turno.estatus == 'activado') {
            this.router.navigate(['/tienda', this.idUser]);
          }
        }
        resolve(true);
      });
    });
  }

  Alluser() {
    return new Promise(resolve => {
      let body = {
        idTienda: this.idTienda,
        fecha: this.momentos,
        estatus: 'pendiente',
        accion: 'getAllUser'
      };

      this.sucProv.postData(body, 'turno.php').subscribe(data => {
        for (let all of data.result) {
          this.allUser.push(all)
          console.log("Datos de los usuarios en tienda", this.allUser);
          this.total = parseInt(this.allUser.length);
          console.log("total de usuarios en espera", this.total);
        }
        resolve(true);
      });
    });
  }

  goToEstablecimiento() {
    this.router.navigate(['/tienda', this.idUser]);
  }

  refreshPage() {
    this.ngOnInit();
  }


  ConsComTienda(){
    return new Promise(resolve => {
      let body = {
        idUser: this.idUser,
        idTienda: this.idTienda,
        accion: 'getComTienda'
      };

      this.sucProv.postData(body, 'add_formulario.php').subscribe(data => {

        if (data.result == 0) {
          console.log("si cuenta y es 0");      
          
          this.myUser();
        }

        resolve(true);
      });

    });
  }

  myUser() {
    return new Promise(resolve => {
      let body = {
        idUser: this.idUser,
        accion: 'getDataMyuser'
      };

      this.sucProv.postData(body, 'add_user.php').subscribe(data => {
        for (let myUser of data.result) {
          this.myUsers.push(myUser);
          console.log("Estos son los datos del usuario", this.myUsers);
          console.log("Datos del formulario", myUser.formulario);

          if (myUser.formulario == 0) {
            let body = {
              formulario: 1,
              idUser: this.idUser,
              accion: 'getUpdateFormUser'
            };

            this.sucProv.postData(body, 'add_user.php').subscribe(data => {
              if(data.success){
                console.log("Se ha modificado el estatus formulario");
              }
            });

          }
        }
        resolve(true);
      });
    });


  }


}
