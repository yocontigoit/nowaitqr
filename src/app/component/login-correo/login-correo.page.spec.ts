import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LoginCorreoPage } from './login-correo.page';

describe('LoginCorreoPage', () => {
  let component: LoginCorreoPage;
  let fixture: ComponentFixture<LoginCorreoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginCorreoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LoginCorreoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
