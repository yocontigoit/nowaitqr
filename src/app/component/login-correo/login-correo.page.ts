import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, Platform, ToastController } from '@ionic/angular';
import { SucursalService } from 'src/app/servicio/sucursal.service';
import { UsuarioService } from 'src/app/servicio/usuario.service';

import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';

@Component({
  selector: 'app-login-correo',
  templateUrl: './login-correo.page.html',
  styleUrls: ['./login-correo.page.scss'],
})
export class LoginCorreoPage implements OnInit {

  email: string;
  password: string;
  ingresos: any;

  usuarios: any = [];

  qrScan: any;
  qrText: string;

  constructor(
    private authServ: UsuarioService,
    private router: Router,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    private sucProv: SucursalService,
    private qrScanner: QRScanner,
    public platform: Platform
  ) { 
    this.platform.backButton.subscribeWithPriority(0, () => {
      document.getElementsByTagName('body')[0].style.opacity = '1';
      this.qrScan.unsubscribe();
    });
  }

  ngOnInit() {
    this.usuarios = [];
  }

  async OnSubmitLogin() {

    if (this.email != "" && this.password != "") {

      console.log('Estas en la funcion');
      this.authServ.login(this.email, this.password).then(res => {

        this.authServ.ingresoEmail(this.email).subscribe(ingreso => {
          this.ingresos = ingreso;
          console.log("estos son los datos del usuario logueado", this.ingresos);

          this.ingresos.forEach(element => {
            const id = element.idUser
            const idSucursal = element.sucursal

            this.router.navigate(['/tienda', id]);
            this.mensaje();
            localStorage.setItem("uid", id);
            localStorage.setItem("isLogin", 'true');

          })
        })

      }).catch(err => {
        alert('hubo un error en correo');
        this.mensaje_error();
      })

    } else {
      const toast = await this.toastCtrl.create({
        message: 'Los datos estan incompletos',
        duration: 2000
      });
      await toast.present();
    }

  }

  async mensaje() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'my-custom-class',
      message: 'Iniciando Sesión',
      duration: 2000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }


  async mensaje_error() {
    const toast = await this.toastCtrl.create({
      message: 'Correo o Contraseña Invalidos, vuelva a intentarlo',
      duration: 4000
    });
    await toast.present();
  }


  atras() {
    this.router.navigate(['/login']);
  }


  view() {
    // Optionally request the permission early
    this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
        if (status.authorized) {
          // camera permission was granted

          // start scanning
          let scanSub = this.qrScanner.scan().subscribe((text: string) => {
            console.log('Scanned something', text);
            alert("precion")

            this.qrScanner.hide(); // hide camera preview
            scanSub.unsubscribe(); // stop scanning
          });

          this.qrScanner.show();
          document.getElementsByTagName('body')[0].style.opacity = '0';
          this.qrScan = this.qrScanner.scan().subscribe((textFound) => {
            document.getElementsByTagName("body")[0].style.opacity = "1";
            this.qrScanner.hide();
            this.qrScan.unsubscribe();
            this.qrText = textFound;
            alert("precion configuracion")
          }), (err) => {
            alert(JSON.stringify(err));
          };

        } else if (status.denied) {
          // camera permission was permanently denied
          // you must use QRScanner.openSettings() method to guide the user to the settings page
          // then they can grant the permission from there
        } else {
          // permission was denied, but not permanently. You can ask for permission again at a later time.
        }
      })
      .catch((e: any) => console.log('Error is', e));
  }

}
