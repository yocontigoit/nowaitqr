import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LugaresPPage } from './lugares-p.page';

const routes: Routes = [
  {
    path: '',
    component: LugaresPPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LugaresPPageRoutingModule {}
