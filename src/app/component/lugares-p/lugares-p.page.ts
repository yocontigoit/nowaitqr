import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SucursalService } from 'src/app/servicio/sucursal.service';
import * as moment from 'moment';

@Component({
  selector: 'app-lugares-p',
  templateUrl: './lugares-p.page.html',
  styleUrls: ['./lugares-p.page.scss'],
})
export class LugaresPPage implements OnInit {

  idUser: string;
  momentos: any;
  tiendaList: any = [];
  turnoList: any = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private sucProv: SucursalService
  ) { }

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get("id");
    console.log("este es el idUser:", id);
    this.idUser = id;

    var today = moment().format('YYYY-MM-DD');
    console.log("variable today", today);

    this.momentos = today;
    console.log("variable momentos", this.momentos);

    this.tiendaList = [];
    this.turnoList = [];

    this.goToTurno();
    this.goToTienda();
  }

  goToTurno(){
    return new Promise(resolve => {
      let body = {
        idUser: this.idUser,
        fecha: this.momentos,
        accion: 'getInfoTurnoList'
      };

      this.sucProv.postData(body, 'lugares.php').subscribe(data => {
        for(let lugares of data.result){
          this.turnoList.push(lugares)
          console.log("Estos son los turnos", this.turnoList);
          // this.goToTienda(lugares.idTienda)
        }
        resolve(true);
      });
    });
  }

  goToTienda(){
    return new Promise(resolve => {
      let body = {
        // idTienda: idTienda,
        accion: 'getInfoTiendaList'
      };

      this.sucProv.postData(body, 'lugares.php').subscribe(data => {
        for(let fila of data.result){
          this.tiendaList.push(fila)
          console.log("Datos de la tienda seleccionada", this.tiendaList);
        }
        resolve(true);
      });
    });
  }

  goToDetalle(idTienda, idFila){
    this.router.navigate(['/detalles', this.idUser, idTienda, idFila]);
  }

  goToBack() {
    this.router.navigate(['/tienda', this.idUser]);
  }

}
