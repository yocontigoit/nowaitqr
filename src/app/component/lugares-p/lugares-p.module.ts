import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LugaresPPageRoutingModule } from './lugares-p-routing.module';

import { LugaresPPage } from './lugares-p.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LugaresPPageRoutingModule
  ],
  declarations: [LugaresPPage]
})
export class LugaresPPageModule {}
