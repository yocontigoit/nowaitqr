import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LugaresPPage } from './lugares-p.page';

describe('LugaresPPage', () => {
  let component: LugaresPPage;
  let fixture: ComponentFixture<LugaresPPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LugaresPPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LugaresPPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
