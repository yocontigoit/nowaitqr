import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SucursalService } from 'src/app/servicio/sucursal.service';
import { TiendasService } from 'src/app/servicio/tiendas.service';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { AlertController, ToastController } from '@ionic/angular';
import * as moment from 'moment';
import { Http, Headers, RequestOptions } from '@angular/http';

@Component({
  selector: 'app-establecimiento',
  templateUrl: './establecimiento.page.html',
  styleUrls: ['./establecimiento.page.scss'],
})
export class EstablecimientoPage implements OnInit {

  tiendas: any = [];
  idUser: string;
  usuarios: any = [];

  ciudades: any = [];

  idSucursal: number;
  idCiudad: number;
  playerID: string;

  qrScan: any;
  qrText: string;

  myUsers: any = [];
  momentos: any;

  momentoActual: any;
  hora: any;
  minuto: any;
  segundo: any;
  horas: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private tiendaProv: TiendasService,
    private sucProv: SucursalService,
    private qrScanner: QRScanner,
    public alertController: AlertController,
    public toastController: ToastController,
    public http: Http,
  ) { }

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get("id");
    console.log("este es el idUser:", id);
    this.idUser = id;

    this.ciudades = [];

    // this.AllTiendas();
    this.AllUser();
    this.cambioTurno();

    this.tiendas = [];
    this.usuarios = [];
    this.myUsers = [];

    this.myUser();

    var today = moment().format('YYYY-MM-DD');
    console.log("variable today", today);

    this.momentos = today;
    console.log("variable momentos", this.momentos);

    this.momentoActual = new Date()
    this.hora = this.momentoActual.getHours();
    this.minuto = this.momentoActual.getMinutes()
    this.segundo = this.momentoActual.getSeconds()
    this.horas = this.hora + ':' + this.minuto
  }


  AllUser() {
    return new Promise(resolve => {
      let body = {
        idUser: this.idUser,
        accion: 'getdataUser',
      };

      this.sucProv.postData(body, 'establecimiento.php').subscribe(data => {
        for (let usuario of data.result) {
          this.usuarios.push(usuario);
          console.log("Datos de usuarios", this.usuarios);
          console.log("Sucursal que eligio el usuario", usuario.sucursal);

          this.idSucursal = usuario.sucursal;
          this.idCiudad = usuario.ciudad;
          this.playerID = usuario.playerID;

          this.AllTiendas(usuario.sucursal);
        }
        resolve(true);
      });
    });
  }


  AllTiendas(idSucursal) {
    return new Promise(resolve => {
      let body = {
        idSucursal: idSucursal,
        accion: 'getdataTienda',
      };

      this.sucProv.postData(body, 'establecimiento.php').subscribe(data => {
        for (let tienda of data.result) {
          this.tiendas.push(tienda);
          console.log("Total de tiendas", this.tiendas);
        }
        resolve(true);
      });
    });
  }


  goToList(idTienda) {
    this.router.navigate(['/descripcion', this.idUser, this.idSucursal, idTienda, this.playerID]);
  }


  view(idTienda) {
    // Optionally request the permission early
    this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
        if (status.authorized) {
          // camera permission was granted

          // start scanning
          let scanSub = this.qrScanner.scan().subscribe((text: string) => {
            console.log('Scanned something', text);
            // alert("precion")

            this.qrScanner.hide(); // hide camera preview
            scanSub.unsubscribe(); // stop scanning
          });

          this.qrScanner.show();
          document.getElementsByTagName('body')[0].style.opacity = '0';
          this.qrScan = this.qrScanner.scan().subscribe((textFound) => {
            document.getElementsByTagName("body")[0].style.opacity = "1";
            this.qrScanner.hide();
            this.qrScan.unsubscribe();
            this.qrText = textFound;

            this.router.navigate(['/descripcion', this.idUser, this.idSucursal, idTienda, this.playerID]);

            // alert("precion configuracion");
          }), (err) => {
            alert(JSON.stringify(err));
          };

        } else if (status.denied) {
          // camera permission was permanently denied
          // you must use QRScanner.openSettings() method to guide the user to the settings page
          // then they can grant the permission from there
        } else {
          // permission was denied, but not permanently. You can ask for permission again at a later time.
        }
      })
      .catch((e: any) => console.log('Error is', e));
  }


  goToEstablecimiento() {
    this.router.navigate(['/lugares', this.idUser]);
  }

  myUser() {
    return new Promise(resolve => {
      let body = {
        idUser: this.idUser,
        accion: 'getDataMyuser'
      };

      this.sucProv.postData(body, 'add_user.php').subscribe(data => {
        for (let myUser of data.result) {
          this.myUsers.push(myUser);
          console.log("Estos son los datos del usuario", this.myUsers);
          console.log("Datos del formulario", myUser.formulario);

          if (myUser.formulario == 1) {
            this.presentAlertPrompt();
          }
        }
        resolve(true);
      });
    });


  }

  async presentAlertPrompt() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: '¿Cómo fue tu experiencia en la tienda?',
      inputs: [
        {
          name: 'paragraph',
          id: 'paragraph',
          type: 'textarea',
          placeholder: 'Describe como fue tu experiencia ...'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Guardar',
          handler: data => {
            console.log('Confirm Ok');
            console.log("Dato a almacenar", data.paragraph);

            let body = {
              descripcion: data.paragraph,
              type: 'tienda',
              fecha: this.momentos,
              idUser: this.idUser,
              accion: 'add_form_tienda'
            };

            this.sucProv.postData(body, 'add_formulario.php').subscribe(data => {
              if (data.success) {
                console.log("Se ha agregado el Formulario Tienda");

                this.presentFormApp();

              }
            });

          }
        }
      ]
    });

    await alert.present();
  }



  async presentFormApp() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: '¿Cómo fue tu experiencia con la aplicación?',
      inputs: [
        {
          name: 'paragraph',
          id: 'paragraph',
          type: 'textarea',
          placeholder: 'Describe como fue tu experiencia ...'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Guardar',
          handler: data => {
            console.log('Confirm Ok');
            console.log("Dato a almacenar", data.paragraph);

            let body = {
              descripcion: data.paragraph,
              type: 'app',
              fecha: this.momentos,
              idUser: this.idUser,
              accion: 'add_form_tienda'
            };

            this.sucProv.postData(body, 'add_formulario.php').subscribe(data => {
              if (data.success) {
                console.log("Se ha agregado el Formulario App");

                this.presentToast();
              }
            });

          }
        }
      ]
    });

    await alert.present();
  }


  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Se ha enviado tu comentario.',
      duration: 2000
    });
    toast.present();

    let body = {
      formulario: 0,
      idUser: this.idUser,
      accion: 'getUpdateFormUser'
    };

    this.sucProv.postData(body, 'add_user.php').subscribe(data => {
      if (data.success) {
        console.log("Se ha modificado el estatus formulario");
      }
    });
  }


  cambioTurno() {
    
    this.horas = this.hora + ':' + this.minuto
    console.log("Esta es la hora actual", this.hora,':',this.minuto, "Hora completa", this.horas);
    
    return new Promise(resolve => {
      let body = {
        idUser: this.idUser,
        accion: 'getCambioTurno'
      };

      this.sucProv.postData(body, 'add_formulario.php').subscribe(data => {
        for (let turnos of data.result) {
          console.log("Turnos", turnos);

          if (turnos.estatus == 'activado' && this.momentos == turnos.fecha) {
            if (turnos.salida < this.horas) {
              this.continue(turnos.idFila);
              console.log("turnos seleccionados", turnos.idFila);
            }
          }

        }
        resolve(true);
      });
    });
  }


  continue(idFila) {
    let data = JSON.stringify({
      idFila: idFila,
      estatus: 'concluido',
      accion: 'updateCambioTurno'
    });

    let url = 'https://proyectosinternos.com/NoWaitQR/server_api_nowaitqr/add_formulario.php';
    let type = "application/json; charset=UTF-8";
    let headers = new Headers({ "Content-Type": type });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, data, options).subscribe(res => {
      console.log('Se actualizo el estatus', res)
    });
  }

  goToExit(){
    localStorage.setItem("uid", '');
    localStorage.setItem("isLogin", 'false');
    this.router.navigate(['/login']);
  }


}
