import { Component, OnInit } from '@angular/core';
import { SucursalService, Usuario } from 'src/app/servicio/sucursal.service';
import { map } from "rxjs/operators";
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { PushnotificationService } from 'src/app/servicio/pushnotification.service';
import { NotificationService } from 'src/app/servicio/notification.service';
import { AngularFirestore } from '@angular/fire/firestore';
// import { async } from '@angular/core/testing';


@Component({
  selector: 'app-ciudad',
  templateUrl: './ciudad.page.html',
  styleUrls: ['./ciudad.page.scss'],
})
export class CiudadPage implements OnInit {

  listado: any;
  ocultar1: boolean = false;
  ocultar2: boolean = false;
  ocultar3: boolean = false;
  valor: '';

  usuario: Usuario;
  ciudades: any = [];
  sucursales: any = [];
  municipio: any = [];

  idUser: string;
  sucursalSel: number;
  idCiudad: number;
  idSucursal: number;

  constructor(
    private sucProv: SucursalService,
    private router: Router,
    private route: ActivatedRoute,
    public loadingCtrl: LoadingController,
    private pushnot: NotificationService,
    public db: AngularFirestore
  ) { }

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get("id");
    console.log("este es el idUser:", id);
    this.idUser = id;
    
    this.AllMunic();

    this.ciudades = [];
    this.sucursales = [];
    this.municipio = [];
  }

  AllMunic(){
    return new Promise(resolve => {
  		let body = {
        accion: 'getDataMunicipio',
  		};

  		this.sucProv.postData(body, 'add_user.php').subscribe(data => {
  			for(let munic of data.result){
          this.municipio.push(munic);
          console.log("Total de municipio",this.municipio);
  			}
  			resolve(true);
  		});
  	});
  }

  checkEstado(event){
    console.log("Este es el idMunic", event.detail.value);
   
    let body = {
      idMunic: event.detail.value,
      idUser: this.idUser,
      accion: 'update_munic'
    };

    this.sucProv.postData(body, 'add_user.php').subscribe(async data => {
      if (data.success) {
        console.log("Se ha agregado el municipio");
      }
    });

    this.AllCiudad(event.detail.value);
  }


  AllCiudad(event){
    this.ocultar3 = !this.ocultar3;
  	return new Promise(resolve => {
  		let body = {
        idMunic: event,
  			accion : 'getdata',
  		};

  		this.sucProv.postData(body, 'add_user.php').subscribe(data => {
  			for(let ciudad of data.result){
          this.ciudades.push(ciudad);
          console.log("Total de ciudades",this.ciudades);
  			}
  			resolve(true);
  		});
  	});
  }

  checkValue(event){ 
    console.log(event.detail.value)

    let body = {
      idCiudad: event.detail.value,
      idUser: this.idUser,
      accion: 'update_ciudad'
    };

    this.sucProv.postData(body, 'add_user.php').subscribe(async data => {
      if (data.success) {
        console.log("se ha agregado la ciudad");  
      }
    })

    this.sucursal(event.detail.value);
    this.idCiudad = event.detail.value;
  }


  sucursal(ciudad){
    this.ocultar1 = !this.ocultar1;
    console.log("funcion de sucursal", ciudad);

    return new Promise(resolve => {
  		let body = {
        idCiudad: ciudad,
  			accion : 'getdata_sucursal',
  		};

  		this.sucProv.postData(body, 'add_user.php').subscribe(data => {
  			for(let sucursal of data.result){
          this.sucursales.push(sucursal);
          console.log("Total de sucursales",this.sucursales);
  			}
  			resolve(true);
  		});
    });

  }

  Sucursales(event){
    this.ocultar2 = !this.ocultar2;
    console.log(event.detail.value)

    let body = {
      idSucursal: event.detail.value,
      idUser: this.idUser,
      accion: 'update_sucursal'
    }

    this.sucProv.postData(body, 'add_user.php').subscribe(async data => {
      if (data.success) {
        console.log("se ha guardado la sucursal");
      }
    });

    this.idSucursal = event.detail.value;
    // this.goToEstablecimiento();

    this.db.collection("users").doc(this.idUser).update({
      estatusPush: 2
    }).then(() => {
      console.log("Se actualizo");
    });

  }

  async goToEstablecimiento(){
    const loading = await this.loadingCtrl.create({
      cssClass: 'my-custom-class',
      message: 'Iniciando Sesión',
      duration: 2000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');

    this.goToFinally();
  }

  goToFinally(){
    this.router.navigate(['/tienda', this.idUser]);
    // this.pushnot.init_notification(this.idUser);
    // this.goToFinal();

    // setTimeout(this.goToFinal,2000);
  }

  goToFinal(){
    this.router.navigate(['/tienda', this.idUser]);
  }


}
