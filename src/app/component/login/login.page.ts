import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/servicio/usuario.service';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { LoadingController, Platform } from '@ionic/angular';
import * as firebase from 'firebase/app';
import "firebase/database";
//import { auth } from 'firebase';
import 'firebase/auth';
import 'firebase/firestore';
import { AngularFirestore } from '@angular/fire/firestore';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { NotificationService } from 'src/app/servicio/notification.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  email: string;
  password: string;
  us: any;
  url: any;
  usPhotoUrl: any;
  codigos: any;
  ingresos: any;
  ingresoG: any;
  botonesA: any = {};
  uidUserSesion: any;
  nombresUsers: any = {};

  constructor(
    private authServ: UsuarioService,
    private router: Router,
    private fb: Facebook,
    private platform: Platform,
    public loadingCtrl: LoadingController,
    private googlePlus: GooglePlus,
    public db: AngularFirestore,
    private usuarioProv: UsuarioService,
    private pushnot: NotificationService
  ) {

    this.uidUserSesion = localStorage.getItem('uid');
    console.log('id del usuario ya registrado', this.uidUserSesion);

    this.initializeApp();

    
  }

  ngOnInit() {
  }

  signWithCorreo() {
    this.router.navigate(['/correo']);
  }

  register() {
    this.router.navigate(['/register']);
  }

  signInWithFacebook() {
    this.fb.login(['public_profile', 'email']).then((res: FacebookLoginResponse) => {
      const facebookCredential = firebase.default.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
      firebase.default.auth().signInWithCredential(facebookCredential)
        .then(user => {
          this.us = user.user;
          console.log('Usuario: ', JSON.stringify(this.us));
          localStorage.setItem("uid", this.us.uid);
          localStorage.setItem("isLogin", 'true');

          //console.log('userID',this.us.uid);
          this.url = "?height=500";
          this.usPhotoUrl = this.us.photoURL + this.url;
          this.usuarioProv.cargarUsuario(
            this.us.displayName,
            this.us.email,
            this.us.photoURL,
            this.us.uid
          );
          //sacar el codigo del usuario
          this.usuarioProv.getCodigo(this.us.uid).subscribe(co => {
            this.codigos = co;
            console.log('datos tabla user', this.codigos.length);

            if (this.codigos.length == 0) {
              this.db.collection('users').doc(this.usuarioProv.usuario.uid).set({
                idUser: this.usuarioProv.usuario.uid,
                displayName: this.us.displayName,
                email: this.us.email,
                photoURL: this.us.photoURL,
                estatus: 0,
                estatusPush: 0
              })
                .then((docRef) => {
                  console.log("Document written with ID: ", docRef);
                  this.mensaje()
                })
                .catch(function (error) {
                  alert('Error de autenticación' + JSON.stringify(error))
                  console.error("Error adding document: ", error);

                });
            }

            if (this.codigos.length == 1) {

              this.codigos.forEach(element => {
                const id = element.idUser
                const estatus = element.estatus
                const estatusPush = element.estatusPush

                if (estatus == 0) {

                  this.router.navigate(['/ciudad', this.usuarioProv.usuario.uid]);
                  this.usuarioProv.add_UserGmail(this.us.displayName, this.us.email, this.usuarioProv.usuario.uid);
                }

                if (estatusPush == 2) {
                  this.pushnot.init_notification(this.usuarioProv.usuario.uid);
                }

                if (estatus == 1) {
                  this.router.navigate(['/tienda', this.us.uid]);
                }
              })
            }

          });

        }).catch(e => alert('Error de autenticación' + JSON.stringify(e)));
    });
  }

  singWithGoogle() {
    this.googlePlus.login({
      webClientId:
        "307577600417-9j6ulq5se8fp2cv9e9n905dr2psps5qq.apps.googleusercontent.com",
      offline: true
    })
      .then(res => {
        firebase.default.auth().signInWithCredential(
          firebase.default.auth.GoogleAuthProvider.credential(res.idToken)
        )
          .then(user => {
            this.us = user.user;
            console.log(JSON.stringify(user));
            console.log(res.idToken);

            localStorage.setItem("uid", this.us.uid);
            localStorage.setItem("isLogin", 'true');

            this.usuarioProv.cargarUsuarioGmail(
              this.us.displayName,
              this.us.email,
              this.us.photoURL,
              this.us.uid
            );
            this.usuarioProv.ingresoGmail(this.us.uid).subscribe(co => {
              this.ingresoG = co;
              console.log('datos tabla user', this.ingresoG.length);

              if (this.ingresoG.length == 0) {
                this.db.collection('users')
                  .doc(this.usuarioProv.usuario.uid)
                  .set({
                    uid: this.usuarioProv.usuario.uid,
                    displayName: this.us.displayName,
                    email: this.us.email,
                    photoURL: this.us.photoURL,
                    estatus: 0
                  }).then(() => this.mensaje())
                  .catch(function (error) {
                    console.error("Error adding document: ", error);
                  });

                this.pushnot.init_notification(this.us.uid);
                this.usuarioProv.add_UserGmail(this.us.displayName, this.us.email, this.us.uid)
                this.router.navigate(['/ciudad', this.us.uid]);
              }

              if (this.ingresoG.length == 1) {
                this.router.navigate(['/tienda', this.us.uid]);
              }
            });



          })
          .catch(error =>
            console.log("Error en el then prueba 1: " + JSON.stringify(error))
          );
      })
      .catch(err => console.error("No entra al then prueba 1", err));
  }

  async mensaje() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'my-custom-class',
      message: 'Iniciando Sesión',
      duration: 2000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

  initializeApp() { 
    this.botones();
    this.usuarioRe();
    this.hola();
  }


  public botones() {
    this.db.collection('sistema').doc('botones_activos').valueChanges().subscribe(data => {
      this.botonesA = data;
      console.log("Datos de los botones", this.botonesA.estatus);
    });
  }


  usuarioRe() {
    if(this.uidUserSesion == null || this.uidUserSesion == undefined){
      this.router.navigate(['/login']);
    }
    if(this.uidUserSesion != null){
      this.db.collection('users').doc(this.uidUserSesion).valueChanges().subscribe(data => {
        this.nombresUsers = data;
        console.log("Datos del usuario con localstorage", this.nombresUsers);
        
          console.log("Datos de los botones", this.nombresUsers);
          this.router.navigate(['/tienda', this.uidUserSesion]);
          this.mensaje();
          localStorage.setItem("uid", this.uidUserSesion);
          localStorage.setItem("isLogin", 'true');
  
      });
    }
  }
  
  hola(){
    console.log("Por que");
  }


}
