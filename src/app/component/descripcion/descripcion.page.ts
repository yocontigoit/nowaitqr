import { Component, OnInit } from '@angular/core';
import { async } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { SucursalService } from 'src/app/servicio/sucursal.service';
import * as moment from 'moment';
import { NotiTurnoService } from 'src/app/servicio/noti-turno.service';
import { Http, Headers, RequestOptions } from '@angular/http';


@Component({
  selector: 'app-descripcion',
  templateUrl: './descripcion.page.html',
  styleUrls: ['./descripcion.page.scss'],
})
export class DescripcionPage implements OnInit {

  idUser: string;
  idSucursal: number;
  idTienda: number;
  sucursalSec: any = [];
  tiendaSec: any = [];
  turno: any = [];
  numTurno: number;
  momentos: any;
  momentoActual: any;
  hora: any;
  minuto: any;
  segundo: any;
  horas: any;
  userApp: any = [];
  playerUser: any;
  playerID: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private sucProv: SucursalService,
    public loadingCtrl: LoadingController,
    public http: Http,
    private notiSrv: NotiTurnoService) { }

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get("id");
    console.log("este es el idUser:", id);
    this.idUser = id;

    let idSucursal = this.route.snapshot.paramMap.get("idSucursal");
    console.log("este es el idSucursal:", idSucursal);
    this.idSucursal = parseInt(idSucursal);

    let idTienda = this.route.snapshot.paramMap.get("idTienda");
    console.log("este es el idTienda:", idTienda);
    this.idTienda = parseInt(idTienda);

    let playerID = this.route.snapshot.paramMap.get('playerID');
    console.log("este es el playerID", playerID);
    this.playerID = playerID;

    this.sucursalSec = [];
    this.tiendaSec = [];
    this.turno = [];
    this.userApp = [];

    var today = moment().format('YYYY-MM-DD');
    console.log("variable today", today);

    this.momentos = today;
    console.log("variable momentos", this.momentos);

    this.momentoActual = new Date()
    this.hora = this.momentoActual.getHours();
    this.minuto = this.momentoActual.getMinutes()
    this.segundo = this.momentoActual.getSeconds()
    this.horas = this.hora + ':' + this.minuto
    console.log("Esta es la hora actual", this.hora,':',this.minuto, "Hora completa", this.horas);
    

    this.goToInformation();
    this.goToInfoTurno();
    this.goToInfoTienda();
    this.goToInfoUserApp();
  }


  goToInformation(){
    return new Promise(resolve => {
      let body = {
        idSucursal: this.idSucursal,
        accion: 'getInfoSuc'
      };

      this.sucProv.postData(body, 'establecimiento.php').subscribe(data => {
        for(let sucursal of data.result){
          this.sucursalSec.push(sucursal)
          console.log("Datos de la sucursal seleccionada", this.sucursalSec);
        }
        resolve(true);
      });
    });
  }

  goToInfoTienda(){
    return new Promise(resolve => {
      let body = {
        idTienda: this.idTienda,
        accion: 'getInfoTienda'
      };

      this.sucProv.postData(body, 'establecimiento.php').subscribe(data => {
        for(let tienda of data.result){
          this.tiendaSec.push(tienda)
          console.log("Datos de la tienda seleccionada", this.tiendaSec);
        }
        resolve(true);
      });
    });
  }

  goToInfoTurno(){
    return new Promise(resolve => {
      let body = {
        idTienda: this.idTienda,
        estatus: 'pendiente',
        fecha: this.momentos,
        accion: 'getInfoTurno'
      };

      this.sucProv.postData(body, 'establecimiento.php').subscribe(data => {
        console.log("data result", data.result.length);
        if (data.result.length >=1) {
          for(let turnos of data.result){
            this.turno.push(turnos)
            console.log("Turnos", this.turno);
            console.log("numero de turnos", this.turno.length);  
            this.numTurno = this.turno.length;
          }
          resolve(true);
        }
        if (data.result.length == 0) {
          this.numTurno = data.result.length;
        }
      });
    });
  }

  goToInfoUserApp(){
    return new Promise(resolve => {
      let body = {
        idTienda: this.idTienda,
        accion: 'getInfoUserApp'
      };

      this.sucProv.postData(body, 'userapp.php').subscribe(data => {
        for(let Userapp of data.result){
          this.userApp.push(Userapp);
          console.log("Datos del UserApp", this.userApp);
          this.playerUser = Userapp.playerID;
        }
        resolve(true);
      })
    });
  }
  

  goToFila(numTurn) {

    console.log("Si da clicky numero de turno", numTurn);

    var sumTurno = numTurn + 1;

    var today = moment().format('YYYY-MM-DD');
    console.log("variable today", today);

    this.momentos = today;
    console.log("variable momentos", this.momentos);
    
    var d = new Date();
    var n = d.getHours();

    let data = JSON.stringify({
      estatus: 'pendiente',
      fecha: this.momentos,
      hora: this.horas,
      hora_complete: n,
      numTurno: sumTurno,
      idTienda: this.idTienda,
      idUser: this.idUser,
      playerID: this.playerUser,
      accion: 'add_turnos'
    });

    let url = 'https://proyectosinternos.com/NoWaitQR/server_api_nowaitqr/add_turno.php';
    let type = "application/json; charset=UTF-8";
    let headers = new Headers({ "Content-Type": type });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, data, options).subscribe(res => {
      console.log('Se ha agregado', res);
      this.mensaje();
      this.notification(this.playerUser);
      this.router.navigate(['/turno', this.idUser, this.idTienda, sumTurno]); 
    });
  }



  // goToFila2(numTurn){
  //   console.log("Si da clicky numero de turno", numTurn);

  //   var sumTurno = numTurn + 1;

  //   var today = moment().format('YYYY-MM-DD');
  //   console.log("variable today", today);

  //   var d = new Date();
  //   var n = d.getHours();

  //   let body = {
  //     estatus: 'pendiente',
  //     // fecha: this.momentos,
  //     // hora: this.horas,
  //     // hora_complete: n,
  //     // numTurno: sumTurno,
  //     // idTienda: this.idTienda,
  //     // idUser: this.idUser,
  //     // playerID: this.playerUser,
  //     accion: 'add_turnos'
  //   };
  //   console.log("pasa el body", "fecha", this.momentos, "hora" ,this.horas , "hora_complete" , n , 
  //   "numTurno" , sumTurno, "idTienda" , this.idTienda , "idUser" , this.idUser, "playerID" , this.playerUser);

  //   this.sucProv.postData(body, 'turno.php').subscribe(async data => {
  //     if (data.success) {
  //       console.log("Se ha agregado el turno");
  //       // this.notification();
  //       // this.mensaje();
  //       this.router.navigate(['/turno', this.idUser, this.idTienda, sumTurno]); 
  //     }
  //   })
  // }

  notification(playerUser){
    console.log("playerID", playerUser);
    this.notiSrv.insertaNotificacion(playerUser);
  }

  goToBack() {
    this.router.navigate(['/tienda', this.idUser]);
  }


  async mensaje() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'my-custom-class',
      message: 'Obteniendo Turno',
      duration: 2000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

}
