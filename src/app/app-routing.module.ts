import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./component/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'ciudad/:id',
    loadChildren: () => import('./component/ciudad/ciudad.module').then( m => m.CiudadPageModule)
  },
  {
    path: 'correo',
    loadChildren: () => import('./component/login-correo/login-correo.module').then( m => m.LoginCorreoPageModule)
  },
  {
    path: 'gmail',
    loadChildren: () => import('./component/login-gmail/login-gmail.module').then( m => m.LoginGmailPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./component/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'tienda/:id',
    loadChildren: () => import('./component/establecimiento/establecimiento.module').then( m => m.EstablecimientoPageModule)
  },
  {
    path: 'descripcion/:id/:idSucursal/:idTienda/:playerID',
    loadChildren: () => import('./component/descripcion/descripcion.module').then( m => m.DescripcionPageModule)
  },
  {
    path: 'turno/:id/:idTienda/:sumTurno',
    loadChildren: () => import('./component/turno/turno.module').then( m => m.TurnoPageModule)
  },
  {
    path: 'lugares/:id',
    loadChildren: () => import('./component/lugares-p/lugares-p.module').then( m => m.LugaresPPageModule)
  },
  {
    path: 'detalles/:id/:idTienda/:idFila',
    loadChildren: () => import('./component/detalles/detalles.module').then( m => m.DetallesPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
