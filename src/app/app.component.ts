import { Component } from '@angular/core';

import { LoadingController, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { PushnotificationService } from './servicio/pushnotification.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  botonesA: any = {};
  uidUserSesion: any;
  nombresUsers: any = {};

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public PushSrv: PushnotificationService,
    public db: AngularFirestore,
    public loadingCtrl: LoadingController,
    private router: Router
  ) {
    this.initializeApp();
    this.PushSrv.init_notification();

    

    // this.db.collection('users').doc(this.uidUserSesion).valueChanges().subscribe(data => {
    //   this.nombresUsers = data;
    //   if (this.nombresUsers.exists) {
    //     console.log("Datos de los botones", this.nombresUsers);
    //     this.router.navigate(['/tienda', this.uidUserSesion]);
    //     this.mensaje();
    //     localStorage.setItem("uid", this.uidUserSesion);
    //     localStorage.setItem("isLogin", 'true');
    //   }
    // });
    
  }

  initializeApp() {
    this.platform.ready().then(() => {

      this.statusBar.styleDefault();
      this.splashScreen.hide();

      // this.db.collection('sistema').doc('botones_activos').valueChanges().subscribe(data => {
      //   this.botonesA = data;
      //   console.log("Datos de los botones", this.botonesA);
      // });

    });
  }

  usuarioRe() {
    this.db.collection('users').doc(this.uidUserSesion).valueChanges().subscribe(data => {
      this.nombresUsers = data;
      if (this.nombresUsers.exists) {
        console.log("Datos de los botones", this.nombresUsers);
        this.router.navigate(['/tienda', this.uidUserSesion]);
        this.mensaje();
        localStorage.setItem("uid", this.uidUserSesion);
        localStorage.setItem("isLogin", 'true');
      }
    });
  }

  botones() {
    this.db.collection('sistema').doc('botones_activos').valueChanges().subscribe(data => {
      this.botonesA = data;
      console.log("Datos de los botones", this.botonesA);
    });
  }

  async mensaje() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'my-custom-class',
      message: 'Iniciando Sesión',
      duration: 2000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

}
