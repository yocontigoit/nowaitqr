import { Component } from '@angular/core';
import { AlertController, LoadingController, NavController, Platform } from '@ionic/angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import * as firebase from 'firebase/app';
import "firebase/database";
import { UsuarioService } from '../servicio/usuario.service';
// import { AngularFirestore } from 'angularfire2/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  us: any;
  url: any;
  usPhotoUrl: any;
  codigos: any;

  constructor(
    private platform: Platform,
    private fb: Facebook,
    private usuarioProv: UsuarioService,
    public alertController: AlertController,
    // public afs: AngularFirestore,
    public navCtrl: NavController,
    private route: ActivatedRoute,
    private router: Router,
    private Afauth: AngularFireAuth,
    public loadingCtrl: LoadingController,
  ) { }

  // signInWithFacebook() {
  //   console.log("Holi");
  //   localStorage.setItem("isLogin", 'true');

  //   if (this.platform.is('cordova')) {
  //     this.fb.login(['public_profile', 'email']).then((res: FacebookLoginResponse) => {
  //       const facebookCredential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
  //       firebase.auth().signInWithCredential(facebookCredential)
  //         .then(user => {
  //           //console.log('datos de la sesion del user',user);
  //           this.us = user;
  //           console.log('Usuario: ', JSON.stringify(this.us));
  //           localStorage.setItem("uid", this.us.uid);

  //           //console.log('userID',this.us.uid);
  //           this.url = "?height=500";
  //           this.usPhotoUrl = this.us.photoURL + this.url;
  //           console.log("Foto", this.usPhotoUrl);

  //           this.usuarioProv.cargarUsuario(
  //             this.us.displayName,
  //             this.us.email,
  //             this.us.photoURL,
  //             this.us.uid
  //           );

  //           //  sacar el codigo del usuario

  //           this.usuarioProv.getCodigo(this.us.uid).subscribe(co => {
  //             this.codigos = co;
  //             console.log('datos tabla user', this.codigos.length);

  //             if (this.codigos.length == 0) {
  //               console.log('agregar usuario nuevo');
  //               this.afs.collection('users').doc(this.usuarioProv.usuario.uid).set({
  //                 idUser: this.usuarioProv.usuario.uid,
  //                 displayName: this.us.displayName,
  //                 email: this.us.email,
  //                 photoURL: this.us.photoURL
  //               })
  //                 .then(() => this.mensaje(this.us.uid, this.us.email, this.us.photoURL))
  //                 .catch(function (error) {
  //                   console.error("Error adding document: ", error);
  //                 });
  //             }
  //             if (this.codigos.length == 1) {
  //               this.router.navigate(['ciudad', this.us.uid]);
  //             }

  //           });
  //         }).catch(e => alert('Error de autenticación' + JSON.stringify(e)));
  //     })
  //   }
  // }

  // signInWithFacebook() {
  //   localStorage.setItem("isLogin", 'true');
  //   if (this.platform.is('cordova')) {
  //     this.fb.login(['public_profile', 'email']).then((res: FacebookLoginResponse) => {
  //       const facebookCredential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
  //       firebase.auth().signInWithCredential(facebookCredential)
  //         .then(user => {
  //           //console.log('datos de la sesion del user',user);
  //           this.us = user;
  //           console.log('Usuario: ', JSON.stringify(this.us));
  //           localStorage.setItem("uid", this.us.uid);

  //           //console.log('userID',this.us.uid);
  //           this.url = "?height=500";
  //           this.usPhotoUrl = this.us.photoURL + this.url;
  //           this.usuarioProv.cargarUsuario(
  //             this.us.displayName,
  //             this.us.email,
  //             this.us.photoURL,
  //             this.us.uid
  //           );
  //           //sacar el codigo del usuario
  //           this.usuarioProv.getCodigo(this.us.uid).subscribe(co => {
  //             this.codigos = co;
  //             console.log('datos tabla user', this.codigos.length);
  //             if (this.codigos.length == 0) {
  //               console.log('agregar tel');
  //               this.afs.collection('users').doc(this.usuarioProv.usuario.uid).set({
  //                 key: this.usuarioProv.usuario.uid,
  //                 displayName: this.us.displayName,
  //                 email: this.us.email,
  //                 photoURL: this.us.photoURL,
  //                 user: 0,
  //                 status: 0,
  //                 seguidores: 0,
  //                 suscripcion: 0
  //               })
  //                 .then(() => this.mensaje(this.us.uid, this.us.email, this.us.photoURL))
  //                 .catch(function (error) {
  //                   console.error("Error adding document: ", error);
  //                 });

  //             }
  //             if (this.codigos.length == 1) {
  //               this.router.navigate(['ciudad', this.us.uid]);
  //             }
  //           });
  //         }).catch(e => alert('Error de autenticación' + JSON.stringify(e)));
  //     })
  //   }
  // }

  // loginGoogle(){
  //   this.usuarioProv.loginwithGoogle().then(() => {
  //     this.router.navigate(['/ciudad']);
  //   })
  //     .catch(e => alert('Algo salio mal en Google'));
  // }

  // signInWithFacebook() {
  //   this.usuarioProv.loginwithfacebook().then(res => {
  //     this.router.navigate(['/ciudad']);
  //   }).catch(err => {
  //     alert('hubo un error en facebook');
  //   })
  // }

  ciudad(){
    this.router.navigate(['/ciudad']);
  }

  async mensaje(uid, email, photoURL) {
    const loading = await this.loadingCtrl.create({
      cssClass: 'my-custom-class',
      message: 'Iniciando Sesión',
      duration: 4000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }
}
