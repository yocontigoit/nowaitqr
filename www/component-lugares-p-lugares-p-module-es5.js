(function () {
  function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["component-lugares-p-lugares-p-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/component/lugares-p/lugares-p.page.html":
    /*!***********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/component/lugares-p/lugares-p.page.html ***!
      \***********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentLugaresPLugaresPPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n\n  <div id=\"container\">\n    <ion-grid>\n      <ion-row>\n        <ion-col size=\"1\">\n          <ion-icon name=\"arrow-back-circle-outline\" id=\"icono\" (click)=\"goToBack()\"></ion-icon>\n        </ion-col>\n        <ion-col size=\"10\">\n          <img src=\"./assets/icon/new_disigner_small.jpg\" width=\"70%\" style=\"margin-left: 20%;\" alt=\"\">\n          <!-- <div id=\"nombrePro\"><b id=\"frase\">No Wait</b> QR</div> -->\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n\n  <ion-card *ngFor=\"let turno of turnoList\">\n    <div *ngFor=\"let tienda of tiendaList\">\n      <div *ngIf=\"turno.idTienda == tienda.idTienda\">\n        <ion-card-content style=\"margin-top: auto;\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"3\">\n                <ion-thumbnail item-start>\n                  <img src=\"../../assets/icon/icono.PNG\">\n                </ion-thumbnail>\n              </ion-col>\n             <ion-col size=\"7\">\n                <h1>{{tienda.nombre_tienda}}</h1>\n              </ion-col>\n               <ion-col size=\"2\">\n                <ion-buttons end>\n                  <button ion-button icon-only (click)=\"goToDetalle(tienda.idTienda, turno.idFila)\" id=\"icono\">\n                    <ion-icon name=\"scan-circle-outline\"></ion-icon>\n                  </button>\n                </ion-buttons>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </ion-card-content>\n      </div>\n    </div>\n  </ion-card>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/component/lugares-p/lugares-p-routing.module.ts":
    /*!*****************************************************************!*\
      !*** ./src/app/component/lugares-p/lugares-p-routing.module.ts ***!
      \*****************************************************************/

    /*! exports provided: LugaresPPageRoutingModule */

    /***/
    function srcAppComponentLugaresPLugaresPRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LugaresPPageRoutingModule", function () {
        return LugaresPPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _lugares_p_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./lugares-p.page */
      "./src/app/component/lugares-p/lugares-p.page.ts");

      var routes = [{
        path: '',
        component: _lugares_p_page__WEBPACK_IMPORTED_MODULE_3__["LugaresPPage"]
      }];

      var LugaresPPageRoutingModule = function LugaresPPageRoutingModule() {
        _classCallCheck(this, LugaresPPageRoutingModule);
      };

      LugaresPPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], LugaresPPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/component/lugares-p/lugares-p.module.ts":
    /*!*********************************************************!*\
      !*** ./src/app/component/lugares-p/lugares-p.module.ts ***!
      \*********************************************************/

    /*! exports provided: LugaresPPageModule */

    /***/
    function srcAppComponentLugaresPLugaresPModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LugaresPPageModule", function () {
        return LugaresPPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _lugares_p_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./lugares-p-routing.module */
      "./src/app/component/lugares-p/lugares-p-routing.module.ts");
      /* harmony import */


      var _lugares_p_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./lugares-p.page */
      "./src/app/component/lugares-p/lugares-p.page.ts");

      var LugaresPPageModule = function LugaresPPageModule() {
        _classCallCheck(this, LugaresPPageModule);
      };

      LugaresPPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _lugares_p_routing_module__WEBPACK_IMPORTED_MODULE_5__["LugaresPPageRoutingModule"]],
        declarations: [_lugares_p_page__WEBPACK_IMPORTED_MODULE_6__["LugaresPPage"]]
      })], LugaresPPageModule);
      /***/
    },

    /***/
    "./src/app/component/lugares-p/lugares-p.page.scss":
    /*!*********************************************************!*\
      !*** ./src/app/component/lugares-p/lugares-p.page.scss ***!
      \*********************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentLugaresPLugaresPPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "#icono {\n  background: #192938;\n  color: white;\n  font-size: 30px;\n}\n\n#nombrePro {\n  font-size: xx-large;\n  color: #676767;\n  font-family: Arial, Helvetica, sans-serif;\n  text-align: center;\n}\n\n#frase {\n  font-weight: bold;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n#container {\n  left: 0;\n  right: 0;\n  --ion-background-color:#192938;\n  background-color: #192938;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50L2x1Z2FyZXMtcC9sdWdhcmVzLXAucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQUNKOztBQUVBO0VBQ0ksbUJBQUE7RUFDQSxjQUFBO0VBQ0EseUNBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksaUJBQUE7RUFDQSx5Q0FBQTtBQUNKOztBQUVBO0VBQ0ksT0FBQTtFQUNBLFFBQUE7RUFDQSw4QkFBQTtFQUNBLHlCQUFBO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnQvbHVnYXJlcy1wL2x1Z2FyZXMtcC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjaWNvbm8ge1xyXG4gICAgYmFja2dyb3VuZDogIzE5MjkzODtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxufVxyXG5cclxuI25vbWJyZVBybyB7XHJcbiAgICBmb250LXNpemU6IHh4LWxhcmdlO1xyXG4gICAgY29sb3I6ICM2NzY3Njc7XHJcbiAgICBmb250LWZhbWlseTogQXJpYWwsIEhlbHZldGljYSwgc2Fucy1zZXJpZjtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4gIFxyXG4jZnJhc2Uge1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBmb250LWZhbWlseTogQXJpYWwsIEhlbHZldGljYSwgc2Fucy1zZXJpZjtcclxufVxyXG5cclxuI2NvbnRhaW5lciB7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgcmlnaHQ6IDA7XHJcbiAgICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiMxOTI5Mzg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTkyOTM4O1xyXG4gIH0iXX0= */";
      /***/
    },

    /***/
    "./src/app/component/lugares-p/lugares-p.page.ts":
    /*!*******************************************************!*\
      !*** ./src/app/component/lugares-p/lugares-p.page.ts ***!
      \*******************************************************/

    /*! exports provided: LugaresPPage */

    /***/
    function srcAppComponentLugaresPLugaresPPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LugaresPPage", function () {
        return LugaresPPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var src_app_servicio_sucursal_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/servicio/sucursal.service */
      "./src/app/servicio/sucursal.service.ts");
      /* harmony import */


      var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! moment */
      "./node_modules/moment/moment.js");
      /* harmony import */


      var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);

      var LugaresPPage = /*#__PURE__*/function () {
        function LugaresPPage(router, route, sucProv) {
          _classCallCheck(this, LugaresPPage);

          this.router = router;
          this.route = route;
          this.sucProv = sucProv;
          this.tiendaList = [];
          this.turnoList = [];
        }

        _createClass(LugaresPPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var id = this.route.snapshot.paramMap.get("id");
            console.log("este es el idUser:", id);
            this.idUser = id;
            var today = moment__WEBPACK_IMPORTED_MODULE_4__().format('YYYY-MM-DD');
            console.log("variable today", today);
            this.momentos = today;
            console.log("variable momentos", this.momentos);
            this.tiendaList = [];
            this.turnoList = [];
            this.goToTurno();
            this.goToTienda();
          }
        }, {
          key: "goToTurno",
          value: function goToTurno() {
            var _this = this;

            return new Promise(function (resolve) {
              var body = {
                idUser: _this.idUser,
                fecha: _this.momentos,
                accion: 'getInfoTurnoList'
              };

              _this.sucProv.postData(body, 'lugares.php').subscribe(function (data) {
                var _iterator = _createForOfIteratorHelper(data.result),
                    _step;

                try {
                  for (_iterator.s(); !(_step = _iterator.n()).done;) {
                    var lugares = _step.value;

                    _this.turnoList.push(lugares);

                    console.log("Estos son los turnos", _this.turnoList); // this.goToTienda(lugares.idTienda)
                  }
                } catch (err) {
                  _iterator.e(err);
                } finally {
                  _iterator.f();
                }

                resolve(true);
              });
            });
          }
        }, {
          key: "goToTienda",
          value: function goToTienda() {
            var _this2 = this;

            return new Promise(function (resolve) {
              var body = {
                // idTienda: idTienda,
                accion: 'getInfoTiendaList'
              };

              _this2.sucProv.postData(body, 'lugares.php').subscribe(function (data) {
                var _iterator2 = _createForOfIteratorHelper(data.result),
                    _step2;

                try {
                  for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
                    var fila = _step2.value;

                    _this2.tiendaList.push(fila);

                    console.log("Datos de la tienda seleccionada", _this2.tiendaList);
                  }
                } catch (err) {
                  _iterator2.e(err);
                } finally {
                  _iterator2.f();
                }

                resolve(true);
              });
            });
          }
        }, {
          key: "goToDetalle",
          value: function goToDetalle(idTienda, idFila) {
            this.router.navigate(['/detalles', this.idUser, idTienda, idFila]);
          }
        }, {
          key: "goToBack",
          value: function goToBack() {
            this.router.navigate(['/tienda', this.idUser]);
          }
        }]);

        return LugaresPPage;
      }();

      LugaresPPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: src_app_servicio_sucursal_service__WEBPACK_IMPORTED_MODULE_3__["SucursalService"]
        }];
      };

      LugaresPPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-lugares-p',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./lugares-p.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/component/lugares-p/lugares-p.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./lugares-p.page.scss */
        "./src/app/component/lugares-p/lugares-p.page.scss"))["default"]]
      })], LugaresPPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=component-lugares-p-lugares-p-module-es5.js.map