(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["component-login-correo-login-correo-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/component/login-correo/login-correo.page.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/component/login-correo/login-correo.page.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <ion-header>\n  <ion-toolbar>\n    <ion-buttons start>\n      <button ion-button icon-only (click)=\"atras()\" id=\"icono\">\n        <ion-icon name=\"arrow-back-circle-outline\"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-content class=\"cointer_inicio\">\n  \n  <ion-buttons start style=\"background-color: #192938;\">\n    <button ion-button icon-only (click)=\"atras()\" id=\"iconos\">\n      <ion-icon name=\"arrow-back-circle-outline\"></ion-icon>\n    </button>\n  </ion-buttons>\n  \n  <br> <br> <br> <br>\n\n  <p id=\"inicio\"> Inicio de Sesión con Correo</p>\n\n  <br> <br> \n\n  <ion-item>\n    <ion-label position=\"floating\" style=\"color: #fff;font-size:larger;\">Correo electrónico</ion-label>\n    <ion-input type=\"text\" [(ngModel)]=\"email\" name=\"email\" style=\"color: #fff;\"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label position=\"floating\" style=\"color: #fff;font-size:larger;\">Contraseña</ion-label>\n    <ion-input type=\"password\" [(ngModel)]=\"password\" name=\"password\" style=\"color: #fff;\"></ion-input>\n  </ion-item>\n\n  <br> <br> <br> <br> <br>\n\n    <ion-button expand=\"block\" padding color=\"dark\" (click)=\"OnSubmitLogin()\">Entrar</ion-button>\n\n    <!-- <ion-button expand=\"block\" padding color=\"dark\" (click)=\"view()\">QR</ion-button> -->\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/component/login-correo/login-correo-routing.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/component/login-correo/login-correo-routing.module.ts ***!
  \***********************************************************************/
/*! exports provided: LoginCorreoPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginCorreoPageRoutingModule", function() { return LoginCorreoPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _login_correo_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login-correo.page */ "./src/app/component/login-correo/login-correo.page.ts");




const routes = [
    {
        path: '',
        component: _login_correo_page__WEBPACK_IMPORTED_MODULE_3__["LoginCorreoPage"]
    }
];
let LoginCorreoPageRoutingModule = class LoginCorreoPageRoutingModule {
};
LoginCorreoPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], LoginCorreoPageRoutingModule);



/***/ }),

/***/ "./src/app/component/login-correo/login-correo.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/component/login-correo/login-correo.module.ts ***!
  \***************************************************************/
/*! exports provided: LoginCorreoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginCorreoPageModule", function() { return LoginCorreoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _login_correo_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login-correo-routing.module */ "./src/app/component/login-correo/login-correo-routing.module.ts");
/* harmony import */ var _login_correo_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login-correo.page */ "./src/app/component/login-correo/login-correo.page.ts");







let LoginCorreoPageModule = class LoginCorreoPageModule {
};
LoginCorreoPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _login_correo_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginCorreoPageRoutingModule"]
        ],
        declarations: [_login_correo_page__WEBPACK_IMPORTED_MODULE_6__["LoginCorreoPage"]]
    })
], LoginCorreoPageModule);



/***/ }),

/***/ "./src/app/component/login-correo/login-correo.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/component/login-correo/login-correo.page.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#entrar {\n  background: #b3b3b3;\n  color: white;\n  font-size: x-large;\n}\n\n#icono {\n  background: white;\n  font-size: 30px;\n}\n\n#boton {\n  width: 80%;\n  font-weight: bold;\n  left: 0;\n  right: 0;\n  margin-left: 10%;\n}\n\n#inicio {\n  font-size: x-large;\n  color: #fff;\n  text-align: center;\n  font-weight: bold;\n}\n\n#iconos {\n  background-color: #192938;\n  font-size: 40px;\n  color: #fff;\n}\n\n.cointer_inicio {\n  --ion-background-color:#192938;\n  background-color: #192938;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50L2xvZ2luLWNvcnJlby9sb2dpbi1jb3JyZW8ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFFQTtFQUNJLGlCQUFBO0VBQ0EsZUFBQTtBQUNKOztBQUVBO0VBQ0ksVUFBQTtFQUNBLGlCQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFFQSxnQkFBQTtBQUFKOztBQUdBO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQUFKOztBQUdBO0VBQ0kseUJBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQUFKOztBQUdBO0VBQ0ksOEJBQUE7RUFDQSx5QkFBQTtBQUFKIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50L2xvZ2luLWNvcnJlby9sb2dpbi1jb3JyZW8ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2VudHJhciB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjYjNiM2IzO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiB4LWxhcmdlO1xyXG59XHJcblxyXG4jaWNvbm8ge1xyXG4gICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbn1cclxuXHJcbiNib3RvbiB7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgcmlnaHQ6IDA7XHJcbiAgICAvL21hcmdpbi10b3A6IDMwJTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMCU7ICAgIFxyXG59XHJcblxyXG4jaW5pY2lvIHtcclxuICAgIGZvbnQtc2l6ZTogeC1sYXJnZTtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn1cclxuXHJcbiNpY29ub3Mge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzE5MjkzODtcclxuICAgIGZvbnQtc2l6ZTogNDBweDtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG59XHJcblxyXG4uY29pbnRlcl9pbmljaW97XHJcbiAgICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiMxOTI5Mzg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiMxOTI5Mzg7XHJcbn1cclxuIl19 */");

/***/ }),

/***/ "./src/app/component/login-correo/login-correo.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/component/login-correo/login-correo.page.ts ***!
  \*************************************************************/
/*! exports provided: LoginCorreoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginCorreoPage", function() { return LoginCorreoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_servicio_sucursal_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/servicio/sucursal.service */ "./src/app/servicio/sucursal.service.ts");
/* harmony import */ var src_app_servicio_usuario_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/servicio/usuario.service */ "./src/app/servicio/usuario.service.ts");
/* harmony import */ var _ionic_native_qr_scanner_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/qr-scanner/ngx */ "./node_modules/@ionic-native/qr-scanner/__ivy_ngcc__/ngx/index.js");







let LoginCorreoPage = class LoginCorreoPage {
    constructor(authServ, router, loadingCtrl, toastCtrl, sucProv, qrScanner, platform) {
        this.authServ = authServ;
        this.router = router;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.sucProv = sucProv;
        this.qrScanner = qrScanner;
        this.platform = platform;
        this.usuarios = [];
        this.platform.backButton.subscribeWithPriority(0, () => {
            document.getElementsByTagName('body')[0].style.opacity = '1';
            this.qrScan.unsubscribe();
        });
    }
    ngOnInit() {
        this.usuarios = [];
    }
    OnSubmitLogin() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.email != "" && this.password != "") {
                console.log('Estas en la funcion');
                this.authServ.login(this.email, this.password).then(res => {
                    this.authServ.ingresoEmail(this.email).subscribe(ingreso => {
                        this.ingresos = ingreso;
                        console.log("estos son los datos del usuario logueado", this.ingresos);
                        this.ingresos.forEach(element => {
                            const id = element.idUser;
                            const idSucursal = element.sucursal;
                            this.router.navigate(['/tienda', id]);
                            this.mensaje();
                            localStorage.setItem("uid", id);
                            localStorage.setItem("isLogin", 'true');
                        });
                    });
                }).catch(err => {
                    alert('hubo un error en correo');
                    this.mensaje_error();
                });
            }
            else {
                const toast = yield this.toastCtrl.create({
                    message: 'Los datos estan incompletos',
                    duration: 2000
                });
                yield toast.present();
            }
        });
    }
    mensaje() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                cssClass: 'my-custom-class',
                message: 'Iniciando Sesión',
                duration: 2000
            });
            yield loading.present();
            const { role, data } = yield loading.onDidDismiss();
            console.log('Loading dismissed!');
        });
    }
    mensaje_error() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: 'Correo o Contraseña Invalidos, vuelva a intentarlo',
                duration: 4000
            });
            yield toast.present();
        });
    }
    atras() {
        this.router.navigate(['/login']);
    }
    view() {
        // Optionally request the permission early
        this.qrScanner.prepare()
            .then((status) => {
            if (status.authorized) {
                // camera permission was granted
                // start scanning
                let scanSub = this.qrScanner.scan().subscribe((text) => {
                    console.log('Scanned something', text);
                    alert("precion");
                    this.qrScanner.hide(); // hide camera preview
                    scanSub.unsubscribe(); // stop scanning
                });
                this.qrScanner.show();
                document.getElementsByTagName('body')[0].style.opacity = '0';
                this.qrScan = this.qrScanner.scan().subscribe((textFound) => {
                    document.getElementsByTagName("body")[0].style.opacity = "1";
                    this.qrScanner.hide();
                    this.qrScan.unsubscribe();
                    this.qrText = textFound;
                    alert("precion configuracion");
                }), (err) => {
                    alert(JSON.stringify(err));
                };
            }
            else if (status.denied) {
                // camera permission was permanently denied
                // you must use QRScanner.openSettings() method to guide the user to the settings page
                // then they can grant the permission from there
            }
            else {
                // permission was denied, but not permanently. You can ask for permission again at a later time.
            }
        })
            .catch((e) => console.log('Error is', e));
    }
};
LoginCorreoPage.ctorParameters = () => [
    { type: src_app_servicio_usuario_service__WEBPACK_IMPORTED_MODULE_5__["UsuarioService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: src_app_servicio_sucursal_service__WEBPACK_IMPORTED_MODULE_4__["SucursalService"] },
    { type: _ionic_native_qr_scanner_ngx__WEBPACK_IMPORTED_MODULE_6__["QRScanner"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] }
];
LoginCorreoPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login-correo',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./login-correo.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/component/login-correo/login-correo.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./login-correo.page.scss */ "./src/app/component/login-correo/login-correo.page.scss")).default]
    })
], LoginCorreoPage);



/***/ }),

/***/ "./src/app/servicio/sucursal.service.ts":
/*!**********************************************!*\
  !*** ./src/app/servicio/sucursal.service.ts ***!
  \**********************************************/
/*! exports provided: SucursalService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SucursalService", function() { return SucursalService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");



//import 'rxjs/add/operator/map';

let SucursalService = class SucursalService {
    //server: string = "http://localhost/server_api_nowaitqr/";
    constructor(http) {
        this.http = http;
        this.server = "https://proyectosinternos.com/NoWaitQR/server_api_nowaitqr/";
    }
    // listar() {
    //   return this.http.get('https://proyectosinternos.com/NoWaitQR/select_ciudad.php/');
    // }
    // getAll(){
    //   return this.http.get<[Usuario]>(this.url);
    // }
    postData(body, file) {
        let type = "application/json; charset=UTF-8";
        let headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({ 'Content-Type': type });
        let options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        return this.http.post(this.server + file, JSON.stringify(body), options)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(res => res.json()));
    }
};
SucursalService.ctorParameters = () => [
    { type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"] }
];
SucursalService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], SucursalService);



/***/ })

}]);
//# sourceMappingURL=component-login-correo-login-correo-module-es2015.js.map