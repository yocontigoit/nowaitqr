(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["component-register-register-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/component/register/register.page.html":
    /*!*********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/component/register/register.page.html ***!
      \*********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentRegisterRegisterPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- <ion-header style=\"background-color: #192938;\">\n  <ion-toolbar>\n    <ion-buttons start>\n      <button ion-button icon-only (click)=\"atras()\" id=\"iconos\">\n        <ion-icon name=\"arrow-back-circle-outline\"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-content class=\"cointer_inicio\">\n  \n  <ion-buttons start style=\"background-color: #192938;\">\n    <button ion-button icon-only (click)=\"atras()\" id=\"iconos\">\n      <ion-icon name=\"arrow-back-circle-outline\"></ion-icon>\n    </button>\n  </ion-buttons>\n\n  <br><br>\n\n  <p id=\"inicio\"> REGISTRO</p>\n\n    <ion-list>\n\n      <br> <br> <br>\n\n      <ion-item>\n        <ion-label position=\"floating\" id=\"frase\" style=\"color: #fff;font-size:larger;\">Nombre</ion-label>\n        <ion-input type=\"text\" [(ngModel)]=\"nombre\" name=\"nombre\" style=\"color: #fff;\"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label position=\"floating\" id=\"frase\" style=\"color: #fff;font-size:larger;\">Correo electrónico</ion-label>\n        <ion-input type=\"text\" [(ngModel)]=\"email\" name=\"email\" style=\"color: #fff;\"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label position=\"floating\" id=\"frase\" style=\"color: #fff;font-size:larger;\">Contraseña</ion-label>\n        <ion-input type=\"password\" [(ngModel)]=\"password\" name=\"password\" style=\"color: #fff;\"></ion-input>\n      </ion-item>\n      <br> <br> <br> <br> <br>\n      <ion-button expand=\"block\" padding color=\"dark\" id=\"frase\" (click)=\"OnCreateLogin()\">Registrar</ion-button>\n    </ion-list>\n\n\n\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/component/register/register-routing.module.ts":
    /*!***************************************************************!*\
      !*** ./src/app/component/register/register-routing.module.ts ***!
      \***************************************************************/

    /*! exports provided: RegisterPageRoutingModule */

    /***/
    function srcAppComponentRegisterRegisterRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RegisterPageRoutingModule", function () {
        return RegisterPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _register_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./register.page */
      "./src/app/component/register/register.page.ts");

      var routes = [{
        path: '',
        component: _register_page__WEBPACK_IMPORTED_MODULE_3__["RegisterPage"]
      }];

      var RegisterPageRoutingModule = function RegisterPageRoutingModule() {
        _classCallCheck(this, RegisterPageRoutingModule);
      };

      RegisterPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], RegisterPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/component/register/register.module.ts":
    /*!*******************************************************!*\
      !*** ./src/app/component/register/register.module.ts ***!
      \*******************************************************/

    /*! exports provided: RegisterPageModule */

    /***/
    function srcAppComponentRegisterRegisterModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RegisterPageModule", function () {
        return RegisterPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _register_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./register-routing.module */
      "./src/app/component/register/register-routing.module.ts");
      /* harmony import */


      var _register_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./register.page */
      "./src/app/component/register/register.page.ts");

      var RegisterPageModule = function RegisterPageModule() {
        _classCallCheck(this, RegisterPageModule);
      };

      RegisterPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _register_routing_module__WEBPACK_IMPORTED_MODULE_5__["RegisterPageRoutingModule"]],
        declarations: [_register_page__WEBPACK_IMPORTED_MODULE_6__["RegisterPage"]]
      })], RegisterPageModule);
      /***/
    },

    /***/
    "./src/app/component/register/register.page.scss":
    /*!*******************************************************!*\
      !*** ./src/app/component/register/register.page.scss ***!
      \*******************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentRegisterRegisterPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "#container {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 50%;\n  transform: translateY(-50%);\n}\n\n#titulo {\n  font-size: x-large;\n  color: #676767;\n  text-transform: uppercase;\n}\n\n#logo {\n  color: #3b5998;\n  height: 50px;\n}\n\n#logoG {\n  color: #c22222;\n  height: 50px;\n}\n\n#icono {\n  height: 50px;\n}\n\n#boton {\n  width: 80%;\n  font-weight: bold;\n  left: 0;\n  right: 0;\n  margin-top: 30%;\n  margin-left: 10%;\n  font-family: sans-serif;\n}\n\n#frase {\n  font-family: sans-serif;\n}\n\n#botonE {\n  width: 80%;\n  font-weight: bold;\n  left: 0;\n  right: 0;\n  margin-left: 10%;\n}\n\n#inicio {\n  font-size: x-large;\n  color: #fff;\n  text-align: center;\n  font-weight: bold;\n  font-family: sans-serif;\n}\n\nion-slide {\n  height: calc(100vh - 140px);\n}\n\n#iconos {\n  background-color: #192938;\n  font-size: 40px;\n  color: #fff;\n}\n\n.cointer_inicio {\n  --ion-background-color:#192938;\n  background-color: #192938;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50L3JlZ2lzdGVyL3JlZ2lzdGVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLFFBQUE7RUFDQSwyQkFBQTtBQUNKOztBQUVBO0VBQ0ksa0JBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7QUFDSjs7QUFFQTtFQUNJLGNBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSxjQUFBO0VBQ0EsWUFBQTtBQUNKOztBQUVBO0VBQ0ksWUFBQTtBQUNKOztBQUVBO0VBQ0ksVUFBQTtFQUNBLGlCQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSx1QkFBQTtBQUNKOztBQUVBO0VBQ0ksdUJBQUE7QUFDSjs7QUFFQTtFQUNJLFVBQUE7RUFDQSxpQkFBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0VBRUEsZ0JBQUE7QUFBSjs7QUFHQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSx1QkFBQTtBQUFKOztBQUdBO0VBQ0ksMkJBQUE7QUFBSjs7QUFHQTtFQUNJLHlCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUFBSjs7QUFHQTtFQUNJLDhCQUFBO0VBQ0EseUJBQUE7QUFBSiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudC9yZWdpc3Rlci9yZWdpc3Rlci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjY29udGFpbmVyIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICByaWdodDogMDtcclxuICAgIHRvcDogNTAlO1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xyXG59XHJcblxyXG4jdGl0dWxvIHtcclxuICAgIGZvbnQtc2l6ZTogeC1sYXJnZTtcclxuICAgIGNvbG9yOiAjNjc2NzY3O1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxufVxyXG5cclxuI2xvZ28ge1xyXG4gICAgY29sb3I6ICMzYjU5OTg7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbn1cclxuXHJcbiNsb2dvRyB7XHJcbiAgICBjb2xvcjogI2MyMjIyMjtcclxuICAgIGhlaWdodDogNTBweDtcclxufVxyXG5cclxuI2ljb25vIHtcclxuICAgIGhlaWdodDogNTBweDtcclxufVxyXG5cclxuI2JvdG9uIHtcclxuICAgIHdpZHRoOiA4MCU7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICByaWdodDogMDtcclxuICAgIG1hcmdpbi10b3A6IDMwJTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMCU7ICAgIFxyXG4gICAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XHJcbn1cclxuXHJcbiNmcmFzZSB7XHJcbiAgICBmb250LWZhbWlseTogc2Fucy1zZXJpZjtcclxufVxyXG5cclxuI2JvdG9uRSB7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgcmlnaHQ6IDA7XHJcbiAgICAvL21hcmdpbi10b3A6IDMwJTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMCU7ICAgIFxyXG59XHJcblxyXG4jaW5pY2lvIHtcclxuICAgIGZvbnQtc2l6ZTogeC1sYXJnZTtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBmb250LWZhbWlseTogc2Fucy1zZXJpZjtcclxufVxyXG5cclxuaW9uLXNsaWRlIHtcclxuICAgIGhlaWdodDogY2FsYygxMDB2aCAtIDE0MHB4KTtcclxufVxyXG5cclxuI2ljb25vcyB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTkyOTM4O1xyXG4gICAgZm9udC1zaXplOiA0MHB4O1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbn1cclxuXHJcbi5jb2ludGVyX2luaWNpb3tcclxuICAgIC0taW9uLWJhY2tncm91bmQtY29sb3I6IzE5MjkzODtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IzE5MjkzODtcclxufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/component/register/register.page.ts":
    /*!*****************************************************!*\
      !*** ./src/app/component/register/register.page.ts ***!
      \*****************************************************/

    /*! exports provided: RegisterPage */

    /***/
    function srcAppComponentRegisterRegisterPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RegisterPage", function () {
        return RegisterPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var src_app_servicio_usuario_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/servicio/usuario.service */
      "./src/app/servicio/usuario.service.ts");
      /* harmony import */


      var firebase_app__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! firebase/app */
      "./node_modules/firebase/app/dist/index.esm.js");
      /* harmony import */


      var firebase_database__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! firebase/database */
      "./node_modules/firebase/database/dist/index.esm.js");
      /* harmony import */


      var firebase_auth__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! firebase/auth */
      "./node_modules/firebase/auth/dist/index.esm.js");
      /* harmony import */


      var firebase_firestore__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! firebase/firestore */
      "./node_modules/firebase/firestore/dist/index.esm.js");
      /* harmony import */


      var _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @ionic-native/facebook/ngx */
      "./node_modules/@ionic-native/facebook/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @angular/fire/firestore */
      "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");
      /* harmony import */


      var src_app_servicio_notification_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! src/app/servicio/notification.service */
      "./src/app/servicio/notification.service.ts"); //import { auth } from 'firebase';


      var RegisterPage = /*#__PURE__*/function () {
        function RegisterPage(authServ, router, fb, loadingCtrl, db, toastCtrl, pushnot) {
          _classCallCheck(this, RegisterPage);

          this.authServ = authServ;
          this.router = router;
          this.fb = fb;
          this.loadingCtrl = loadingCtrl;
          this.db = db;
          this.toastCtrl = toastCtrl;
          this.pushnot = pushnot;
        }

        _createClass(RegisterPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "segmentChanged",
          value: function segmentChanged($event) {
            console.log($event);
          }
        }, {
          key: "OnCreateLogin",
          value: function OnCreateLogin() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _this = this;

              var toast;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      if (!(this.nombre != "" && this.email != "" && this.password != "")) {
                        _context.next = 4;
                        break;
                      }

                      this.authServ.createEmail(this.nombre, this.email, this.password).then(function (res) {
                        console.log("datos del then", res);

                        _this.authServ.ingresoEmail(_this.email).subscribe(function (ingreso) {
                          _this.ingresos = ingreso;
                          console.log("estos son los datos del usuario logueado", _this.ingresos);

                          _this.ingresos.forEach(function (element) {
                            var id = element.idUser;
                            var estatus = element.estatus;

                            if (estatus == 1) {
                              _this.router.navigate(['/ciudad', id]);

                              _this.mensaje();

                              localStorage.setItem("uid", id);
                              localStorage.setItem("isLogin", 'true');
                            }

                            if (estatus == 0) {
                              _this.pushnot.init_notification(id);

                              localStorage.setItem("uid", id);
                              localStorage.setItem("isLogin", 'true'); // this.pushnot.continue('0dGzNzHl4NRS4z6L3tNUhB7XVgE3' , 1 , '62280ad0-e157-4625-a93f-65f9afb1b305');
                            } // this.router.navigate(['/ciudad', id]);
                            // this.mensaje();

                          });
                        });
                      })["catch"](function (err) {
                        alert('hubo un error en email+');
                      });
                      _context.next = 8;
                      break;

                    case 4:
                      _context.next = 6;
                      return this.toastCtrl.create({
                        message: 'Falta algun campo por llenar',
                        duration: 3000
                      });

                    case 6:
                      toast = _context.sent;
                      toast.present();

                    case 8:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "signInWithFacebook",
          value: function signInWithFacebook() {
            var _this2 = this;

            this.fb.login(['public_profile', 'email']).then(function (res) {
              var facebookCredential = firebase_app__WEBPACK_IMPORTED_MODULE_5__["default"].auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
              firebase_app__WEBPACK_IMPORTED_MODULE_5__["default"].auth().signInWithCredential(facebookCredential).then(function (user) {
                //console.log('datos de la sesion del user',user);
                _this2.us = user.user;
                console.log('Usuario: ', JSON.stringify(_this2.us));
                localStorage.setItem("uid", _this2.us.uid); //console.log('userID',this.us.uid);

                _this2.url = "?height=500";
                _this2.usPhotoUrl = _this2.us.photoURL + _this2.url;

                _this2.authServ.cargarUsuario(_this2.us.displayName, _this2.us.email, _this2.us.photoURL, _this2.us.uid); //sacar el codigo del usuario


                _this2.authServ.getCodigo(_this2.us.uid).subscribe(function (co) {
                  _this2.codigos = co;
                  console.log('datos tabla user', _this2.codigos.length);

                  if (_this2.codigos.length == 0) {
                    console.log('agregar tel');

                    _this2.db.collection('users').doc(_this2.authServ.usuario.uid).set({
                      idUser: _this2.authServ.usuario.uid,
                      displayName: _this2.us.displayName,
                      email: _this2.us.email,
                      photoURL: _this2.us.photoURL
                    }).then(function () {
                      return _this2.mensaje();
                    })["catch"](function (error) {
                      console.error("Error adding document: ", error);
                    });
                  }

                  if (_this2.codigos.length == 1) {
                    _this2.router.navigate(['/ciudad', _this2.us.uid]);
                  }
                }); // this.navCtrl.setRoot(HomePage);

              })["catch"](function (e) {
                return alert('Error de autenticación' + JSON.stringify(e));
              });
            });
          }
        }, {
          key: "mensaje",
          value: function mensaje() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var loading, _yield$loading$onDidD, role, data;

              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.loadingCtrl.create({
                        cssClass: 'my-custom-class',
                        message: 'Iniciando Sesión',
                        duration: 2000
                      });

                    case 2:
                      loading = _context2.sent;
                      _context2.next = 5;
                      return loading.present();

                    case 5:
                      _context2.next = 7;
                      return loading.onDidDismiss();

                    case 7:
                      _yield$loading$onDidD = _context2.sent;
                      role = _yield$loading$onDidD.role;
                      data = _yield$loading$onDidD.data;
                      console.log('Loading dismissed!');

                    case 11:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "atras",
          value: function atras() {
            this.router.navigate(['/login']);
          }
        }]);

        return RegisterPage;
      }();

      RegisterPage.ctorParameters = function () {
        return [{
          type: src_app_servicio_usuario_service__WEBPACK_IMPORTED_MODULE_4__["UsuarioService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_9__["Facebook"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]
        }, {
          type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_10__["AngularFirestore"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]
        }, {
          type: src_app_servicio_notification_service__WEBPACK_IMPORTED_MODULE_11__["NotificationService"]
        }];
      };

      RegisterPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-register',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./register.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/component/register/register.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./register.page.scss */
        "./src/app/component/register/register.page.scss"))["default"]]
      })], RegisterPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=component-register-register-module-es5.js.map