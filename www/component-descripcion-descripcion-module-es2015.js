(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["component-descripcion-descripcion-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/component/descripcion/descripcion.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/component/descripcion/descripcion.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content padding>\n\n  <div id=\"container\">\n    <ion-grid>\n      <ion-row>\n        <ion-col size=\"1\">\n          <ion-icon name=\"arrow-back-circle-outline\" id=\"icono\" (click)=\"goToBack()\"></ion-icon>\n        </ion-col>\n        <ion-col size=\"10\">\n          <img src=\"./assets/icon/new_disigner_small.jpg\" width=\"70%\" style=\"margin-left: 20%;\" alt=\"\">\n          <!-- <div id=\"nombrePro\"><b id=\"frase\">No Wait</b> QR</div> -->\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n\n  <ion-card id=\"cuadro\">\n    <ion-card-header>\n    </ion-card-header>\n  \n    <ion-card-content>\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card id=\"cuadroTwo\">\n    <ion-card-header>\n    </ion-card-header>\n  \n    <ion-card-content>\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card id=\"cuadroThree\">\n    <ion-card-header>\n    </ion-card-header>\n  \n    <ion-card-content>\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card id=\"cuadroFour\">\n    <ion-card-header>\n      <div *ngFor=\"let tienda of tiendaSec\">\n        <p id=\"nombre\">{{tienda.nombre_tienda}}</p>\n      </div>\n      <div *ngFor=\"let suc of sucursalSec\">\n        <p id=\"nombre_ti\">{{suc.nombre_suc}}</p>\n      </div>\n    </ion-card-header>\n\n    <br>\n\n    <ion-card-content>\n      <p id=\"turno\">Personas esperando su turno:</p>\n      <p id=\"numTurno\">000{{numTurno}}</p>\n    </ion-card-content>\n  </ion-card>\n\n  <br> <br>\n\n  <ion-button id=\"botonc\" (click)=\"goToFila(numTurno)\" color=\"dark\">\n    Obtener Turno\n  </ion-button>\n\n  <!-- <br>\n\n  <ion-button id=\"botonb\" (click)=\"goToBack()\" color=\"dark\">\n    Atrás\n  </ion-button> -->\n\n</ion-content>\n\n");

/***/ }),

/***/ "./src/app/component/descripcion/descripcion-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/component/descripcion/descripcion-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: DescripcionPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DescripcionPageRoutingModule", function() { return DescripcionPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _descripcion_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./descripcion.page */ "./src/app/component/descripcion/descripcion.page.ts");




const routes = [
    {
        path: '',
        component: _descripcion_page__WEBPACK_IMPORTED_MODULE_3__["DescripcionPage"]
    }
];
let DescripcionPageRoutingModule = class DescripcionPageRoutingModule {
};
DescripcionPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DescripcionPageRoutingModule);



/***/ }),

/***/ "./src/app/component/descripcion/descripcion.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/component/descripcion/descripcion.module.ts ***!
  \*************************************************************/
/*! exports provided: DescripcionPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DescripcionPageModule", function() { return DescripcionPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _descripcion_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./descripcion-routing.module */ "./src/app/component/descripcion/descripcion-routing.module.ts");
/* harmony import */ var _descripcion_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./descripcion.page */ "./src/app/component/descripcion/descripcion.page.ts");







let DescripcionPageModule = class DescripcionPageModule {
};
DescripcionPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _descripcion_routing_module__WEBPACK_IMPORTED_MODULE_5__["DescripcionPageRoutingModule"]
        ],
        declarations: [_descripcion_page__WEBPACK_IMPORTED_MODULE_6__["DescripcionPage"]]
    })
], DescripcionPageModule);



/***/ }),

/***/ "./src/app/component/descripcion/descripcion.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/component/descripcion/descripcion.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#cuadro {\n  background-color: black;\n  height: 350px;\n  width: 300px;\n  border-radius: 25px;\n  margin-left: 5%;\n  position: fixed;\n}\n\n#cuadroTwo {\n  background-color: #797979;\n  height: 350px;\n  width: 300px;\n  border-radius: 25px;\n  margin-top: 17%;\n  margin-left: 10%;\n  position: fixed;\n}\n\n#cuadroThree {\n  background-color: #484646;\n  height: 350px;\n  width: 300px;\n  border-radius: 25px;\n  margin-top: 27%;\n  margin-left: 15%;\n  position: fixed;\n}\n\n#cuadroFour {\n  background-color: #b3b3b3;\n  height: 350px;\n  width: 300px;\n  border-radius: 25px;\n  margin-top: 37%;\n  margin-left: 10%;\n  position: fixed;\n}\n\n#nombre {\n  font-weight: bold;\n  font-size: x-large;\n  color: black;\n  text-align: center;\n  text-transform: uppercase;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n#nombre_ti {\n  text-align: center;\n  margin-top: -15px;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n#botonc {\n  width: 80%;\n  font-size: 71%;\n  font-weight: bold;\n  margin-left: 10%;\n  position: fixed;\n  margin-top: 130%;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n#turno {\n  font-size: large;\n  text-align: center;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n#numTurno {\n  font-size: xxx-large;\n  font-family: Arial, Helvetica, sans-serif;\n  text-align: center;\n}\n\n#botonb {\n  width: 80%;\n  font-size: 71%;\n  font-weight: bold;\n  margin-left: 10%;\n  position: fixed;\n  margin-top: 135%;\n}\n\n#nombrePro {\n  font-size: xx-large;\n  color: #676767;\n  font-family: sans-serif;\n  text-align: center;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n#frase {\n  font-weight: bold;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n#icono {\n  color: white;\n  font-size: 40px;\n  background-color: #192938;\n  margin-top: 18px;\n}\n\n#container {\n  left: 0;\n  right: 0;\n  --ion-background-color:#192938;\n  background-color: #192938;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50L2Rlc2NyaXBjaW9uL2Rlc2NyaXBjaW9uLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHVCQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBRUE7RUFDSSx5QkFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBRUE7RUFDSSx5QkFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBRUE7RUFDSSx5QkFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBRUE7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSx5Q0FBQTtBQUNKOztBQUVBO0VBQ0ksa0JBQUE7RUFDQSxpQkFBQTtFQUNBLHlDQUFBO0FBQ0o7O0FBRUE7RUFDSSxVQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5Q0FBQTtBQUNKOztBQUVBO0VBQ0ksZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLHlDQUFBO0FBQ0o7O0FBRUE7RUFDSSxvQkFBQTtFQUNBLHlDQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFFQTtFQUNJLFVBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQUNKOztBQUVBO0VBQ0ksbUJBQUE7RUFDQSxjQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtFQUNBLHlDQUFBO0FBQ0o7O0FBRUE7RUFDSSxpQkFBQTtFQUNBLHlDQUFBO0FBQ0o7O0FBRUE7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLHlCQUFBO0VBQ0EsZ0JBQUE7QUFDSjs7QUFFQTtFQUNJLE9BQUE7RUFDQSxRQUFBO0VBQ0EsOEJBQUE7RUFDQSx5QkFBQTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50L2Rlc2NyaXBjaW9uL2Rlc2NyaXBjaW9uLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNjdWFkcm97XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxuICAgIGhlaWdodDogMzUwcHg7XHJcbiAgICB3aWR0aDogMzAwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDUlO1xyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG59XHJcblxyXG4jY3VhZHJvVHdve1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzc5Nzk3OTtcclxuICAgIGhlaWdodDogMzUwcHg7XHJcbiAgICB3aWR0aDogMzAwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgbWFyZ2luLXRvcDogMTclO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwJTtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxufVxyXG5cclxuI2N1YWRyb1RocmVlIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM0ODQ2NDY7XHJcbiAgICBoZWlnaHQ6IDM1MHB4O1xyXG4gICAgd2lkdGg6IDMwMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgIG1hcmdpbi10b3A6IDI3JTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxNSU7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbn1cclxuXHJcbiNjdWFkcm9Gb3VyIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNiM2IzYjM7XHJcbiAgICBoZWlnaHQ6IDM1MHB4O1xyXG4gICAgd2lkdGg6IDMwMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgIG1hcmdpbi10b3A6IDM3JTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMCU7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbn1cclxuXHJcbiNub21icmUge1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBmb250LXNpemU6IHgtbGFyZ2U7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgZm9udC1mYW1pbHk6IEFyaWFsLCBIZWx2ZXRpY2EsIHNhbnMtc2VyaWY7XHJcbn1cclxuXHJcbiNub21icmVfdGkge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbWFyZ2luLXRvcDogLTE1cHg7XHJcbiAgICBmb250LWZhbWlseTogQXJpYWwsIEhlbHZldGljYSwgc2Fucy1zZXJpZjtcclxufVxyXG5cclxuI2JvdG9uYyB7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICAgZm9udC1zaXplOiA3MSU7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMCU7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICBtYXJnaW4tdG9wOiAxMzAlO1xyXG4gICAgZm9udC1mYW1pbHk6IEFyaWFsLCBIZWx2ZXRpY2EsIHNhbnMtc2VyaWY7XHJcbn1cclxuXHJcbiN0dXJub3tcclxuICAgIGZvbnQtc2l6ZTogbGFyZ2U7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LWZhbWlseTogQXJpYWwsIEhlbHZldGljYSwgc2Fucy1zZXJpZjtcclxufVxyXG5cclxuI251bVR1cm5ve1xyXG4gICAgZm9udC1zaXplOiB4eHgtbGFyZ2U7XHJcbiAgICBmb250LWZhbWlseTogQXJpYWwsIEhlbHZldGljYSwgc2Fucy1zZXJpZjtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuI2JvdG9uYntcclxuICAgIHdpZHRoOiA4MCU7XHJcbiAgICBmb250LXNpemU6IDcxJTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwJTtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIG1hcmdpbi10b3A6IDEzNSU7XHJcbn1cclxuXHJcbiNub21icmVQcm8ge1xyXG4gICAgZm9udC1zaXplOiB4eC1sYXJnZTtcclxuICAgIGNvbG9yOiAjNjc2NzY3O1xyXG4gICAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LWZhbWlseTogQXJpYWwsIEhlbHZldGljYSwgc2Fucy1zZXJpZjtcclxufVxyXG4gIFxyXG4jZnJhc2Uge1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBmb250LWZhbWlseTogQXJpYWwsIEhlbHZldGljYSwgc2Fucy1zZXJpZjtcclxufVxyXG5cclxuI2ljb25vIHtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogNDBweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMxOTI5Mzg7XHJcbiAgICBtYXJnaW4tdG9wOiAxOHB4O1xyXG59XHJcblxyXG4jY29udGFpbmVyIHtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICByaWdodDogMDtcclxuICAgIC0taW9uLWJhY2tncm91bmQtY29sb3I6IzE5MjkzODtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMxOTI5Mzg7XHJcbiAgfSJdfQ== */");

/***/ }),

/***/ "./src/app/component/descripcion/descripcion.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/component/descripcion/descripcion.page.ts ***!
  \***********************************************************/
/*! exports provided: DescripcionPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DescripcionPage", function() { return DescripcionPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_servicio_sucursal_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/servicio/sucursal.service */ "./src/app/servicio/sucursal.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var src_app_servicio_noti_turno_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/servicio/noti-turno.service */ "./src/app/servicio/noti-turno.service.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/__ivy_ngcc__/fesm2015/http.js");








let DescripcionPage = class DescripcionPage {
    constructor(router, route, sucProv, loadingCtrl, http, notiSrv) {
        this.router = router;
        this.route = route;
        this.sucProv = sucProv;
        this.loadingCtrl = loadingCtrl;
        this.http = http;
        this.notiSrv = notiSrv;
        this.sucursalSec = [];
        this.tiendaSec = [];
        this.turno = [];
        this.userApp = [];
    }
    ngOnInit() {
        let id = this.route.snapshot.paramMap.get("id");
        console.log("este es el idUser:", id);
        this.idUser = id;
        let idSucursal = this.route.snapshot.paramMap.get("idSucursal");
        console.log("este es el idSucursal:", idSucursal);
        this.idSucursal = parseInt(idSucursal);
        let idTienda = this.route.snapshot.paramMap.get("idTienda");
        console.log("este es el idTienda:", idTienda);
        this.idTienda = parseInt(idTienda);
        let playerID = this.route.snapshot.paramMap.get('playerID');
        console.log("este es el playerID", playerID);
        this.playerID = playerID;
        this.sucursalSec = [];
        this.tiendaSec = [];
        this.turno = [];
        this.userApp = [];
        var today = moment__WEBPACK_IMPORTED_MODULE_5__().format('YYYY-MM-DD');
        console.log("variable today", today);
        this.momentos = today;
        console.log("variable momentos", this.momentos);
        this.momentoActual = new Date();
        this.hora = this.momentoActual.getHours();
        this.minuto = this.momentoActual.getMinutes();
        this.segundo = this.momentoActual.getSeconds();
        this.horas = this.hora + ':' + this.minuto;
        console.log("Esta es la hora actual", this.hora, ':', this.minuto, "Hora completa", this.horas);
        this.goToInformation();
        this.goToInfoTurno();
        this.goToInfoTienda();
        this.goToInfoUserApp();
    }
    goToInformation() {
        return new Promise(resolve => {
            let body = {
                idSucursal: this.idSucursal,
                accion: 'getInfoSuc'
            };
            this.sucProv.postData(body, 'establecimiento.php').subscribe(data => {
                for (let sucursal of data.result) {
                    this.sucursalSec.push(sucursal);
                    console.log("Datos de la sucursal seleccionada", this.sucursalSec);
                }
                resolve(true);
            });
        });
    }
    goToInfoTienda() {
        return new Promise(resolve => {
            let body = {
                idTienda: this.idTienda,
                accion: 'getInfoTienda'
            };
            this.sucProv.postData(body, 'establecimiento.php').subscribe(data => {
                for (let tienda of data.result) {
                    this.tiendaSec.push(tienda);
                    console.log("Datos de la tienda seleccionada", this.tiendaSec);
                }
                resolve(true);
            });
        });
    }
    goToInfoTurno() {
        return new Promise(resolve => {
            let body = {
                idTienda: this.idTienda,
                estatus: 'pendiente',
                fecha: this.momentos,
                accion: 'getInfoTurno'
            };
            this.sucProv.postData(body, 'establecimiento.php').subscribe(data => {
                console.log("data result", data.result.length);
                if (data.result.length >= 1) {
                    for (let turnos of data.result) {
                        this.turno.push(turnos);
                        console.log("Turnos", this.turno);
                        console.log("numero de turnos", this.turno.length);
                        this.numTurno = this.turno.length;
                    }
                    resolve(true);
                }
                if (data.result.length == 0) {
                    this.numTurno = data.result.length;
                }
            });
        });
    }
    goToInfoUserApp() {
        return new Promise(resolve => {
            let body = {
                idTienda: this.idTienda,
                accion: 'getInfoUserApp'
            };
            this.sucProv.postData(body, 'userapp.php').subscribe(data => {
                for (let Userapp of data.result) {
                    this.userApp.push(Userapp);
                    console.log("Datos del UserApp", this.userApp);
                    this.playerUser = Userapp.playerID;
                }
                resolve(true);
            });
        });
    }
    goToFila(numTurn) {
        console.log("Si da clicky numero de turno", numTurn);
        var sumTurno = numTurn + 1;
        var today = moment__WEBPACK_IMPORTED_MODULE_5__().format('YYYY-MM-DD');
        console.log("variable today", today);
        this.momentos = today;
        console.log("variable momentos", this.momentos);
        var d = new Date();
        var n = d.getHours();
        let data = JSON.stringify({
            estatus: 'pendiente',
            fecha: this.momentos,
            hora: this.horas,
            hora_complete: n,
            numTurno: sumTurno,
            idTienda: this.idTienda,
            idUser: this.idUser,
            playerID: this.playerUser,
            accion: 'add_turnos'
        });
        let url = 'https://proyectosinternos.com/NoWaitQR/server_api_nowaitqr/add_turno.php';
        let type = "application/json; charset=UTF-8";
        let headers = new _angular_http__WEBPACK_IMPORTED_MODULE_7__["Headers"]({ "Content-Type": type });
        let options = new _angular_http__WEBPACK_IMPORTED_MODULE_7__["RequestOptions"]({ headers: headers });
        return this.http.post(url, data, options).subscribe(res => {
            console.log('Se ha agregado', res);
            this.mensaje();
            this.notification(this.playerUser);
            this.router.navigate(['/turno', this.idUser, this.idTienda, sumTurno]);
        });
    }
    // goToFila2(numTurn){
    //   console.log("Si da clicky numero de turno", numTurn);
    //   var sumTurno = numTurn + 1;
    //   var today = moment().format('YYYY-MM-DD');
    //   console.log("variable today", today);
    //   var d = new Date();
    //   var n = d.getHours();
    //   let body = {
    //     estatus: 'pendiente',
    //     // fecha: this.momentos,
    //     // hora: this.horas,
    //     // hora_complete: n,
    //     // numTurno: sumTurno,
    //     // idTienda: this.idTienda,
    //     // idUser: this.idUser,
    //     // playerID: this.playerUser,
    //     accion: 'add_turnos'
    //   };
    //   console.log("pasa el body", "fecha", this.momentos, "hora" ,this.horas , "hora_complete" , n , 
    //   "numTurno" , sumTurno, "idTienda" , this.idTienda , "idUser" , this.idUser, "playerID" , this.playerUser);
    //   this.sucProv.postData(body, 'turno.php').subscribe(async data => {
    //     if (data.success) {
    //       console.log("Se ha agregado el turno");
    //       // this.notification();
    //       // this.mensaje();
    //       this.router.navigate(['/turno', this.idUser, this.idTienda, sumTurno]); 
    //     }
    //   })
    // }
    notification(playerUser) {
        console.log("playerID", playerUser);
        this.notiSrv.insertaNotificacion(playerUser);
    }
    goToBack() {
        this.router.navigate(['/tienda', this.idUser]);
    }
    mensaje() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                cssClass: 'my-custom-class',
                message: 'Obteniendo Turno',
                duration: 2000
            });
            yield loading.present();
            const { role, data } = yield loading.onDidDismiss();
            console.log('Loading dismissed!');
        });
    }
};
DescripcionPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: src_app_servicio_sucursal_service__WEBPACK_IMPORTED_MODULE_4__["SucursalService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _angular_http__WEBPACK_IMPORTED_MODULE_7__["Http"] },
    { type: src_app_servicio_noti_turno_service__WEBPACK_IMPORTED_MODULE_6__["NotiTurnoService"] }
];
DescripcionPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-descripcion',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./descripcion.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/component/descripcion/descripcion.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./descripcion.page.scss */ "./src/app/component/descripcion/descripcion.page.scss")).default]
    })
], DescripcionPage);



/***/ }),

/***/ "./src/app/servicio/noti-turno.service.ts":
/*!************************************************!*\
  !*** ./src/app/servicio/noti-turno.service.ts ***!
  \************************************************/
/*! exports provided: NotiTurnoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotiTurnoService", function() { return NotiTurnoService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/__ivy_ngcc__/fesm2015/http.js");



let NotiTurnoService = class NotiTurnoService {
    constructor(http) {
        this.http = http;
    }
    insertaNotificacion(playerUser) {
        let data = JSON.stringify({
            titulo: 'Nuevo Cliente',
            descripcion: 'Se encunetra un nuevo cliente esperando en la fila',
            idUsers: playerUser
        });
        // let url = 'https://proyectosinternos.com/server_ez/notiAdmin.php/';
        let url = 'https://proyectosinternos.com/NoWaitQR_Notificacion/notificacion.php/';
        let type = "application/json; charset=UTF-8";
        let headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({ "Content-Type": type });
        let options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        return this.http.post(url, data, options).subscribe(res => {
            // console.log('Que pasa ', res._body)
        });
    }
};
NotiTurnoService.ctorParameters = () => [
    { type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"] }
];
NotiTurnoService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], NotiTurnoService);



/***/ })

}]);
//# sourceMappingURL=component-descripcion-descripcion-module-es2015.js.map