(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["component-login-login-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/component/login/login.page.html":
    /*!***************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/component/login/login.page.html ***!
      \***************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentLoginLoginPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content class=\"cointer_inicio\">\n\n  <div id=\"container\">\n\n    <img src=\"./assets/icon/new_disigner.jpeg\" alt=\"\">\n\n    <!-- <p style=\"font-size: xx-large;color: #676767;font-family: sans-serif;\"><b style=\"font-weight: bold;\">No Wait</b> QR</p> -->\n\n    <br> \n        \n    <div *ngIf=\"botonesA.estatus == 1\">\n      <ion-button id=\"boton\" (click)=\"singWithGoogle()\" color=\"light\">\n        <ion-icon name=\"logo-google\" id=\"logoG\"></ion-icon>\n        &nbsp;&nbsp;&nbsp;&nbsp;\n        Regístrate con Google\n      </ion-button>\n  \n      <br> <br>\n  \n      <ion-button id=\"boton\" (click)=\"signInWithFacebook()\" color=\"light\">\n        <ion-icon name=\"logo-facebook\" id=\"logo\"></ion-icon>\n        &nbsp;&nbsp;&nbsp;&nbsp;\n        Regístrate con Facebook\n      </ion-button>\n  \n      <br> <br>\n    </div>\n\n    <ion-button id=\"botonc\" (click)=\"signWithCorreo()\" color=\"dark\">\n      Regístrate con tu correo electrónico\n    </ion-button>\n\n    <br> <br> <br>\n\n    <p style=\"margin-left: 3%;text-align: center;font-family: sans-serif;\"> \n      ¿Ya te has registrado?\n      <a style=\"color: rgb(92, 177, 216);\" (click)=\"register()\">\n       Inicia Sesión \n      </a> en tu cuenta\n    </p>\n\n    <br> <br>\n  </div>\n  <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/component/login/login-routing.module.ts":
    /*!*********************************************************!*\
      !*** ./src/app/component/login/login-routing.module.ts ***!
      \*********************************************************/

    /*! exports provided: LoginPageRoutingModule */

    /***/
    function srcAppComponentLoginLoginRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function () {
        return LoginPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./login.page */
      "./src/app/component/login/login.page.ts");

      var routes = [{
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
      }];

      var LoginPageRoutingModule = function LoginPageRoutingModule() {
        _classCallCheck(this, LoginPageRoutingModule);
      };

      LoginPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], LoginPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/component/login/login.module.ts":
    /*!*************************************************!*\
      !*** ./src/app/component/login/login.module.ts ***!
      \*************************************************/

    /*! exports provided: LoginPageModule */

    /***/
    function srcAppComponentLoginLoginModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPageModule", function () {
        return LoginPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./login-routing.module */
      "./src/app/component/login/login-routing.module.ts");
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./login.page */
      "./src/app/component/login/login.page.ts");

      var LoginPageModule = function LoginPageModule() {
        _classCallCheck(this, LoginPageModule);
      };

      LoginPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"]],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
      })], LoginPageModule);
      /***/
    },

    /***/
    "./src/app/component/login/login.page.scss":
    /*!*************************************************!*\
      !*** ./src/app/component/login/login.page.scss ***!
      \*************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentLoginLoginPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "#container {\n  text-align: center;\n  --ion-background-color:#192938;\n  background-color: #192938;\n}\n\n#container strong {\n  font-size: 20px;\n  line-height: 26px;\n}\n\n#container p {\n  font-size: 16px;\n  line-height: 22px;\n  color: #8c8c8c;\n  margin: 0;\n}\n\n#container a {\n  text-decoration: none;\n}\n\n#ion-button {\n  background: white;\n  background: -webkit-gradient(left top, right top, color-stop(0%, white), color-stop(47%, #f6f6f6), color-stop(100%, #ededed));\n  background: linear-gradient(to right, white 0%, #f6f6f6 47%, #ededed 100%);\n  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\"#ffffff\", endColorstr=\"#ededed\", GradientType=1 );\n}\n\n#logo {\n  color: #3b5998;\n}\n\n#logoG {\n  color: #c22222;\n}\n\n#boton {\n  width: 80%;\n  font-weight: bold;\n  font-family: sans-serif;\n}\n\n#botonc {\n  width: 80%;\n  font-size: 71%;\n  font-weight: bold;\n  font-family: sans-serif;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50L2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0VBT0EsOEJBQUE7RUFDQSx5QkFBQTtBQUxKOztBQVFFO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FBTEo7O0FBUUU7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7RUFFQSxjQUFBO0VBRUEsU0FBQTtBQVBKOztBQVVFO0VBQ0UscUJBQUE7QUFQSjs7QUFVRTtFQUNFLGlCQUFBO0VBRUEsNkhBQUE7RUFJQSwwRUFBQTtFQUNBLG9IQUFBO0FBUEo7O0FBVUU7RUFDRSxjQUFBO0FBUEo7O0FBVUU7RUFDSSxjQUFBO0FBUE47O0FBVUU7RUFDRSxVQUFBO0VBRUEsaUJBQUE7RUFDQSx1QkFBQTtBQVJKOztBQVdFO0VBQ0UsVUFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLHVCQUFBO0FBUkoiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnQvbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2NvbnRhaW5lciB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgXHJcbiAgICAvLyBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAvLyBsZWZ0OiAwO1xyXG4gICAgLy8gcmlnaHQ6IDA7XHJcbiAgICAvLyB0b3A6IDUwJTtcclxuICAgIC8vIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcclxuICAgIC0taW9uLWJhY2tncm91bmQtY29sb3I6IzE5MjkzODtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IzE5MjkzODtcclxuICB9XHJcbiAgXHJcbiAgI2NvbnRhaW5lciBzdHJvbmcge1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDI2cHg7XHJcbiAgfVxyXG4gIFxyXG4gICNjb250YWluZXIgcCB7XHJcbiAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICBsaW5lLWhlaWdodDogMjJweDtcclxuICBcclxuICAgIGNvbG9yOiAjOGM4YzhjO1xyXG4gIFxyXG4gICAgbWFyZ2luOiAwO1xyXG4gIH1cclxuICBcclxuICAjY29udGFpbmVyIGEge1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gIH1cclxuICBcclxuICAjaW9uLWJ1dHRvbiB7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwyNTUsMjU1LDEpO1xyXG4gICAgYmFja2dyb3VuZDogLW1vei1saW5lYXItZ3JhZGllbnQobGVmdCwgcmdiYSgyNTUsMjU1LDI1NSwxKSAwJSwgcmdiYSgyNDYsMjQ2LDI0NiwxKSA0NyUsIHJnYmEoMjM3LDIzNywyMzcsMSkgMTAwJSk7XHJcbiAgICBiYWNrZ3JvdW5kOiAtd2Via2l0LWdyYWRpZW50KGxlZnQgdG9wLCByaWdodCB0b3AsIGNvbG9yLXN0b3AoMCUsIHJnYmEoMjU1LDI1NSwyNTUsMSkpLCBjb2xvci1zdG9wKDQ3JSwgcmdiYSgyNDYsMjQ2LDI0NiwxKSksIGNvbG9yLXN0b3AoMTAwJSwgcmdiYSgyMzcsMjM3LDIzNywxKSkpO1xyXG4gICAgYmFja2dyb3VuZDogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQobGVmdCwgcmdiYSgyNTUsMjU1LDI1NSwxKSAwJSwgcmdiYSgyNDYsMjQ2LDI0NiwxKSA0NyUsIHJnYmEoMjM3LDIzNywyMzcsMSkgMTAwJSk7XHJcbiAgICBiYWNrZ3JvdW5kOiAtby1saW5lYXItZ3JhZGllbnQobGVmdCwgcmdiYSgyNTUsMjU1LDI1NSwxKSAwJSwgcmdiYSgyNDYsMjQ2LDI0NiwxKSA0NyUsIHJnYmEoMjM3LDIzNywyMzcsMSkgMTAwJSk7XHJcbiAgICBiYWNrZ3JvdW5kOiAtbXMtbGluZWFyLWdyYWRpZW50KGxlZnQsIHJnYmEoMjU1LDI1NSwyNTUsMSkgMCUsIHJnYmEoMjQ2LDI0NiwyNDYsMSkgNDclLCByZ2JhKDIzNywyMzcsMjM3LDEpIDEwMCUpO1xyXG4gICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCByZ2JhKDI1NSwyNTUsMjU1LDEpIDAlLCByZ2JhKDI0NiwyNDYsMjQ2LDEpIDQ3JSwgcmdiYSgyMzcsMjM3LDIzNywxKSAxMDAlKTtcclxuICAgIGZpbHRlcjogcHJvZ2lkOkRYSW1hZ2VUcmFuc2Zvcm0uTWljcm9zb2Z0LmdyYWRpZW50KCBzdGFydENvbG9yc3RyPScjZmZmZmZmJywgZW5kQ29sb3JzdHI9JyNlZGVkZWQnLCBHcmFkaWVudFR5cGU9MSApO1xyXG4gIH1cclxuICBcclxuICAjbG9nbyB7XHJcbiAgICBjb2xvcjogIzNiNTk5OFxyXG4gIH1cclxuXHJcbiAgI2xvZ29HIHtcclxuICAgICAgY29sb3I6ICNjMjIyMjI7XHJcbiAgfVxyXG4gIFxyXG4gICNib3RvbiB7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICAgLy8gZm9udC1zaXplOiBsYXJnZTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XHJcbiAgfVxyXG5cclxuICAjYm90b25jIHtcclxuICAgIHdpZHRoOiA4MCU7XHJcbiAgICBmb250LXNpemU6IDcxJTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XHJcbiAgfVxyXG4gIFxyXG5cclxuICAiXX0= */";
      /***/
    },

    /***/
    "./src/app/component/login/login.page.ts":
    /*!***********************************************!*\
      !*** ./src/app/component/login/login.page.ts ***!
      \***********************************************/

    /*! exports provided: LoginPage */

    /***/
    function srcAppComponentLoginLoginPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPage", function () {
        return LoginPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var src_app_servicio_usuario_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/servicio/usuario.service */
      "./src/app/servicio/usuario.service.ts");
      /* harmony import */


      var _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic-native/facebook/ngx */
      "./node_modules/@ionic-native/facebook/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var firebase_app__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! firebase/app */
      "./node_modules/firebase/app/dist/index.esm.js");
      /* harmony import */


      var firebase_database__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! firebase/database */
      "./node_modules/firebase/database/dist/index.esm.js");
      /* harmony import */


      var firebase_auth__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! firebase/auth */
      "./node_modules/firebase/auth/dist/index.esm.js");
      /* harmony import */


      var firebase_firestore__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! firebase/firestore */
      "./node_modules/firebase/firestore/dist/index.esm.js");
      /* harmony import */


      var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @angular/fire/firestore */
      "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");
      /* harmony import */


      var _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! @ionic-native/google-plus/ngx */
      "./node_modules/@ionic-native/google-plus/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var src_app_servicio_notification_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! src/app/servicio/notification.service */
      "./src/app/servicio/notification.service.ts"); //import { auth } from 'firebase';


      var LoginPage = /*#__PURE__*/function () {
        function LoginPage(authServ, router, fb, platform, loadingCtrl, googlePlus, db, usuarioProv, pushnot) {
          _classCallCheck(this, LoginPage);

          this.authServ = authServ;
          this.router = router;
          this.fb = fb;
          this.platform = platform;
          this.loadingCtrl = loadingCtrl;
          this.googlePlus = googlePlus;
          this.db = db;
          this.usuarioProv = usuarioProv;
          this.pushnot = pushnot;
          this.botonesA = {};
          this.nombresUsers = {};
          this.uidUserSesion = localStorage.getItem('uid');
          console.log('id del usuario ya registrado', this.uidUserSesion);
          this.initializeApp();
        }

        _createClass(LoginPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "signWithCorreo",
          value: function signWithCorreo() {
            this.router.navigate(['/correo']);
          }
        }, {
          key: "register",
          value: function register() {
            this.router.navigate(['/register']);
          }
        }, {
          key: "signInWithFacebook",
          value: function signInWithFacebook() {
            var _this = this;

            this.fb.login(['public_profile', 'email']).then(function (res) {
              var facebookCredential = firebase_app__WEBPACK_IMPORTED_MODULE_6__["default"].auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
              firebase_app__WEBPACK_IMPORTED_MODULE_6__["default"].auth().signInWithCredential(facebookCredential).then(function (user) {
                _this.us = user.user;
                console.log('Usuario: ', JSON.stringify(_this.us));
                localStorage.setItem("uid", _this.us.uid);
                localStorage.setItem("isLogin", 'true'); //console.log('userID',this.us.uid);

                _this.url = "?height=500";
                _this.usPhotoUrl = _this.us.photoURL + _this.url;

                _this.usuarioProv.cargarUsuario(_this.us.displayName, _this.us.email, _this.us.photoURL, _this.us.uid); //sacar el codigo del usuario


                _this.usuarioProv.getCodigo(_this.us.uid).subscribe(function (co) {
                  _this.codigos = co;
                  console.log('datos tabla user', _this.codigos.length);

                  if (_this.codigos.length == 0) {
                    _this.db.collection('users').doc(_this.usuarioProv.usuario.uid).set({
                      idUser: _this.usuarioProv.usuario.uid,
                      displayName: _this.us.displayName,
                      email: _this.us.email,
                      photoURL: _this.us.photoURL,
                      estatus: 0,
                      estatusPush: 0
                    }).then(function (docRef) {
                      console.log("Document written with ID: ", docRef);

                      _this.mensaje();
                    })["catch"](function (error) {
                      alert('Error de autenticación' + JSON.stringify(error));
                      console.error("Error adding document: ", error);
                    });
                  }

                  if (_this.codigos.length == 1) {
                    _this.codigos.forEach(function (element) {
                      var id = element.idUser;
                      var estatus = element.estatus;
                      var estatusPush = element.estatusPush;

                      if (estatus == 0) {
                        _this.router.navigate(['/ciudad', _this.usuarioProv.usuario.uid]);

                        _this.usuarioProv.add_UserGmail(_this.us.displayName, _this.us.email, _this.usuarioProv.usuario.uid);
                      }

                      if (estatusPush == 2) {
                        _this.pushnot.init_notification(_this.usuarioProv.usuario.uid);
                      }

                      if (estatus == 1) {
                        _this.router.navigate(['/tienda', _this.us.uid]);
                      }
                    });
                  }
                });
              })["catch"](function (e) {
                return alert('Error de autenticación' + JSON.stringify(e));
              });
            });
          }
        }, {
          key: "singWithGoogle",
          value: function singWithGoogle() {
            var _this2 = this;

            this.googlePlus.login({
              webClientId: "307577600417-9j6ulq5se8fp2cv9e9n905dr2psps5qq.apps.googleusercontent.com",
              offline: true
            }).then(function (res) {
              firebase_app__WEBPACK_IMPORTED_MODULE_6__["default"].auth().signInWithCredential(firebase_app__WEBPACK_IMPORTED_MODULE_6__["default"].auth.GoogleAuthProvider.credential(res.idToken)).then(function (user) {
                _this2.us = user.user;
                console.log(JSON.stringify(user));
                console.log(res.idToken);
                localStorage.setItem("uid", _this2.us.uid);
                localStorage.setItem("isLogin", 'true');

                _this2.usuarioProv.cargarUsuarioGmail(_this2.us.displayName, _this2.us.email, _this2.us.photoURL, _this2.us.uid);

                _this2.usuarioProv.ingresoGmail(_this2.us.uid).subscribe(function (co) {
                  _this2.ingresoG = co;
                  console.log('datos tabla user', _this2.ingresoG.length);

                  if (_this2.ingresoG.length == 0) {
                    _this2.db.collection('users').doc(_this2.usuarioProv.usuario.uid).set({
                      uid: _this2.usuarioProv.usuario.uid,
                      displayName: _this2.us.displayName,
                      email: _this2.us.email,
                      photoURL: _this2.us.photoURL,
                      estatus: 0
                    }).then(function () {
                      return _this2.mensaje();
                    })["catch"](function (error) {
                      console.error("Error adding document: ", error);
                    });

                    _this2.pushnot.init_notification(_this2.us.uid);

                    _this2.usuarioProv.add_UserGmail(_this2.us.displayName, _this2.us.email, _this2.us.uid);

                    _this2.router.navigate(['/ciudad', _this2.us.uid]);
                  }

                  if (_this2.ingresoG.length == 1) {
                    _this2.router.navigate(['/tienda', _this2.us.uid]);
                  }
                });
              })["catch"](function (error) {
                return console.log("Error en el then prueba 1: " + JSON.stringify(error));
              });
            })["catch"](function (err) {
              return console.error("No entra al then prueba 1", err);
            });
          }
        }, {
          key: "mensaje",
          value: function mensaje() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var loading, _yield$loading$onDidD, role, data;

              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.loadingCtrl.create({
                        cssClass: 'my-custom-class',
                        message: 'Iniciando Sesión',
                        duration: 2000
                      });

                    case 2:
                      loading = _context.sent;
                      _context.next = 5;
                      return loading.present();

                    case 5:
                      _context.next = 7;
                      return loading.onDidDismiss();

                    case 7:
                      _yield$loading$onDidD = _context.sent;
                      role = _yield$loading$onDidD.role;
                      data = _yield$loading$onDidD.data;
                      console.log('Loading dismissed!');

                    case 11:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "initializeApp",
          value: function initializeApp() {
            this.botones();
            this.usuarioRe();
            this.hola();
          }
        }, {
          key: "botones",
          value: function botones() {
            var _this3 = this;

            this.db.collection('sistema').doc('botones_activos').valueChanges().subscribe(function (data) {
              _this3.botonesA = data;
              console.log("Datos de los botones", _this3.botonesA.estatus);
            });
          }
        }, {
          key: "usuarioRe",
          value: function usuarioRe() {
            var _this4 = this;

            if (this.uidUserSesion == null || this.uidUserSesion == undefined) {
              this.router.navigate(['/login']);
            }

            if (this.uidUserSesion != null) {
              this.db.collection('users').doc(this.uidUserSesion).valueChanges().subscribe(function (data) {
                _this4.nombresUsers = data;
                console.log("Datos del usuario con localstorage", _this4.nombresUsers);
                console.log("Datos de los botones", _this4.nombresUsers);

                _this4.router.navigate(['/tienda', _this4.uidUserSesion]);

                _this4.mensaje();

                localStorage.setItem("uid", _this4.uidUserSesion);
                localStorage.setItem("isLogin", 'true');
              });
            }
          }
        }, {
          key: "hola",
          value: function hola() {
            console.log("Por que");
          }
        }]);

        return LoginPage;
      }();

      LoginPage.ctorParameters = function () {
        return [{
          type: src_app_servicio_usuario_service__WEBPACK_IMPORTED_MODULE_3__["UsuarioService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_4__["Facebook"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Platform"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"]
        }, {
          type: _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_11__["GooglePlus"]
        }, {
          type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_10__["AngularFirestore"]
        }, {
          type: src_app_servicio_usuario_service__WEBPACK_IMPORTED_MODULE_3__["UsuarioService"]
        }, {
          type: src_app_servicio_notification_service__WEBPACK_IMPORTED_MODULE_12__["NotificationService"]
        }];
      };

      LoginPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./login.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/component/login/login.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./login.page.scss */
        "./src/app/component/login/login.page.scss"))["default"]]
      })], LoginPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=component-login-login-module-es5.js.map