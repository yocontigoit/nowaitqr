(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["component-establecimiento-establecimiento-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/component/establecimiento/establecimiento.page.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/component/establecimiento/establecimiento.page.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n\n  <div id=\"container\">\n\n    <ion-grid>\n      <ion-row>\n        <ion-col id=\"centrarCaja\">\n          <img src=\"./assets/icon/new_disigner_small.jpg\" width=\"80%\" style=\"margin-left: 30%;\" alt=\"\">\n        </ion-col>\n        <ion-col size=\"4\" id=\"iconoCol\">\n          <!-- <ion-icon name=\"power\" (click)=\"goToExit()\"></ion-icon> -->\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n  </div>\n\n  <ion-card *ngFor=\"let tienda of tiendas\">\n    <ion-card-content style=\"margin-top: auto;\">\n      <ion-grid>\n        <ion-row>\n          <ion-col size=\"3\">\n            <ion-thumbnail item-start>\n              <img src=\"../../assets/icon/icono.PNG\">\n            </ion-thumbnail>\n          </ion-col>\n          <ion-col size=\"7\">\n            <h1>{{tienda.nombre_tienda}}</h1>\n          </ion-col>\n          <ion-col size=\"2\">\n            <ion-buttons end>\n              <!-- <button ion-button icon-only (click)=\"goToList(tienda.idTienda)\" id=\"icono\">\n                <ion-icon name=\"scan-circle-outline\"></ion-icon>\n              </button> -->\n              <button ion-button icon-only (click)=\"view(tienda.idTienda)\" id=\"icono\">\n                <ion-icon name=\"scan-circle-outline\"></ion-icon>\n              </button>\n            </ion-buttons>\n          </ion-col> \n        </ion-row>\n      </ion-grid>\n      <!-- <button ion-button clear item-end (click)=\"goToList(tienda.idTienda)\">\n        <ion-icon name=\"scan-circle-outline\" item-end></ion-icon>\n      </button> -->\n      <!-- <button ion-button clear item-end (click)=\"view()\">View</button> -->\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n\n<ion-footer>\n  <ion-toolbar id=\"imgfo\">\n    <p>Lugares Pendientes</p>\n    <img src=\"./assets/icon/ejemplo.png\" style=\"width: 80px;\" (click)=\"goToEstablecimiento()\" alt=\"\">\n  </ion-toolbar>\n</ion-footer>\n");

/***/ }),

/***/ "./src/app/component/establecimiento/establecimiento-routing.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/component/establecimiento/establecimiento-routing.module.ts ***!
  \*****************************************************************************/
/*! exports provided: EstablecimientoPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EstablecimientoPageRoutingModule", function() { return EstablecimientoPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _establecimiento_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./establecimiento.page */ "./src/app/component/establecimiento/establecimiento.page.ts");




const routes = [
    {
        path: '',
        component: _establecimiento_page__WEBPACK_IMPORTED_MODULE_3__["EstablecimientoPage"]
    }
];
let EstablecimientoPageRoutingModule = class EstablecimientoPageRoutingModule {
};
EstablecimientoPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EstablecimientoPageRoutingModule);



/***/ }),

/***/ "./src/app/component/establecimiento/establecimiento.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/component/establecimiento/establecimiento.module.ts ***!
  \*********************************************************************/
/*! exports provided: EstablecimientoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EstablecimientoPageModule", function() { return EstablecimientoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _establecimiento_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./establecimiento-routing.module */ "./src/app/component/establecimiento/establecimiento-routing.module.ts");
/* harmony import */ var _establecimiento_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./establecimiento.page */ "./src/app/component/establecimiento/establecimiento.page.ts");







let EstablecimientoPageModule = class EstablecimientoPageModule {
};
EstablecimientoPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _establecimiento_routing_module__WEBPACK_IMPORTED_MODULE_5__["EstablecimientoPageRoutingModule"]
        ],
        declarations: [_establecimiento_page__WEBPACK_IMPORTED_MODULE_6__["EstablecimientoPage"]]
    })
], EstablecimientoPageModule);



/***/ }),

/***/ "./src/app/component/establecimiento/establecimiento.page.scss":
/*!*********************************************************************!*\
  !*** ./src/app/component/establecimiento/establecimiento.page.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#icono {\n  background: white;\n  color: black;\n  font-size: 30px;\n}\n\n#titulo {\n  font-size: larger;\n  text-align: center;\n  color: #484646;\n  font-weight: bold;\n}\n\n#imgfo {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  /* top: 50%; */\n  transform: translateY(-100%);\n}\n\n#nombrePro {\n  font-size: xx-large;\n  color: #676767;\n  font-family: sans-serif;\n}\n\n#frase {\n  font-weight: bold;\n}\n\n#nowait {\n  font-size: xx-large;\n  color: #676767;\n  font-family: sans-serif;\n  text-align: center;\n}\n\n#qr {\n  font-weight: bold;\n}\n\n.footer-ios ion-toolbar:first-of-type {\n  --border-width: 0px 0 0;\n}\n\n#container {\n  left: 0;\n  right: 0;\n  --ion-background-color:#192938;\n  background-color: #192938;\n}\n\n#iconoCol {\n  font-size: xxx-large;\n  color: #fff;\n  position: relative !important;\n  left: 18% !important;\n  margin-top: 3%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50L2VzdGFibGVjaW1pZW50by9lc3RhYmxlY2ltaWVudG8ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQUNKOztBQUVBO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtBQUNKOztBQUVBO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0VBQ0EsY0FBQTtFQUNBLDRCQUFBO0FBQ0o7O0FBRUU7RUFDRSxtQkFBQTtFQUNBLGNBQUE7RUFDQSx1QkFBQTtBQUNKOztBQUVFO0VBQ0UsaUJBQUE7QUFDSjs7QUFFRTtFQUNFLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFFRTtFQUNFLGlCQUFBO0FBQ0o7O0FBR0E7RUFDSSx1QkFBQTtBQUFKOztBQUdBO0VBQ0UsT0FBQTtFQUNBLFFBQUE7RUFDQSw4QkFBQTtFQUNBLHlCQUFBO0FBQUY7O0FBR0E7RUFDRSxvQkFBQTtFQUNBLFdBQUE7RUFDQSw2QkFBQTtFQUNBLG9CQUFBO0VBQ0EsY0FBQTtBQUFGIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50L2VzdGFibGVjaW1pZW50by9lc3RhYmxlY2ltaWVudG8ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2ljb25vIHtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG59XHJcblxyXG4jdGl0dWxve1xyXG4gICAgZm9udC1zaXplOiBsYXJnZXI7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBjb2xvcjogIzQ4NDY0NjtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcblxyXG4jaW1nZm8ge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgbGVmdDogMDtcclxuICAgIHJpZ2h0OiAwO1xyXG4gICAgLyogdG9wOiA1MCU7ICovXHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTEwMCUpO1xyXG4gIH1cclxuXHJcbiAgI25vbWJyZVBybyB7XHJcbiAgICBmb250LXNpemU6IHh4LWxhcmdlO1xyXG4gICAgY29sb3I6ICM2NzY3Njc7XHJcbiAgICBmb250LWZhbWlseTogc2Fucy1zZXJpZjtcclxuICB9XHJcbiAgXHJcbiAgI2ZyYXNlIHtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIH1cclxuXHJcbiAgI25vd2FpdCB7XHJcbiAgICBmb250LXNpemU6IHh4LWxhcmdlO1xyXG4gICAgY29sb3I6ICM2NzY3Njc7XHJcbiAgICBmb250LWZhbWlseTogc2Fucy1zZXJpZjtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcblxyXG4gICNxciB7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICB9XHJcblxyXG5cclxuLmZvb3Rlci1pb3MgaW9uLXRvb2xiYXI6Zmlyc3Qtb2YtdHlwZSB7XHJcbiAgICAtLWJvcmRlci13aWR0aDogMHB4IDAgMDtcclxufVxyXG5cclxuI2NvbnRhaW5lciB7XHJcbiAgbGVmdDogMDtcclxuICByaWdodDogMDtcclxuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiMxOTI5Mzg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzE5MjkzODtcclxufVxyXG5cclxuI2ljb25vQ29se1xyXG4gIGZvbnQtc2l6ZTogeHh4LWxhcmdlO1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZSAhaW1wb3J0YW50O1xyXG4gIGxlZnQ6IDE4JSAhaW1wb3J0YW50O1xyXG4gIG1hcmdpbi10b3A6IDMlO1xyXG59Il19 */");

/***/ }),

/***/ "./src/app/component/establecimiento/establecimiento.page.ts":
/*!*******************************************************************!*\
  !*** ./src/app/component/establecimiento/establecimiento.page.ts ***!
  \*******************************************************************/
/*! exports provided: EstablecimientoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EstablecimientoPage", function() { return EstablecimientoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var src_app_servicio_sucursal_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/servicio/sucursal.service */ "./src/app/servicio/sucursal.service.ts");
/* harmony import */ var src_app_servicio_tiendas_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/servicio/tiendas.service */ "./src/app/servicio/tiendas.service.ts");
/* harmony import */ var _ionic_native_qr_scanner_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/qr-scanner/ngx */ "./node_modules/@ionic-native/qr-scanner/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/__ivy_ngcc__/fesm2015/http.js");









let EstablecimientoPage = class EstablecimientoPage {
    constructor(router, route, tiendaProv, sucProv, qrScanner, alertController, toastController, http) {
        this.router = router;
        this.route = route;
        this.tiendaProv = tiendaProv;
        this.sucProv = sucProv;
        this.qrScanner = qrScanner;
        this.alertController = alertController;
        this.toastController = toastController;
        this.http = http;
        this.tiendas = [];
        this.usuarios = [];
        this.ciudades = [];
        this.myUsers = [];
    }
    ngOnInit() {
        let id = this.route.snapshot.paramMap.get("id");
        console.log("este es el idUser:", id);
        this.idUser = id;
        this.ciudades = [];
        // this.AllTiendas();
        this.AllUser();
        this.cambioTurno();
        this.tiendas = [];
        this.usuarios = [];
        this.myUsers = [];
        this.myUser();
        var today = moment__WEBPACK_IMPORTED_MODULE_7__().format('YYYY-MM-DD');
        console.log("variable today", today);
        this.momentos = today;
        console.log("variable momentos", this.momentos);
        this.momentoActual = new Date();
        this.hora = this.momentoActual.getHours();
        this.minuto = this.momentoActual.getMinutes();
        this.segundo = this.momentoActual.getSeconds();
        this.horas = this.hora + ':' + this.minuto;
    }
    AllUser() {
        return new Promise(resolve => {
            let body = {
                idUser: this.idUser,
                accion: 'getdataUser',
            };
            this.sucProv.postData(body, 'establecimiento.php').subscribe(data => {
                for (let usuario of data.result) {
                    this.usuarios.push(usuario);
                    console.log("Datos de usuarios", this.usuarios);
                    console.log("Sucursal que eligio el usuario", usuario.sucursal);
                    this.idSucursal = usuario.sucursal;
                    this.idCiudad = usuario.ciudad;
                    this.playerID = usuario.playerID;
                    this.AllTiendas(usuario.sucursal);
                }
                resolve(true);
            });
        });
    }
    AllTiendas(idSucursal) {
        return new Promise(resolve => {
            let body = {
                idSucursal: idSucursal,
                accion: 'getdataTienda',
            };
            this.sucProv.postData(body, 'establecimiento.php').subscribe(data => {
                for (let tienda of data.result) {
                    this.tiendas.push(tienda);
                    console.log("Total de tiendas", this.tiendas);
                }
                resolve(true);
            });
        });
    }
    goToList(idTienda) {
        this.router.navigate(['/descripcion', this.idUser, this.idSucursal, idTienda, this.playerID]);
    }
    view(idTienda) {
        // Optionally request the permission early
        this.qrScanner.prepare()
            .then((status) => {
            if (status.authorized) {
                // camera permission was granted
                // start scanning
                let scanSub = this.qrScanner.scan().subscribe((text) => {
                    console.log('Scanned something', text);
                    // alert("precion")
                    this.qrScanner.hide(); // hide camera preview
                    scanSub.unsubscribe(); // stop scanning
                });
                this.qrScanner.show();
                document.getElementsByTagName('body')[0].style.opacity = '0';
                this.qrScan = this.qrScanner.scan().subscribe((textFound) => {
                    document.getElementsByTagName("body")[0].style.opacity = "1";
                    this.qrScanner.hide();
                    this.qrScan.unsubscribe();
                    this.qrText = textFound;
                    this.router.navigate(['/descripcion', this.idUser, this.idSucursal, idTienda, this.playerID]);
                    // alert("precion configuracion");
                }), (err) => {
                    alert(JSON.stringify(err));
                };
            }
            else if (status.denied) {
                // camera permission was permanently denied
                // you must use QRScanner.openSettings() method to guide the user to the settings page
                // then they can grant the permission from there
            }
            else {
                // permission was denied, but not permanently. You can ask for permission again at a later time.
            }
        })
            .catch((e) => console.log('Error is', e));
    }
    goToEstablecimiento() {
        this.router.navigate(['/lugares', this.idUser]);
    }
    myUser() {
        return new Promise(resolve => {
            let body = {
                idUser: this.idUser,
                accion: 'getDataMyuser'
            };
            this.sucProv.postData(body, 'add_user.php').subscribe(data => {
                for (let myUser of data.result) {
                    this.myUsers.push(myUser);
                    console.log("Estos son los datos del usuario", this.myUsers);
                    console.log("Datos del formulario", myUser.formulario);
                    if (myUser.formulario == 1) {
                        this.presentAlertPrompt();
                    }
                }
                resolve(true);
            });
        });
    }
    presentAlertPrompt() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                header: '¿Cómo fue tu experiencia en la tienda?',
                inputs: [
                    {
                        name: 'paragraph',
                        id: 'paragraph',
                        type: 'textarea',
                        placeholder: 'Describe como fue tu experiencia ...'
                    }
                ],
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: () => {
                            console.log('Confirm Cancel');
                        }
                    }, {
                        text: 'Guardar',
                        handler: data => {
                            console.log('Confirm Ok');
                            console.log("Dato a almacenar", data.paragraph);
                            let body = {
                                descripcion: data.paragraph,
                                type: 'tienda',
                                fecha: this.momentos,
                                idUser: this.idUser,
                                accion: 'add_form_tienda'
                            };
                            this.sucProv.postData(body, 'add_formulario.php').subscribe(data => {
                                if (data.success) {
                                    console.log("Se ha agregado el Formulario Tienda");
                                    this.presentFormApp();
                                }
                            });
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    presentFormApp() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                header: '¿Cómo fue tu experiencia con la aplicación?',
                inputs: [
                    {
                        name: 'paragraph',
                        id: 'paragraph',
                        type: 'textarea',
                        placeholder: 'Describe como fue tu experiencia ...'
                    }
                ],
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: () => {
                            console.log('Confirm Cancel');
                        }
                    }, {
                        text: 'Guardar',
                        handler: data => {
                            console.log('Confirm Ok');
                            console.log("Dato a almacenar", data.paragraph);
                            let body = {
                                descripcion: data.paragraph,
                                type: 'app',
                                fecha: this.momentos,
                                idUser: this.idUser,
                                accion: 'add_form_tienda'
                            };
                            this.sucProv.postData(body, 'add_formulario.php').subscribe(data => {
                                if (data.success) {
                                    console.log("Se ha agregado el Formulario App");
                                    this.presentToast();
                                }
                            });
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    presentToast() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: 'Se ha enviado tu comentario.',
                duration: 2000
            });
            toast.present();
            let body = {
                formulario: 0,
                idUser: this.idUser,
                accion: 'getUpdateFormUser'
            };
            this.sucProv.postData(body, 'add_user.php').subscribe(data => {
                if (data.success) {
                    console.log("Se ha modificado el estatus formulario");
                }
            });
        });
    }
    cambioTurno() {
        this.horas = this.hora + ':' + this.minuto;
        console.log("Esta es la hora actual", this.hora, ':', this.minuto, "Hora completa", this.horas);
        return new Promise(resolve => {
            let body = {
                idUser: this.idUser,
                accion: 'getCambioTurno'
            };
            this.sucProv.postData(body, 'add_formulario.php').subscribe(data => {
                for (let turnos of data.result) {
                    console.log("Turnos", turnos);
                    if (turnos.estatus == 'activado' && this.momentos == turnos.fecha) {
                        if (turnos.salida < this.horas) {
                            this.continue(turnos.idFila);
                            console.log("turnos seleccionados", turnos.idFila);
                        }
                    }
                }
                resolve(true);
            });
        });
    }
    continue(idFila) {
        let data = JSON.stringify({
            idFila: idFila,
            estatus: 'concluido',
            accion: 'updateCambioTurno'
        });
        let url = 'https://proyectosinternos.com/NoWaitQR/server_api_nowaitqr/add_formulario.php';
        let type = "application/json; charset=UTF-8";
        let headers = new _angular_http__WEBPACK_IMPORTED_MODULE_8__["Headers"]({ "Content-Type": type });
        let options = new _angular_http__WEBPACK_IMPORTED_MODULE_8__["RequestOptions"]({ headers: headers });
        return this.http.post(url, data, options).subscribe(res => {
            console.log('Se actualizo el estatus', res);
        });
    }
    goToExit() {
        localStorage.setItem("uid", '');
        localStorage.setItem("isLogin", 'false');
        this.router.navigate(['/login']);
    }
};
EstablecimientoPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: src_app_servicio_tiendas_service__WEBPACK_IMPORTED_MODULE_4__["TiendasService"] },
    { type: src_app_servicio_sucursal_service__WEBPACK_IMPORTED_MODULE_3__["SucursalService"] },
    { type: _ionic_native_qr_scanner_ngx__WEBPACK_IMPORTED_MODULE_5__["QRScanner"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"] },
    { type: _angular_http__WEBPACK_IMPORTED_MODULE_8__["Http"] }
];
EstablecimientoPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-establecimiento',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./establecimiento.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/component/establecimiento/establecimiento.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./establecimiento.page.scss */ "./src/app/component/establecimiento/establecimiento.page.scss")).default]
    })
], EstablecimientoPage);



/***/ }),

/***/ "./src/app/servicio/tiendas.service.ts":
/*!*********************************************!*\
  !*** ./src/app/servicio/tiendas.service.ts ***!
  \*********************************************/
/*! exports provided: TiendasService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TiendasService", function() { return TiendasService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");




let TiendasService = class TiendasService {
    constructor(http) {
        this.http = http;
        //server: string = "http://localhost/server_api_nowaitqr/";
        this.server = "https://proyectosinternos.com/NoWaitQR/server_api_nowaitqr/";
    }
    postData(body, file) {
        let type = "application/json; charset=UTF-8";
        let headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({ 'Content-Type': type });
        let options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        return this.http.post(this.server + file, JSON.stringify(body), options)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(res => res.json()));
    }
};
TiendasService.ctorParameters = () => [
    { type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"] }
];
TiendasService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], TiendasService);



/***/ })

}]);
//# sourceMappingURL=component-establecimiento-establecimiento-module-es2015.js.map