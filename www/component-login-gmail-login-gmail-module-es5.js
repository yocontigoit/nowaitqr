(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["component-login-gmail-login-gmail-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/component/login-gmail/login-gmail.page.html":
    /*!***************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/component/login-gmail/login-gmail.page.html ***!
      \***************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentLoginGmailLoginGmailPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title>login_gmail</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/component/login-gmail/login-gmail-routing.module.ts":
    /*!*********************************************************************!*\
      !*** ./src/app/component/login-gmail/login-gmail-routing.module.ts ***!
      \*********************************************************************/

    /*! exports provided: LoginGmailPageRoutingModule */

    /***/
    function srcAppComponentLoginGmailLoginGmailRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginGmailPageRoutingModule", function () {
        return LoginGmailPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _login_gmail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./login-gmail.page */
      "./src/app/component/login-gmail/login-gmail.page.ts");

      var routes = [{
        path: '',
        component: _login_gmail_page__WEBPACK_IMPORTED_MODULE_3__["LoginGmailPage"]
      }];

      var LoginGmailPageRoutingModule = function LoginGmailPageRoutingModule() {
        _classCallCheck(this, LoginGmailPageRoutingModule);
      };

      LoginGmailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], LoginGmailPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/component/login-gmail/login-gmail.module.ts":
    /*!*************************************************************!*\
      !*** ./src/app/component/login-gmail/login-gmail.module.ts ***!
      \*************************************************************/

    /*! exports provided: LoginGmailPageModule */

    /***/
    function srcAppComponentLoginGmailLoginGmailModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginGmailPageModule", function () {
        return LoginGmailPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _login_gmail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./login-gmail-routing.module */
      "./src/app/component/login-gmail/login-gmail-routing.module.ts");
      /* harmony import */


      var _login_gmail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./login-gmail.page */
      "./src/app/component/login-gmail/login-gmail.page.ts");

      var LoginGmailPageModule = function LoginGmailPageModule() {
        _classCallCheck(this, LoginGmailPageModule);
      };

      LoginGmailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _login_gmail_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginGmailPageRoutingModule"]],
        declarations: [_login_gmail_page__WEBPACK_IMPORTED_MODULE_6__["LoginGmailPage"]]
      })], LoginGmailPageModule);
      /***/
    },

    /***/
    "./src/app/component/login-gmail/login-gmail.page.scss":
    /*!*************************************************************!*\
      !*** ./src/app/component/login-gmail/login-gmail.page.scss ***!
      \*************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentLoginGmailLoginGmailPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudC9sb2dpbi1nbWFpbC9sb2dpbi1nbWFpbC5wYWdlLnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/component/login-gmail/login-gmail.page.ts":
    /*!***********************************************************!*\
      !*** ./src/app/component/login-gmail/login-gmail.page.ts ***!
      \***********************************************************/

    /*! exports provided: LoginGmailPage */

    /***/
    function srcAppComponentLoginGmailLoginGmailPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginGmailPage", function () {
        return LoginGmailPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var LoginGmailPage = /*#__PURE__*/function () {
        function LoginGmailPage() {
          _classCallCheck(this, LoginGmailPage);
        }

        _createClass(LoginGmailPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return LoginGmailPage;
      }();

      LoginGmailPage.ctorParameters = function () {
        return [];
      };

      LoginGmailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login-gmail',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./login-gmail.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/component/login-gmail/login-gmail.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./login-gmail.page.scss */
        "./src/app/component/login-gmail/login-gmail.page.scss"))["default"]]
      })], LoginGmailPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=component-login-gmail-login-gmail-module-es5.js.map